﻿using SalesRoom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.SalesRoom
{
    public interface IQuoteRepository
    {
        List<Quote> GetQuotes();

        Quote GetQuote(int id);

        Quote AddQuote(Quote Quote);

        Quote EditQuote(Quote Quote);

        void DeleteQuote(int id);
    }
}

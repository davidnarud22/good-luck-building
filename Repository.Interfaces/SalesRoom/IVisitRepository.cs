﻿using SalesRoom.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.SalesRoom
{
    public interface IVisitRepository
    {
        List<Visit> GetVisits();

        Visit GetVisit(int id);

        Visit AddVisit(Visit Visit);

        Visit EditVisit(Visit Visit);

        void DeleteVisit(int id);
    }
}

﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface ICityRepository
    {
        List<City> GetCities();

        City GetCity(int id);

        City AddCity(City City);

        City EditCity(City City);

        void DeleteCity(int id);
    }
}

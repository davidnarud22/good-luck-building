﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface IStateOfPropertyRepository
    {
        List<StateOfProperty> GetStateOfPropertys();

        StateOfProperty GetStateOfProperty(int id);

        StateOfProperty AddStateOfProperty(StateOfProperty StateOfProperty);

        StateOfProperty EditStateOfProperty(StateOfProperty StateOfProperty);

        void DeleteStateOfProperty(int id);
    }
}

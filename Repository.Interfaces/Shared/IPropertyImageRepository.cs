﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface IPropertyImageRepository
    {
        List<PropertyImage> GetPropertyImages();

        PropertyImage GetPropertyImage(int id);

        PropertyImage AddPropertyImage(PropertyImage PropertyImage);

        List<PropertyImage> AddPropertyImages(List<PropertyImage> propertyImages);

        PropertyImage EditPropertyImage(PropertyImage PropertyImage);

        void DeletePropertyImage(int id);
    }
}

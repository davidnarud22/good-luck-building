﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface IMacroprojectRepository
    {
        List<Macroproject> GetMacroprojects();

        Macroproject GetMacroproject(int id);

        Macroproject AddMacroproject(Macroproject Macroproject);

        Macroproject EditMacroproject(Macroproject Macroproject);

        void DeleteMacroproject(int id);
    }
}

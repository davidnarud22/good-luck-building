﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface IProjectRepository
    {
        List<Project> GetProjects();

        Project GetProject(int id);

        Project AddProject(Project Project);

        Project EditProject(Project Project);

        void DeleteProject(int id);
    }
}

﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Shared
{
    public interface ITypeOfPaymentRepository
    {
        List<TypeOfPayment> GetTypeOfPayments();

        TypeOfPayment GetTypeOfPayment(int id);

        TypeOfPayment AddTypeOfPayment(TypeOfPayment TypeOfPayment);

        TypeOfPayment EditTypeOfPayment(TypeOfPayment TypeOfPayment);

        void DeleteTypeOfPayment(int id);
    }
}

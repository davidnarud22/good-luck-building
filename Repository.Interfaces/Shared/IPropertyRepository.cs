﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Shared
{
    public interface IPropertyRepository
    {
        List<Property> GetPropertys();

        Property GetProperty(int id);

        Property AddProperty(Property Property);

        Property EditProperty(Property Property);

        void DeleteProperty(int id);

    }
}

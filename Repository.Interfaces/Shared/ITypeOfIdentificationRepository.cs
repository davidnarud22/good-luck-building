﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Shared
{
    public interface ITypeOfIdentificationRepository
    {
        List<TypeOfIdentification> GetTypeOfIdentifications();

        TypeOfIdentification GetTypeOfIdentification(int id);

        TypeOfIdentification AddTypeOfIdentification(TypeOfIdentification TypeOfIdentification);

        TypeOfIdentification EditTypeOfIdentification(TypeOfIdentification TypeOfIdentification);

        void DeleteTypeOfIdentification(int id);
    }
}

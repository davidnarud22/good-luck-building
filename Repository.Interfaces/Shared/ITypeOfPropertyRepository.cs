﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Shared
{
    public interface ITypeOfPropertyRepository
    {
        List<TypeOfProperty> GetTypeOfPropertys();

        TypeOfProperty GetTypeOfProperty(int id);

        TypeOfProperty AddTypeOfProperty(TypeOfProperty TypeOfProperty);

        TypeOfProperty EditTypeOfProperty(TypeOfProperty TypeOfProperty);

        void DeleteTypeOfProperty(int id);
    }
}

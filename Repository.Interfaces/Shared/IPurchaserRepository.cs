﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Shared
{
    public interface IPurchaserRepository
    {
        List<Purchaser> GetPurchasers();

        Purchaser GetPurchaser(int id);

        Purchaser AddPurchaser(Purchaser Purchaser);

        Purchaser EditPurchaser(Purchaser Purchaser);

        void DeletePurchaser(Purchaser Purchaser);
    }
}

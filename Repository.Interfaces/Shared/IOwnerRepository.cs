﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface IOwnerRepository
    {
        List<Owner> GetOwners();

        Owner GetOwner(int id);

        Owner AddOwner(Owner Owner);

        Owner EditOwner(Owner Owner);

        void DeleteOwner(int id);
    }
}

﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface IDepartmentRepository
    {
        List<Department> GetDepartments();

        Department GetDepartment(int id);

        Department AddDepartment(Department Department);

        Department EditDepartment(Department Department);

        void DeleteDepartment(int id);
    }
}

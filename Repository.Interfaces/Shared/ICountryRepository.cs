﻿using Shared.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Shared
{
    public interface ICountryRepository
    {
        List<Country> GetCountries();

        Country GetCountry(int id);

        Country AddCountry(Country Country);

        Country EditCountry(Country Country);

        void DeleteCountry(int id);
    }
}

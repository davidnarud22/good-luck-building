﻿using Formalities.Models;
using Sales.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.PaymentSchedules
{
    public interface IPaymentScheduleRepository
    {
        List<PaymentSchedule> GetPaymentSchedules();

        PaymentSchedule GetPaymentSchedule(int id);

        PaymentSchedule AddPaymentSchedule(PaymentSchedule PaymentSchedule);

        PaymentSchedule EditPaymentSchedule(PaymentSchedule PaymentSchedule);

        void DeletePaymentSchedule(int id);

    }
}

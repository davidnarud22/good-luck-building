﻿using Sales.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Sales
{
    public interface IPropertyTraceRepository
    {
        List<PropertyTrace> GetPropertyTraces();

        PropertyTrace GetPropertyTrace(int id);

        PropertyTrace AddPropertyTrace(PropertyTrace PropertyTrace);

        PropertyTrace EditPropertyTrace(PropertyTrace PropertyTrace);

        void DeletePropertyTrace(int id);

    }
}

﻿using Formalities.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Formalities
{
    public interface IProcedureRepository
    {
        List<Procedure> GetProcedures();

        Procedure GetProcedure(int id);

        Procedure AddProcedure(Procedure procedure);

        Procedure EditProcedure(Procedure procedure);

        void DeleteProcedure(Procedure procedure);

    }
}

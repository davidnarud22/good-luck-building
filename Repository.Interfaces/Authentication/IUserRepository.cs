﻿using Authentication.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Authentication
{
    public interface IUserRepository
    {
        List<User> GetUsers();

        User GetUser(int id);

        User AddUser(User User);

        User EditUser(User User);

        void DeleteUser(int id);
    }
}

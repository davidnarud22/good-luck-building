﻿using Authentication.Models;
using System.Collections.Generic;

namespace Repository.Interfaces.Authentication
{
    public interface IUserTypeRepository
    {
        List<UserType> GetUsersType();

        UserType GetUserType(int id);

        UserType AddUserType(UserType UserType);

        UserType EditUserType(UserType UserType);

        void DeleteUserType(int id);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class StatesOfPropertyRepository : IStateOfPropertyRepository
    {
        private SharedContext _sharedContext;

        public StateOfProperty GetStateOfProperty(int id)
        {
            return _sharedContext.stateOfProperties.Find(id);
        }

        public List<StateOfProperty> GetStateOfPropertys()
        {
            return _sharedContext.stateOfProperties.ToList();
        }

        public StatesOfPropertyRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }

        public StateOfProperty AddStateOfProperty(StateOfProperty stateOfProperty)
        {
            _sharedContext.stateOfProperties.Add(stateOfProperty);
            _sharedContext.SaveChanges();
            return stateOfProperty;
        }

        public StateOfProperty EditStateOfProperty(StateOfProperty stateOfProperty)
        {
            _sharedContext.Entry(stateOfProperty).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return stateOfProperty;
        }

        public void DeleteStateOfProperty(int id)
        {
            var statePropertyToDelete = _sharedContext.stateOfProperties.Find(id);
            _sharedContext.stateOfProperties.Remove(statePropertyToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

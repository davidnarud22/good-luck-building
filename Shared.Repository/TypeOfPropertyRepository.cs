﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class TypeOfPropertyRepository : ITypeOfPropertyRepository
    {
        private SharedContext _sharedContext;

        public TypeOfPropertyRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }
        public TypeOfProperty GetTypeOfProperty(int id)
        {
            return _sharedContext.typeOfProperties.Find(id);
        }

        public List<TypeOfProperty> GetTypeOfPropertys()
        {
            return _sharedContext.typeOfProperties.ToList();
        }

        public TypeOfProperty AddTypeOfProperty(TypeOfProperty typeOfProperty)
        {
            _sharedContext.typeOfProperties.Add(typeOfProperty);
            _sharedContext.SaveChanges();
            return typeOfProperty;
        }

        public TypeOfProperty EditTypeOfProperty(TypeOfProperty typeOfProperty)
        {
            _sharedContext.Entry(typeOfProperty).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return typeOfProperty;
        }

        public void DeleteTypeOfProperty(int id)
        {
            var typeOfPropertyToDelete = _sharedContext.typeOfProperties.Find(id);
            _sharedContext.typeOfProperties.Remove(typeOfPropertyToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

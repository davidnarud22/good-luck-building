﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class TypeOfPaymentRepository : ITypeOfPaymentRepository
    {
        private SharedContext _sharedContext;

        public TypeOfPaymentRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }

        public TypeOfPayment GetTypeOfPayment(int id)
        {
            return _sharedContext.typeOfPayments.Find(id);
        }

        public List<TypeOfPayment> GetTypeOfPayments()
        {
            return _sharedContext.typeOfPayments.ToList();
        }

        public TypeOfPayment AddTypeOfPayment(TypeOfPayment typeOfPayment)
        {
            _sharedContext.typeOfPayments.Add(typeOfPayment);
            _sharedContext.SaveChanges();
            return typeOfPayment;
        }

        public TypeOfPayment EditTypeOfPayment(TypeOfPayment typeOfPayment)
        {
            _sharedContext.Entry(typeOfPayment).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return typeOfPayment;
        }

        public void DeleteTypeOfPayment(int id)
        {
            var typeOfPaymentToDelete = _sharedContext.typeOfPayments.Find(id);
            _sharedContext.typeOfPayments.Remove(typeOfPaymentToDelete);
            _sharedContext.SaveChanges();
        }

    }
}

﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class PropertyImageRepository : IPropertyImageRepository
    {
        private SharedContext _sharedContext;

        public PropertyImageRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }
        public PropertyImage GetPropertyImage(int id)
        {
            return _sharedContext.propertyImages.Find(id);
        }

        public List<PropertyImage> GetPropertyImages()
        {
            return _sharedContext.propertyImages.ToList();
        }

        public PropertyImage AddPropertyImage(PropertyImage propertyImage)
        {
            _sharedContext.propertyImages.Add(propertyImage);
            _sharedContext.SaveChanges();
            return propertyImage;
        }
        public List<PropertyImage> AddPropertyImages(List<PropertyImage> propertyImages)
        {
            _sharedContext.propertyImages.AddRange(propertyImages);
            _sharedContext.SaveChanges();
            return propertyImages;
        }

        public PropertyImage EditPropertyImage(PropertyImage propertyImage)
        {
            _sharedContext.Entry(propertyImage).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return propertyImage;
        }
        public void DeletePropertyImage(int id)
        {
            var propertyImageToDelete = _sharedContext.propertyImages.Find(id);
            _sharedContext.propertyImages.Remove(propertyImageToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

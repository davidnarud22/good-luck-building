﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Repository.Migrations
{
    public partial class AddSPFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var createProcSql = @"CREATE PROCEDURE [dbo].[SP_FILTERS_PROPERTIES](
			@NumberOfBathrooms NVARCHAR(10),
			@MAJORORMINOROFBATHROOMS NVARCHAR(10),
			@NumberOfFloors NVARCHAR(10),
			@MAJORORMINOROFFLOORS NVARCHAR(10),
			@NumberOfRooms NVARCHAR(10),
			@MAJORORMINOROFROOMS NVARCHAR(10),
			@Area NVARCHAR(10) ,
			@MAJORORMINOROAREA NVARCHAR(10),
			@State NVARCHAR(10) ,
			@Year NVARCHAR(10) ,
			@MAJORORMINOROYEAR NVARCHAR(10),
			@Price NVARCHAR(MAX),
			@MAJORORMINOROPRICE NVARCHAR(10),
			@IdTypeOfProperty NVARCHAR(10) 
)AS

DECLARE @SELECT NVARCHAR(MAX) ,			
			@WHERE NVARCHAR(MAX),
			@AND NVARCHAR(MAX),
			@QUERY_FINAL NVARCHAR(MAX),
			@MAJOR NVARCHAR(2) = '>=',
			@MINOR NVARCHAR(2)='<='

			--@NumberOfBathrooms NVARCHAR(10) = 0,
			--@MAJORORMINOROFBATHROOMS NVARCHAR(10)= 1,
			--@NumberOfFloors NVARCHAR(10) = 1,
			--@MAJORORMINOROFFLOORS NVARCHAR(10)= 1,
			--@NumberOfRooms NVARCHAR(10) = 1,
			--@MAJORORMINOROFROOMS NVARCHAR(10)= 1,
			--@Area NVARCHAR(10) = 19,
			--@MAJORORMINOROAREA NVARCHAR(10)= 1,
			--@State NVARCHAR(10) = 3,
			--@Year NVARCHAR(10) = 1,
			--@MAJORORMINOROYEAR NVARCHAR(10)= 1,
			--@Price NVARCHAR(MAX) = 1,
			--@MAJORORMINOROPRICE NVARCHAR(10)= 1,
			--@IdTypeOfProperty NVARCHAR(10) = 6

			SET @SELECT = 'SELECT properties.Id
								  ,properties.Name
								  ,properties.Address
								  ,properties.IdProject
								  ,projects.Name AS NAMEPROJECT
								  ,departments.Name AS DEPARTMENT
								  ,Cities.Name AS City
								  ,countries.Name AS COUNTRY
								  ,properties.IdTypeOfProperty
								  ,typeOfProperties.Name AS TYPEPROPERTY
								  ,properties.Price
								  ,properties.CodeInternal
								  ,properties.Year
								  ,properties.State
								  ,stateOfProperties.Name AS STATEPROPERTY
								  ,properties.IdOwner
								  ,owners.FullName AS OWNER
								  ,properties.NumberOfBathrooms
								  ,properties.NumberOfFloors
								  ,properties.NumberOfRooms
								  ,properties.Area
								  ,properties.Neighborhood
							FROM PROPERTIES
							INNER JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON projects.IdCity = Cities.Id
							INNER JOIN countries ON projects.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id'

			SET	@WHERE = ' WHERE PROPERTIES.ID > 1'


			IF(@NumberOfBathrooms > 0)
			BEGIN
				IF(@MAJORORMINOROFBATHROOMS = 0)
					BEGIN
						SET @MAJORORMINOROFBATHROOMS = @MINOR
					END
				ELSE IF (@MAJORORMINOROFBATHROOMS = 1)
					BEGIN
						SET @MAJORORMINOROFBATHROOMS = @MAJOR
					END
				SET @AND = ' AND PROPERTIES.NumberOfBathrooms ' + @MAJORORMINOROFBATHROOMS + ' '+ @NumberOfBathrooms
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@NumberOfFloors > 0)
			BEGIN
				IF(@MAJORORMINOROFFLOORS = 0)
					BEGIN
						SET @MAJORORMINOROFFLOORS = @MINOR
					END
				ELSE IF (@MAJORORMINOROFFLOORS = 1)
					BEGIN
						SET @MAJORORMINOROFFLOORS = @MAJOR
					END
				SET @AND = ' AND PROPERTIES.NumberOfFloors ' + @MAJORORMINOROFFLOORS + ' '+ @NumberOfFloors
			
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@NumberOfRooms > 0)
			BEGIN
				IF(@MAJORORMINOROFROOMS = 0)
					BEGIN
						SET @MAJORORMINOROFROOMS = @MINOR
					END
				ELSE IF (@MAJORORMINOROFROOMS = 1)
					BEGIN
						SET @MAJORORMINOROFROOMS = @MAJOR
					END
				SET @AND = ' AND PROPERTIES.NumberOfRooms ' + @MAJORORMINOROFROOMS + ' '+ @NumberOfRooms
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@Area > 0)
			BEGIN
				IF(@MAJORORMINOROAREA = 0)
					BEGIN
						SET @MAJORORMINOROAREA = @MINOR
					END
				ELSE IF (@MAJORORMINOROAREA = 1)
					BEGIN
						SET @MAJORORMINOROAREA = @MAJOR
					END
				SET @AND = ' AND PROPERTIES.Area ' + @MAJORORMINOROAREA + ' '+ @Area
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@State > 0)
			BEGIN
				SET @AND = ' AND PROPERTIES.State ' + '=' + ' '+ @State
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@Year > 0)
			BEGIN
				IF(@MAJORORMINOROYEAR = 0)
					BEGIN
						SET @MAJORORMINOROYEAR = @MINOR
					END
				ELSE IF (@MAJORORMINOROYEAR = 1)
					BEGIN
						SET @MAJORORMINOROYEAR = @MAJOR
					END
				SET @AND = ' AND PROPERTIES.Year ' + @MAJORORMINOROYEAR + ' '+ @Year
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@Price > 0)
			BEGIN
				IF(@MAJORORMINOROPRICE = 0)
					BEGIN
						SET @MAJORORMINOROPRICE = @MINOR
					END
				ELSE IF (@MAJORORMINOROPRICE = 1)
					BEGIN
						SET @MAJORORMINOROPRICE = @MAJOR
					END
				SET @AND = ' AND PROPERTIES.Price ' + @MAJORORMINOROPRICE + ' '+ @Price
				SET @WHERE = @WHERE + '' + @AND
			END

			IF(@IdTypeOfProperty > 0)
			BEGIN
				SET @AND = ' AND PROPERTIES.IdTypeOfProperty ' + '=' + ' '+ @IdTypeOfProperty
				SET @WHERE = @WHERE + '' + @AND
			END
			
			SET @QUERY_FINAL = @SELECT + @WHERE

			PRINT @QUERY_FINAL

			EXEC SP_EXECUTESQL  @QUERY_FINAL;";
            migrationBuilder.Sql(createProcSql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

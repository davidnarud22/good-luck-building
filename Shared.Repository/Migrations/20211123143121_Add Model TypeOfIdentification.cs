﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Repository.Migrations
{
    public partial class AddModelTypeOfIdentification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_macroprojects_owners_IdOwner",
                table: "macroprojects");

            migrationBuilder.DropIndex(
                name: "IX_macroprojects_IdOwner",
                table: "macroprojects");

            migrationBuilder.DropColumn(
                name: "TypeOfIdentification",
                table: "owners");

            migrationBuilder.AddColumn<int>(
                name: "IdTypeOfIdentification",
                table: "owners",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "typeOfIdentifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_typeOfIdentifications", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "typeOfIdentifications");

            migrationBuilder.DropColumn(
                name: "IdTypeOfIdentification",
                table: "owners");

            migrationBuilder.AddColumn<string>(
                name: "TypeOfIdentification",
                table: "owners",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_macroprojects_IdOwner",
                table: "macroprojects",
                column: "IdOwner");

            migrationBuilder.AddForeignKey(
                name: "FK_macroprojects_owners_IdOwner",
                table: "macroprojects",
                column: "IdOwner",
                principalTable: "owners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

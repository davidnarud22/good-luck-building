﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Repository.Migrations
{
    public partial class Addpropertyareaofpropery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Area",
                table: "properties",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Area",
                table: "properties");
        }
    }
}

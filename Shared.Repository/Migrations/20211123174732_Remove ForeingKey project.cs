﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Repository.Migrations
{
    public partial class RemoveForeingKeyproject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_projects_owners_IdOwner",
                table: "projects");

            migrationBuilder.DropIndex(
                name: "IX_projects_IdOwner",
                table: "projects");

            migrationBuilder.AlterColumn<int>(
                name: "IdOwner",
                table: "projects",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdOwner",
                table: "projects",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_projects_IdOwner",
                table: "projects",
                column: "IdOwner");

            migrationBuilder.AddForeignKey(
                name: "FK_projects_owners_IdOwner",
                table: "projects",
                column: "IdOwner",
                principalTable: "owners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

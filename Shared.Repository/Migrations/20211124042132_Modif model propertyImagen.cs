﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Repository.Migrations
{
    public partial class ModifmodelpropertyImagen : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "nameProperty",
                table: "propertyImages",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "order",
                table: "propertyImages",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "nameProperty",
                table: "propertyImages");

            migrationBuilder.DropColumn(
                name: "order",
                table: "propertyImages");
        }
    }
}

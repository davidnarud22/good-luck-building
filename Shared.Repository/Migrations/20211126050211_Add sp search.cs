﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Repository.Migrations
{
    public partial class Addspsearch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			var createProcSql = @"CREATE PROCEDURE [dbo].[SP_SEARCH](
@SEARCH NVARCHAR(MAX)

			
)AS


DECLARE @SELECT NVARCHAR(MAX) ,			
			@WHERE NVARCHAR(MAX) = 'WHERE properties.ID < 0',
			@QUERY_FINAL NVARCHAR(MAX),
			@SEARCHFORNAME INT,
			@SEARCHFORDEPARTMENT INT,
			@SEARCHFORCITY INT,
			@SEARCHCOUNTRY INT,
			@SEARCHNEIGHBOORHOOD INT




			SET @SELECT = 'SELECT properties.Id
								  ,properties.Name
								  ,properties.Address
								  ,properties.IdProject
								  ,projects.Name AS NAMEPROJECT
								  ,departments.Name AS DEPARTMENT
								  ,Cities.Name AS City
								  ,countries.Name AS COUNTRY
								  ,properties.IdTypeOfProperty
								  ,typeOfProperties.Name AS TYPEPROPERTY
								  ,properties.Price
								  ,properties.CodeInternal
								  ,properties.Year
								  ,properties.State
								  ,stateOfProperties.Name AS STATEPROPERTY
								  ,properties.IdOwner
								  ,owners.FullName AS OWNER
								  ,properties.NumberOfBathrooms
								  ,properties.NumberOfFloors
								  ,properties.NumberOfRooms
								  ,properties.Area
								  ,properties.Neighborhood
							FROM PROPERTIES
							FULL JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON departments.IdCity = Cities.Id
							INNER JOIN countries ON Cities.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id'


			SET @SEARCHFORNAME =(SELECT COUNT(*)  FROM PROPERTIES
							FULL JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON departments.IdCity = Cities.Id
							INNER JOIN countries ON Cities.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id
							WHERE properties.Name LIKE '%'+@SEARCH+'%')

			SET @SEARCHFORDEPARTMENT = (SELECT COUNT(*)  FROM PROPERTIES
							FULL JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON departments.IdCity = Cities.Id
							INNER JOIN countries ON Cities.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id
							WHERE departments.Name LIKE '%'+@SEARCH+'%')

			SET @SEARCHFORCITY =(SELECT COUNT(*)  FROM PROPERTIES
							FULL JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON departments.IdCity = Cities.Id
							INNER JOIN countries ON Cities.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id
							WHERE Cities.Name LIKE '%'+@SEARCH+'%')

			SET @SEARCHCOUNTRY =(SELECT COUNT(*)  FROM PROPERTIES
							FULL JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON departments.IdCity = Cities.Id
							INNER JOIN countries ON Cities.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id
							WHERE countries.Name LIKE '%'+@SEARCH+'%')

			SET @SEARCHNEIGHBOORHOOD = (SELECT COUNT(*)  FROM PROPERTIES
							FULL JOIN projects ON properties.IdProject = projects.Id
							INNER JOIN departments ON properties.IdDepartment = departments.Id
							INNER JOIN Cities ON departments.IdCity = Cities.Id
							INNER JOIN countries ON Cities.IdCountry = countries.id
							INNER JOIN typeOfProperties ON properties.IdTypeOfProperty = typeOfProperties.Id
							INNER JOIN stateOfProperties ON properties.State = stateOfProperties.Id
							INNER JOIN owners ON properties.IdOwner = owners.Id
							WHERE properties.Neighborhood LIKE '%'+@SEARCH+'%')

								
			IF( @SEARCHFORNAME > 0)
			BEGIN
				SET @WHERE = ' WHERE properties.Name LIKE ' +'''%'+ @SEARCH +'%'''
			END

			ELSE IF(@SEARCHFORNAME = 0 AND @SEARCHFORDEPARTMENT > 0)
			BEGIN
				SET @WHERE = ' WHERE departments.Name LIKE ' + '''%'+ @SEARCH +'%'''
			END

			ELSE IF(@SEARCHFORNAME = 0 AND @SEARCHFORDEPARTMENT= 0 AND @SEARCHFORCITY > 0)
			BEGIN
				SET @WHERE = ' WHERE Cities.Name LIKE ' + '''%'+ @SEARCH +'%'''
			END

			ELSE IF(@SEARCHFORNAME = 0 AND @SEARCHFORDEPARTMENT= 0 AND @SEARCHFORCITY = 0 AND @SEARCHCOUNTRY > 0 )
			BEGIN
				SET @WHERE = ' WHERE countries.Name LIKE ' + '''%'+ @SEARCH +'%'''
			END

			ELSE IF(@SEARCHFORNAME = 0 AND @SEARCHFORDEPARTMENT= 0 AND @SEARCHFORCITY = 0 AND @SEARCHCOUNTRY = 0 AND @SEARCHNEIGHBOORHOOD > 0 )
			BEGIN
				SET @WHERE = ' WHERE properties.Neighborhood LIKE ' + '''%'+ @SEARCH +'%'''
			END

			
			SET @QUERY_FINAL = @SELECT + @WHERE
		
			EXEC SP_EXECUTESQL  @QUERY_FINAL;";
			migrationBuilder.Sql(createProcSql);

		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

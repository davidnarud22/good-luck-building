﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class OwnerRepository : IOwnerRepository
    {
        private SharedContext _sharedContext;

        public OwnerRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }
        public Owner GetOwner(int id)
        {
            return _sharedContext.owners.Find(id);
        }
        public List<Owner> GetOwners()
        {
            return _sharedContext.owners.ToList();
        }

        public Owner AddOwner(Owner owner)
        {
            _sharedContext.owners.Add(owner);
            _sharedContext.SaveChanges();
            return owner;
        }

        public Owner EditOwner(Owner owner)
        {
            _sharedContext.Entry(owner).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return owner;
        }
        public void DeleteOwner(int id)
        {
            var ownerToDelete = _sharedContext.owners.Find(id);
            _sharedContext.owners.Remove(ownerToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class PropertyRepository : IPropertyRepository
    {
        private SharedContext _sharedContext;

        public PropertyRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }

        public Property GetProperty(int id)
        {
            return _sharedContext.properties.Find(id);
        }

        public List<Property> GetPropertys()
        {
            return _sharedContext.properties.ToList();
        }

        public Property AddProperty(Property property)
        {
            _sharedContext.properties.Add(property);
            _sharedContext.SaveChanges();
            return property;
        }

        public Property EditProperty(Property property)
        {
            _sharedContext.Entry(property).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return property;
        }

        public void DeleteProperty(int id)
        {
            var propertyToDelete = _sharedContext.properties.Find(id);
            _sharedContext.properties.Remove(propertyToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

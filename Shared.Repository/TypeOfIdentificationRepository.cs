﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class TypeOfIdentificationRepository : ITypeOfIdentificationRepository
    {
        private SharedContext _sharedContext;

        public TypeOfIdentificationRepository(SharedContext sharedContext)
        {
            _sharedContext = sharedContext;
        }

        public TypeOfIdentification GetTypeOfIdentification(int id)
        {
            return _sharedContext.typeOfIdentifications.Find(id);
        }

        public List<TypeOfIdentification> GetTypeOfIdentifications()
        {
            return _sharedContext.typeOfIdentifications.ToList();
        }

        public TypeOfIdentification AddTypeOfIdentification(TypeOfIdentification typeOfIdentification)
        {
            _sharedContext.typeOfIdentifications.Add(typeOfIdentification);
            _sharedContext.SaveChanges();
            return typeOfIdentification;
        }

        public TypeOfIdentification EditTypeOfIdentification(TypeOfIdentification typeOfIdentification)
        {
            _sharedContext.Entry(typeOfIdentification).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return typeOfIdentification;
        }

        public void DeleteTypeOfIdentification(int id)
        {
            var typeOfIdentificationtoDelete = _sharedContext.typeOfIdentifications.Find(id);
            _sharedContext.typeOfIdentifications.Remove(typeOfIdentificationtoDelete);
            _sharedContext.SaveChanges();
        }

    }
}

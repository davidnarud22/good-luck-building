﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Repository
{
    public class ProjectRepository : IProjectRepository
    {
        private SharedContext _sharedContext;

        public ProjectRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;

        }

        public Project GetProject(int id)
        {
            return _sharedContext.projects.Find(id);
        }

        public List<Project> GetProjects()
        {
            return _sharedContext.projects.ToList();
        }

        public Project AddProject(Project project)
        {
            _sharedContext.projects.Add(project);
            _sharedContext.SaveChanges();
            return project;
        }

        public Project EditProject(Project project)
        {
            _sharedContext.Entry(project).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return project;
        }
        public void DeleteProject(int id)
        {
            var projectToDelete = _sharedContext.projects.Find(id);
            _sharedContext.projects.Remove(projectToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class PurchaserRepository : IPurchaserRepository
    {
        private SharedContext _sharedContext;

        public PurchaserRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }

        public Purchaser GetPurchaser(int id)
        {
            return _sharedContext.purchasers.Find(id);
        }

        public List<Purchaser> GetPurchasers()
        {
            return _sharedContext.purchasers.ToList();
        }

        public Purchaser AddPurchaser(Purchaser purchaser)
        {
            _sharedContext.purchasers.Add(purchaser);
            _sharedContext.SaveChanges();
            return purchaser;
        }

        public Purchaser EditPurchaser(Purchaser purchaser)
        {
            _sharedContext.Entry(purchaser).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return purchaser;
        }

        public void DeletePurchaser(Purchaser purchaser)
        {
            _sharedContext.purchasers.Remove(purchaser);
            _sharedContext.SaveChanges();
        }
    }
}

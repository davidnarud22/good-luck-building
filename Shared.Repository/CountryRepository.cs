﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;
using System.Linq;


namespace Shared.Repository
{
    public class CountryRepository : ICountryRepository
    {
        private SharedContext _sharedContext;

        public CountryRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }

        public Country GetCountry(int id)
        {
            return _sharedContext.countries.Find(id);
        }
        public List<Country> GetCountries()
        {
            return _sharedContext.countries.ToList();
        }

        public Country AddCountry(Country country)
        {
            _sharedContext.countries.Add(country);
            _sharedContext.SaveChanges();
            return country;
        }

        public Country EditCountry(Country country)
        {
            _sharedContext.Entry(country).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return country;
        }

        public void DeleteCountry(int id)
        {
            var countryToDelete = _sharedContext.countries.Find(id);
            _sharedContext.countries.Remove(countryToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

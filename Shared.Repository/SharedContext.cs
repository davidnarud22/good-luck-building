﻿using Microsoft.EntityFrameworkCore;
using Shared.Models;

namespace Shared.Repository
{
    public class SharedContext : DbContext
    {
        public SharedContext(DbContextOptions<SharedContext> options) : base(options)
        {

        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Country> countries{ get; set; }
        public DbSet<Department> departments { get; set; }
        public DbSet<Macroproject> macroprojects { get; set; }
        public DbSet<Project> projects { get; set; }
        public DbSet<Property> properties { get; set; }
        public DbSet<Owner> owners{ get; set; }
        public DbSet<PropertyImage> propertyImages{ get; set; }
        public DbSet<Purchaser> purchasers{ get; set; }
        public DbSet<TypeOfProperty> typeOfProperties{ get; set; }
        public DbSet<TypeOfPayment> typeOfPayments{ get; set; }
        public DbSet<TypeOfIdentification> typeOfIdentifications{ get; set; }
        public DbSet<StateOfProperty> stateOfProperties { get; set; }

    }
}

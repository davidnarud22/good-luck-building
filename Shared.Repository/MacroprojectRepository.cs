﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Repository
{
    public class MacroprojectRepository : IMacroprojectRepository
    {
        private SharedContext _sharedContext;

        public MacroprojectRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }
        public Macroproject GetMacroproject(int id)
        {
            return _sharedContext.macroprojects.Find(id);
        }
        public List<Macroproject> GetMacroprojects()
        {
            return _sharedContext.macroprojects.ToList();
        }

        public Macroproject AddMacroproject(Macroproject macroproject)
        {
            _sharedContext.macroprojects.Add(macroproject);
            _sharedContext.SaveChanges();
            return macroproject;
        }

        public Macroproject EditMacroproject(Macroproject macroproject)
        {
            _sharedContext.Entry(macroproject).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return macroproject;
        }
        public void DeleteMacroproject(int id)
        {
            var MacroprojectToDelete = _sharedContext.macroprojects.Find(id);
            _sharedContext.macroprojects.Remove(MacroprojectToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

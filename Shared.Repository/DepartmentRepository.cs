﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private SharedContext _sharedContext;

        public DepartmentRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }
        public Department GetDepartment(int id)
        {
            return _sharedContext.departments.Find(id);
        }

        public List<Department> GetDepartments()
        {
            return _sharedContext.departments.ToList();
        }
        public Department AddDepartment(Department department)
        {
            _sharedContext.departments.Add(department);
            _sharedContext.SaveChanges();
            return department;
        }

        public Department EditDepartment(Department department)
        {
            _sharedContext.Entry(department).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return department;
        }

        public void DeleteDepartment(int id)
        {
            var departmentToDelete = _sharedContext.departments.Find(id);
            _sharedContext.departments.Remove(departmentToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Repository
{
    public class CityRepository : ICityRepository
    {
        private SharedContext _sharedContext;

        public CityRepository(SharedContext sharedContex)
        {
            _sharedContext = sharedContex;
        }

        public City GetCity(int id)
        {
            return _sharedContext.Cities.Find(id);
        }

        public List<City> GetCities()
        {
            return _sharedContext.Cities.ToList();
        }

        public City AddCity(City city)
        {
            _sharedContext.Cities.Add(city);
            _sharedContext.SaveChanges();
            return city;
        }

        public City EditCity(City city)
        {
            _sharedContext.Entry(city).State = EntityState.Modified;
            _sharedContext.SaveChanges();
            return city;
        }

        public void DeleteCity(int id)
        {
            var CityToDelete = _sharedContext.Cities.Find(id);
            _sharedContext.Cities.Remove(CityToDelete);
            _sharedContext.SaveChanges();
        }
    }
}

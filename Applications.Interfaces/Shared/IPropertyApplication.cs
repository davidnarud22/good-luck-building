﻿using Shared.Models;
using System.Collections.Generic;

namespace Applications.Interfaces.Shared
{
    public interface IPropertyApplication
    {
        List<PropertyForView> GetPropertyForViewsForConstructor(FiltersProperty filter);

        List<PropertyForView> GetPropertyForViewsForNaturalPerson(FiltersProperty filter);

        List<PropertyForView> GetPropertyForViewsForSearch(string search);

        List<int> getNumberOfBathrooms();

        List<int> getNumberOfRooms();

        List<int> getNumberOfFloors();

        List<int> getPrices();

        List<int> getAreas();

        List<int> getYears();
    }
}

﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces.Shared
{
    public interface IDepartmentApplication
    {
        List<Department> GetDepartmentsForCity(int id);
    }
}

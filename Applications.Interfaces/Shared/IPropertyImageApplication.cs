﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces.Shared
{
    public interface IPropertyImageApplication
    {
        int GetLastOrderOfImagenForProyect(int id);

        List<PropertyImage> GetPropertyImagesForProperty(int id);
    }
}

﻿using Authentication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces.Authentication
{
    public interface IJsonWebTokenService
    {
        string CreateToken(User user, string role);
    }
}

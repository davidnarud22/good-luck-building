﻿using Authentication.Models.Models;

namespace Applications.Interfaces.Authentication
{
    public interface ILogin
    {
        TokenAndRole Authentication(LoginUser loginUser);
    }
}

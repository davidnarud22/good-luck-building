﻿using Authentication.Models;
using Authentication.Models.Models;

namespace Applications.Interfaces.Authentication
{
    public interface IUserApplication
    {
        User getUserForEmail(string email);

    }
}

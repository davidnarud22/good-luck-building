﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class FiltersProperty
    {
        public int numberOfBathrooms { get; set; }
        public int MAJORORMINOROFBATHROOMS { get; set; }
        public int numberOfFloors { get; set; }
        public int MAJORORMINOROFFLOORS { get; set; }
        public int numberOfRooms { get; set; }
        public int MAJORORMINOROFROOMS { get; set; }
        public int area { get; set; }
        public int MAJORORMINOROAREA { get; set; }
        public int state { get; set; }
        public int year { get; set; }
        public int MAJORORMINOROYEAR { get; set; }
        public int price { get; set; }
        public int MAJORORMINOROPRICE { get; set; }
        public int idTypeOfProperty { get; set; }
    }
}

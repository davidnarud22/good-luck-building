﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shared.Models
{
    public class Property
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        public int? IdProject { get; set; }

        [Required]
        public int IdTypeOfProperty { get; set; }

        [Required]
        public int IdDepartment { get; set; }

        [Required]
        public string Neighborhood { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public int Area { get; set; }

        [Required]
        public int NumberOfFloors { get; set; }

        [Required]
        public int NumberOfRooms { get; set; }

        [Required]
        public int NumberOfBathrooms { get; set; }

        [Required]
        public int CodeInternal { get; set; }

        [Required]
        public int Year { get; set; }

        public int State { get; set; }

        [Required]
        public int? IdOwner { get; set; }

        [ForeignKey("IdOwner")]
        protected virtual Owner owner { get; set; }
    }
}

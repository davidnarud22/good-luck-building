﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shared.Models
{
    public class Project
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int? IdOwner { get; set; }

        [Required]
        public int IdMacroProject { get; set; }

        [Required]
        public int IdCity { get; set; }

        [Required]
        public int IdDepartment { get; set; }

        [Required]
        public int IdCountry { get; set; }
    }
}

﻿namespace Shared.Models
{
    public class PropertyForView
    {
        public int id { get; set; }

        public string name { get; set; }

        public string address { get; set; }

        public string neighborhood { get; set; }

        public int? idProject { get; set; }

        public string nameProject { get; set; }

        public string department { get; set; }

        public string city { get; set; }

        public string country { get; set; }

        public int idTypeOfProperty { get; set; }

        public string typeProperty { get; set; }

        public int price { get; set; }

        public int area { get; set; }

        public int numberOfFloors { get; set; }

        public int numberOfRooms { get; set; }

        public int numberOfBathrooms { get; set; }

        public int codeInternal { get; set; }

        public int year { get; set; }

        public int state { get; set; }

        public string stateProperty { get; set; }

        public int? idOwner { get; set; }

        public string owner { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class Owner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FullName { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        [Required]
        public string FirstSurName { get; set; }

        public string SecondSurName { get; set; }

        public string Address{ get; set; }

        public string Photo { get; set; }

        public DateTime DateOfBirth { get; set; }

        [Required]
        public string IdentificactionNumber { get; set; }

        [Required]
        public int IdTypeOfIdentification{ get; set; }

        public string Email { get; set; }

        public string CelPhone { get; set; }
    }
}

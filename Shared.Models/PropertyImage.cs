﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shared.Models
{
    public class PropertyImage
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string File { get; set; }

        [Required]
        public Boolean Enable { get; set; }

        public int IdProperty { get; set; }

        public string nameProperty { get; set; }

        public int order { get; set; }

        [ForeignKey("IdProperty")]
        protected virtual Property property { get; set; }

    }
}

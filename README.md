# Good Luck Building



## Getting started

Pasos a realizar antes de ejecutar el proyecto {

	Ejecutar Scripts sql en el siguiente orden
	1.Query Create DataBase.sql
	2.Query All SCHEMA and DATA.sql

	En la carpeta "web" dentro de la carpeta good luck building ejecutar el comando Npm install.
}

En el repositorio se incluye una archivo .pem porfavor descargarlo y la ruta en donde se deja este archivo porfavor modificarla en el archivo appsettings.json
en la propiedad Path del objeto PemCertificate 

 "PemCertificate": {
    "Path": "C:\\Users\\David\\source\\repos\\Good Luck Building\\GoodLuckBuilding.pem"
  },


Para ejecutar el proyecto primero ejecute la API Good Luck Building, siguiente a esto en la carpeta "web" que esta en la carpeta
raiz good luck building ejecutar el comando npm start

Para ingresar a las opciones de administracion del proyecto ingresar con el usuario tipo "Administrator" el correo de este usuario es "administrator@goodLuckbuilding.com"
Para ingresar a la opcion de visualizacion de las propiedades ingresar con el usuario tipo "User" el correo de este usuario es "user@goodluckbuilding.com"



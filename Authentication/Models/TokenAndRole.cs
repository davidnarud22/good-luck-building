﻿namespace Authentication.Models.Models
{
    public class TokenAndRole
    {
        public string Token { get; set; }

        public string Role { get; set; }
    }
}

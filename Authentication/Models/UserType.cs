﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Authentication.Models
{
    public class UserType
    {

        [Key]
        [Required]
        public int IdUserType { get; set; }

        [Required]
        public string Name { get; set; }

    }
}

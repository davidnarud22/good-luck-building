﻿namespace Authentication.Models.Models
{
    public class LoginUser
    {
        public string email { get; set; }

        public string password { get; set; }
    }
}

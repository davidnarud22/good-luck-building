﻿using APIPeopleRisk.Utils;
using Applications.Interfaces.Authentication;
using Authentication.Models;
using Authentication.Models.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace APIPeopleRisk.Infrastructure.Services
{
    public class JsonWebTokenService : IJsonWebTokenService
    {
        private readonly IConfiguration _configuration;
        private readonly RSACryptographyHelper _RSACryptographyHelper;

        public JsonWebTokenService(IConfiguration configuration)
        {
            _configuration = configuration;
            _RSACryptographyHelper = new RSACryptographyHelper(configuration);
        }

        public string CreateToken(User user,string role)
        {
            JwtSecurityTokenHandler tokenHandler = new();
            var key = Encoding.ASCII.GetBytes(_configuration["JWTConfig:Key"]);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Name, _RSACryptographyHelper.KeyPairEncrypt(user.Email)),
                new Claim(ClaimTypes.NameIdentifier, _configuration["JWTConfig:NameIdentifier"]),
                new Claim(ClaimTypes.Role,role)
            }),
                Expires = DateTime.UtcNow.AddMinutes(int.Parse(_configuration["JWTConfig:Expiration"])),
                SigningCredentials = new(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace APIPeopleRisk.Utils
{
    public class RSACryptographyHelper
    {
        private readonly RSACryptoServiceProvider RSACryptoService;
        private readonly IConfiguration _configuration;


        public RSACryptographyHelper(IConfiguration configuration)
        {
            RSACryptoService = new RSACryptoServiceProvider(2048);
            _configuration = configuration;
        }


        /// <summary> Get the public and private key from pem keypair file </summary>
        /// <returns> An object with the public and private key </returns>
        private AsymmetricCipherKeyPair GetCipherKeyPair()
        {
            StreamReader fr = File.OpenText(_configuration["PemCertificate:Path"]);
            PemReader pemReader = new PemReader(fr);



            return (AsymmetricCipherKeyPair)pemReader.ReadObject();
        }



        /// <summary> Encrypt the data using the keys from pem keypair file </summary>
        /// <param name="textToEncrypt"> String wich will be encrypted </param>
        /// <returns> Encrypted string with RSA </returns>
        public string KeyPairEncrypt(string textToEncrypt)
        {
            RSACryptoService.ImportParameters(DotNetUtilities.ToRSAParameters((RsaKeyParameters)GetCipherKeyPair().Public));
            byte[] dataEncrypt = RSACryptoService.Encrypt(Encoding.UTF8.GetBytes(textToEncrypt), false);



            return Convert.ToBase64String(dataEncrypt);
        }



        /// <summary> Decrypt the data using the keys from pem keypair file </summary>
        /// <param name="textToDecrypt"> String in base64 to be Decrypted </param>
        /// <returns> Decrypted string </returns>
        public string KeyPairDecrypt(string textToDecrypt)
        {
            RSACryptoService.ImportParameters(DotNetUtilities.ToRSAParameters((RsaPrivateCrtKeyParameters)GetCipherKeyPair().Private));
            byte[] dataDecrypt = RSACryptoService.Decrypt(Convert.FromBase64String(textToDecrypt), false);



            return Encoding.UTF8.GetString(dataDecrypt);
        }
    }
}
﻿namespace APIPeopleRisk.Utils
{
    public static class BCryptHelper
    {
        public static string HashText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            return BCrypt.Net.BCrypt.HashPassword(text);
        }

        public static bool VerifyHash(string verifyText, string verifyHash)
        {
            if (string.IsNullOrEmpty(verifyText) || string.IsNullOrEmpty(verifyHash))
                return false;

            return BCrypt.Net.BCrypt.Verify(verifyText, verifyHash);
        }
    }
}

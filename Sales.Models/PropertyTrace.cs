﻿using Shared.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sales.Models
{
    public class PropertyTrace
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public DateTime DateSale { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Value { get; set; }

        [Required]
        public int Tax { get; set; }

        public int? DownPayment { get; set; }

        [Required]
        public int IdProperty { get; set; }

        [ForeignKey("IdProperty")]
        protected virtual Property property { get; set; }

        [Required]
        public int IdTypeOfPayment { get; set; }

        [Required]
        public int IdNewOwner { get; set; }

        public int? IdPaymentSchedule { get; set; }

        [ForeignKey("IdPaymentSchedule")]
        protected virtual PaymentSchedule paymentSchedule { get; set; }

        [Required]
        public int IdQuote { get; set; }
    }
}

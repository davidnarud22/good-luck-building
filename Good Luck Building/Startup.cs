using APIPeopleRisk.Infrastructure.Services;
using Applications.Authentication;
using Applications.Interfaces.Authentication;
using Applications.Interfaces.Shared;
using Applications.Shared;
using AuthenticationRepository;
using Formalities.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Repository.Interfaces.Authentication;
using Repository.Interfaces.Formalities;
using Repository.Interfaces.PaymentSchedules;
using Repository.Interfaces.Sales;
using Repository.Interfaces.SalesRoom;
using Repository.Interfaces.Shared;
using Sales.Repository;
using SalesRoom.Repository;
using Shared.Repository;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Good_Luck_Building
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<AuthenticationContext>(options => options.UseSqlServer(
              Configuration.GetConnectionString("ConnectionString")));

            services.AddDbContextPool<SharedContext>(options => options.UseSqlServer(
              Configuration.GetConnectionString("ConnectionString")));

            services.AddDbContextPool<SalesRoomContext>(options => options.UseSqlServer(
              Configuration.GetConnectionString("ConnectionString")));

            services.AddDbContextPool<SalesContext>(options => options.UseSqlServer(
              Configuration.GetConnectionString("ConnectionString")));

            services.AddDbContextPool<FormalitiesContext>(options => options.UseSqlServer(
              Configuration.GetConnectionString("ConnectionString")));

            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<IMacroprojectRepository, MacroprojectRepository>();
            services.AddScoped<IOwnerRepository, OwnerRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IPropertyImageRepository, PropertyImageRepository>();
            services.AddScoped<IPropertyRepository, PropertyRepository>();
            services.AddScoped<IPurchaserRepository, PurchaserRepository>();
            services.AddScoped<ITypeOfPaymentRepository, TypeOfPaymentRepository>();
            services.AddScoped<ITypeOfPropertyRepository, TypeOfPropertyRepository>();
            services.AddScoped<IVisitRepository, VisitRepository>();
            services.AddScoped<IQuoteRepository, QuoteRepository>();
            services.AddScoped<IVisitRepository, VisitRepository>();
            services.AddScoped<IPaymentScheduleRepository, PaymentScheduleRepository>();
            services.AddScoped<IPropertyTraceRepository, PropertyTraceRepository>();
            services.AddScoped<IProcedureRepository, ProcedureRepository>();
            services.AddScoped<ITypeOfIdentificationRepository, TypeOfIdentificationRepository>();
            services.AddScoped<IStateOfPropertyRepository, StatesOfPropertyRepository>();
            services.AddScoped<IUserTypeRepository, UserTypeRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ICityApplication, CityApplication>();
            services.AddScoped<IDepartmentApplication, DepartmentApplication>();
            services.AddScoped<IProjectApplication, ProjectApplication>();
            services.AddScoped<IPropertyImageApplication, PropertyImageApplication>();
            services.AddScoped<IPropertyApplication, PropertyApplication>();
            services.AddScoped<IUserTypeApplication, UserTypeApplication>();
            services.AddScoped<IUserApplication, UserApplication>();
            services.AddScoped<IJsonWebTokenService, JsonWebTokenService>();
            services.AddScoped<ILogin, Login>();


            services.AddControllers();
            services.AddSwaggerGen(doc =>
            {
                doc.SwaggerDoc("v1", new OpenApiInfo
                {
                    Contact = new OpenApiContact()
                    {
                        Name = "Good Luck Building",
                    },
                    Title = "Good Luck Building",
                    Version = "v1",
                    Description = "Good Luck Building"
                });

                var xmlPath = Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                doc.IncludeXmlComments(xmlPath);

                doc.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Authorization: Bearer {token}"
                });

                doc.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                              new OpenApiSecurityScheme
                                {
                                    Reference = new OpenApiReference
                                    {
                                        Type = ReferenceType.SecurityScheme,
                                        Id = "Bearer"
                                    }
                                },
                                Array.Empty<string>()
                        }
                    });
            });
            services.AddCors();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = TimeSpan.Zero,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWTConfig:Key"]))
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Good_Luck_Building v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseCors(options =>
            {
                options.AllowAnyOrigin(); options.AllowAnyMethod(); options.AllowAnyHeader();
            }
           );

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

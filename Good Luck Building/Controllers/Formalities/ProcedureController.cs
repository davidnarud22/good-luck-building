﻿using Formalities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Formalities;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Formalities
{
    [Route("Procedure")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class ProcedureController : ControllerBase
    {
        private IProcedureRepository _procedureRepository;
        public ProcedureController(IProcedureRepository procedureRepository)
        {
            _procedureRepository = procedureRepository;
        }

        [HttpGet]
        public Procedure GetProcedure(int id)
        {
            return _procedureRepository.GetProcedure(id);
        }

        [HttpGet]
        [Route("GetProcedures")]
        public List<Procedure> GetProcedures()
        {
            return _procedureRepository.GetProcedures();
        }

        [HttpPost]
        [Route("AddProcedure")]
        public Procedure AddProcedure(Procedure procedure)
        {
            return _procedureRepository.AddProcedure(procedure);
        }

        [HttpPut]
        [Route("EditProcedure")]
        public Procedure EditProcedure(Procedure procedure)
        {
            return _procedureRepository.EditProcedure(procedure);
        }

        [HttpDelete]
        [Route("DeleteProcedure")]
        public void DeleteProcedure(Procedure procedure)
        {
            _procedureRepository.DeleteProcedure(procedure);
        }
    }
}

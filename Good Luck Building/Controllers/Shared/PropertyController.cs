﻿using Applications.Interfaces.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("Property")]
    [ApiController]

    public class PropertyController : ControllerBase
    {
        private IPropertyRepository _propertyRepository;
        private IPropertyApplication _propertyApplication;

        public PropertyController(IPropertyRepository propertyRepository, IPropertyApplication propertyApplication)
        {
            _propertyRepository = propertyRepository;
            _propertyApplication = propertyApplication;
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public Property GetProperty(int id)
        {
            return _propertyRepository.GetProperty(id);
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("GetPropertys")]
        public List<Property> GetPropertys()
        {
            return _propertyRepository.GetPropertys();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("getNumberOfRooms")]
        public List<int> getNumberOfRooms()
        {
            return _propertyApplication.getNumberOfRooms();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("getNumberOfFloors")]
        public List<int> getNumberOfFloors()
        {
            return _propertyApplication.getNumberOfFloors();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("getNumberOfBathrooms")]
        public List<int> getNumberOfBathrooms()
        {
            return _propertyApplication.getNumberOfBathrooms();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("getPrices")]
        public List<int> getPrices()
        {
            return _propertyApplication.getPrices();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("getAreas")]
        public List<int> getAreas()
        {
            return _propertyApplication.getAreas();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("getYears")]
        public List<int> getYears()
        {
            return _propertyApplication.getYears();
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpPost]
        [Route("GetPropertyForViewsForConstructor")]
        public List<PropertyForView> GetPropertyForViewsForConstructor([FromBody] FiltersProperty filter)
        {
            return _propertyApplication.GetPropertyForViewsForConstructor(filter);
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpPost]
        [Route("GetPropertyForViewsForNaturalPerson")]
        public List<PropertyForView> GetPropertyForViewsForNaturalPerson([FromBody] FiltersProperty filter)
        {
            return _propertyApplication.GetPropertyForViewsForNaturalPerson(filter);
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpPost]
        [Route("GetPropertyForViewsForSearch/{search}")]
        public List<PropertyForView> GetPropertyForViewsForSearch(string search)
        {
            return _propertyApplication.GetPropertyForViewsForSearch(search);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("AddProperty")]
        public Property AddProperty(Property property)
        {
            return _propertyRepository.AddProperty(property);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut]
        [Route("EditProperty")]
        public Property EditProperty(Property property)
        {
            return _propertyRepository.EditProperty(property);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("DeleteProperty/{id}")]
        public void DeleteProperty(int id)
        {
            _propertyRepository.DeleteProperty(id);
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("Purchaser")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class PurchaserController : ControllerBase
    {
        private IPurchaserRepository _purchaserRepository;
        public PurchaserController(IPurchaserRepository purchaserRepository)
        {
            _purchaserRepository = purchaserRepository;
        }

        [HttpGet]
        public Purchaser GetPurchaser(int id)
        {
            return _purchaserRepository.GetPurchaser(id);
        }

        [HttpGet]
        [Route("GetPurchasers")]
        public List<Purchaser> GetPurchasers()
        {
            return _purchaserRepository.GetPurchasers();
        }

        [HttpPost]
        [Route("AddPurchaser")]
        public Purchaser AddPurchaser(Purchaser purchaser)
        {
            return _purchaserRepository.AddPurchaser(purchaser);
        }

        [HttpPut]
        [Route("EditPurchaser")]
        public Purchaser EditPurchaser(Purchaser purchaser)
        {
            return _purchaserRepository.EditPurchaser(purchaser);
        }

        [HttpDelete]
        [Route("DeletePurchaser")]
        public void DeletePurchaser(Purchaser purchaser)
        {
            _purchaserRepository.DeletePurchaser(purchaser);
        }
    }
}

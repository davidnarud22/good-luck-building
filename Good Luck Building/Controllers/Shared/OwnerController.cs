﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("Owner")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class OwnerController : ControllerBase
    {
        private IOwnerRepository _ownerRepository;
        public OwnerController(IOwnerRepository ownerRepository)
        {
            _ownerRepository = ownerRepository;
        }

        [HttpGet]
        public Owner GetOwner(int id)
        {
            return _ownerRepository.GetOwner(id);
        }

        [HttpGet]
        [Route("GetOwners")]
        public List<Owner> GetOwners()
        {
            return _ownerRepository.GetOwners();
        }

        [HttpPost]
        [Route("AddOwner")]
        public Owner AddOwner(Owner owner)
        {
            return _ownerRepository.AddOwner(owner);
        }

        [HttpPut]
        [Route("EditOwner")]
        public Owner EditOwner(Owner owner)
        {
            return _ownerRepository.EditOwner(owner);
        }

        [HttpDelete]
        [Route("DeleteOwner/{id}")]
        public void DeleteOwner(int id)
        {
            _ownerRepository.DeleteOwner(id);
        }
    }
}

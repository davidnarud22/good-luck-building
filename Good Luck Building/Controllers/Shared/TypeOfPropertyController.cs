﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("TypeOfProperty")]
    [ApiController]

    public class TypeOfPropertyController : ControllerBase
    {
        private ITypeOfPropertyRepository _TypeOfPropertyRepository;
        public TypeOfPropertyController(ITypeOfPropertyRepository TypeOfPropertyRepository)
        {
            _TypeOfPropertyRepository = TypeOfPropertyRepository;
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public TypeOfProperty GetTypeOfProperty(int id)
        {
            return _TypeOfPropertyRepository.GetTypeOfProperty(id);
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("GetTypeOfPropertys")]
        public List<TypeOfProperty> GetTypeOfPropertys()
        {
            return _TypeOfPropertyRepository.GetTypeOfPropertys();
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("AddTypeOfProperty")]
        public TypeOfProperty AddTypeOfProperty(TypeOfProperty TypeOfProperty)
        {
            return _TypeOfPropertyRepository.AddTypeOfProperty(TypeOfProperty);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut]
        [Route("EditTypeOfProperty")]
        public TypeOfProperty EditTypeOfProperty(TypeOfProperty TypeOfProperty)
        {
            return _TypeOfPropertyRepository.EditTypeOfProperty(TypeOfProperty);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("DeleteTypeOfProperty/{id}")]
        public void DeleteTypeOfProperty(int id)
        {
            _TypeOfPropertyRepository.DeleteTypeOfProperty(id);
        }
    }
}

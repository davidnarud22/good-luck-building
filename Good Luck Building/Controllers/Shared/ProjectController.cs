﻿using Applications.Interfaces.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("Project")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class ProjectController : ControllerBase
    {
        private IProjectRepository _projectRepository;
        private IProjectApplication _projectApplication;

        public ProjectController(IProjectRepository projectRepository, IProjectApplication projectApplication)
        {
            _projectRepository = projectRepository;
            _projectApplication = projectApplication;
        }

        [HttpGet]
        [Route("GetProject/{id}")]
        public Project GetProject(int id)
        {
            return _projectRepository.GetProject(id);
        }

        [HttpGet]
        [Route("GetProjects")]
        public List<Project> GetProjects()
        {
            return _projectRepository.GetProjects();
        }


        [HttpGet]
        [Route("getProjectsForMacroProjects/{id}")]
        public List<Project> getProjectsForMacroProjects(int id)
        {
            return _projectApplication.getProjectsForMacroProjects(id);
        }

        [HttpPost]
        [Route("AddProject")]
        public Project AddProject(Project project)
        {
            return _projectRepository.AddProject(project);
        }

        [HttpPut]
        [Route("EditProject")]
        public Project EditProject(Project project)
        {
            return _projectRepository.EditProject(project);
        }

        [HttpDelete]
        [Route("DeleteProject/{id}")]
        public void DeleteProject(int id)
        {
            _projectRepository.DeleteProject(id);
        }
    }
}

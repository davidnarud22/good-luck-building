﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("TypeOfIdentification")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class TypeOfIdentificationController : ControllerBase
    {
        private ITypeOfIdentificationRepository _TypeOfIdentificationRepository;
        public TypeOfIdentificationController(ITypeOfIdentificationRepository TypeOfIdentificationRepository)
        {
            _TypeOfIdentificationRepository = TypeOfIdentificationRepository;
        }

        [HttpGet]
        public TypeOfIdentification GetTypeOfIdentification(int id)
        {
            return _TypeOfIdentificationRepository.GetTypeOfIdentification(id);
        }

        [HttpGet]
        [Route("GetTypeOfIdentifications")]
        public List<TypeOfIdentification> GetTypeOfIdentifications()
        {
            return _TypeOfIdentificationRepository.GetTypeOfIdentifications();
        }

        [HttpPost]
        [Route("AddTypeOfIdentification")]
        public TypeOfIdentification AddTypeOfIdentification(TypeOfIdentification TypeOfIdentification)
        {
            return _TypeOfIdentificationRepository.AddTypeOfIdentification(TypeOfIdentification);
        }

        [HttpPut]
        [Route("EditTypeOfIdentification")]
        public TypeOfIdentification EditTypeOfIdentification(TypeOfIdentification TypeOfIdentification)
        {
            return _TypeOfIdentificationRepository.EditTypeOfIdentification(TypeOfIdentification);
        }

        [HttpDelete]
        [Route("DeleteTypeOfIdentification/{id}")]
        public void DeleteTypeOfIdentification(int id)
        {
            _TypeOfIdentificationRepository.DeleteTypeOfIdentification(id);
        }
    }
}

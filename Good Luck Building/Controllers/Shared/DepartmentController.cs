﻿using Applications.Interfaces.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("Department")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class DepartmentController : ControllerBase
    {
        private IDepartmentRepository _departmentRepository;
        private IDepartmentApplication _departmentApplication;


        public DepartmentController(IDepartmentRepository departmentRepository, IDepartmentApplication departmentApplication)
        {
            _departmentRepository = departmentRepository;
            _departmentApplication = departmentApplication;

        }

        [HttpGet]
        public Department GetDepartment(int id)
        {
            return _departmentRepository.GetDepartment(id);
        }

        [HttpGet]
        [Route("GetDepartments")]
        public List<Department> GetDepartments()
        {
            return _departmentRepository.GetDepartments();
        }

        [HttpGet]
        [Route("GetDepartmentsForCity/{id}")]
        public List<Department> GetDepartmentsForCity(int id)
        {
            return _departmentApplication.GetDepartmentsForCity(id);
        }

        [HttpPost]
        [Route("AddDepartment")]
        public Department AddDepartment(Department department)
        {
            return _departmentRepository.AddDepartment(department);
        }

        [HttpPut]
        [Route("EditDepartment")]
        public Department EditDepartment(Department department)
        {
            return _departmentRepository.EditDepartment(department);

        }

        [HttpDelete]
        [Route("DeleteDepartment/{id}")]
        public void DeleteDepartment(int id)
        {
            _departmentRepository.DeleteDepartment(id);
        }
    }
}

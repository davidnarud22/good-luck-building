﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("TypeOfPayment")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class TypeOfPaymentController : ControllerBase
    {
        private ITypeOfPaymentRepository _TypeOfPaymentRepository;
        public TypeOfPaymentController(ITypeOfPaymentRepository TypeOfPaymentRepository)
        {
            _TypeOfPaymentRepository = TypeOfPaymentRepository;
        }

        [HttpGet]
        public TypeOfPayment GetTypeOfPayment(int id)
        {
            return _TypeOfPaymentRepository.GetTypeOfPayment(id);
        }

        [HttpGet]
        [Route("GetTypeOfPayments")]
        public List<TypeOfPayment> GetTypeOfPayments()
        {
            return _TypeOfPaymentRepository.GetTypeOfPayments();
        }

        [HttpPost]
        [Route("AddTypeOfPayment")]
        public TypeOfPayment AddTypeOfPayment(TypeOfPayment TypeOfPayment)
        {
            return _TypeOfPaymentRepository.AddTypeOfPayment(TypeOfPayment);
        }

        [HttpPut]
        [Route("EditTypeOfPayment")]
        public TypeOfPayment EditTypeOfPayment(TypeOfPayment TypeOfPayment)
        {
            return _TypeOfPaymentRepository.EditTypeOfPayment(TypeOfPayment);
        }

        [HttpDelete]
        [Route("DeleteTypeOfPayment/{id}")]
        public void DeleteTypeOfPayment(int id)
        {
            _TypeOfPaymentRepository.DeleteTypeOfPayment(id);
        }
    }
}

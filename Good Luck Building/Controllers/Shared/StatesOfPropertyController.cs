﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("StateOfProperty")]
    [ApiController]
    

    public class StateOfPropertyController : ControllerBase
    {
        private IStateOfPropertyRepository _StateOfPropertyRepository;
        public StateOfPropertyController(IStateOfPropertyRepository StateOfPropertyRepository)
        {
            _StateOfPropertyRepository = StateOfPropertyRepository;
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public StateOfProperty GetStateOfProperty(int id)
        {
            return _StateOfPropertyRepository.GetStateOfProperty(id);
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("GetStateOfPropertys")]
        public List<StateOfProperty> GetStateOfPropertys()
        {
            return _StateOfPropertyRepository.GetStateOfPropertys();
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("AddStateOfProperty")]
        public StateOfProperty AddStateOfProperty(StateOfProperty StateOfProperty)
        {
            return _StateOfPropertyRepository.AddStateOfProperty(StateOfProperty);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut]
        [Route("EditStateOfProperty")]
        public StateOfProperty EditStateOfProperty(StateOfProperty StateOfProperty)
        {
            return _StateOfPropertyRepository.EditStateOfProperty(StateOfProperty);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("DeleteStateOfProperty/{id}")]
        public void DeleteStateOfProperty(int id)
        {
            _StateOfPropertyRepository.DeleteStateOfProperty(id);
        }
    }
}

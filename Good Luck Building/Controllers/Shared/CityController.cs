﻿using Applications.Interfaces.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Produces("application/json")]
    [Route("City")]
    [ApiController]
    [Authorize(Roles = "Administrator")]


    public class CityController : ControllerBase
    {
        private ICityRepository _cityRepository;
        private ICityApplication _cityApplication;
        public CityController(ICityRepository cityRepository, ICityApplication cityApplication)
        {
            _cityRepository = cityRepository;
            _cityApplication = cityApplication;
        }

        [HttpGet]
        public City GetCity(int id)
        {
            return _cityRepository.GetCity(id);
        }

        [HttpGet]
        [Route("GetCities")]
        public List<City> GetCities()
        {
            return _cityRepository.GetCities();
        }

        [HttpGet]
        [Route("GetCitiesForCountry/{id}")]
        public List<City> GetCitiesForCountry(int id)
        {
            return _cityApplication.getCitiesForCountry(id);
        }

        [HttpPost]
        [Route("AddCity")]
        public City AddCity(City city)
        {
            return _cityRepository.AddCity(city);
        }

        [HttpPut]
        [Route("EditCity")]
        public City EditCity(City city)
        {
            return _cityRepository.EditCity(city);
        }

        [HttpDelete]
        [Route("DeleteCity/{id}")]
        public void DeleteCity(int id)
        {
            _cityRepository.DeleteCity(id);
        }

    }
}

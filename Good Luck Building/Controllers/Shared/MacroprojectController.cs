﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{

    [Route("Macroproject")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class MacroprojectController : ControllerBase
    {
        private IMacroprojectRepository _macroprojectRepository;
        public MacroprojectController(IMacroprojectRepository macroprojectRepository)
        {
            _macroprojectRepository = macroprojectRepository;
        }

        [HttpGet]
        public Macroproject GetMacroproject(int id)
        {
            return _macroprojectRepository.GetMacroproject(id);
        }

        [HttpGet]
        [Route("GetMacroprojects")]
        public List<Macroproject> GetMacroprojects()
        {
            return _macroprojectRepository.GetMacroprojects();
        }

        [HttpPost]
        [Route("AddMacroproject")]
        public Macroproject AddMacroproject(Macroproject macroproject)
        {
            return _macroprojectRepository.AddMacroproject(macroproject);
        }

        [HttpPut]
        [Route("EditMacroproject")]
        public Macroproject EditMacroproject(Macroproject macroproject)
        {
            return _macroprojectRepository.EditMacroproject(macroproject);
        }

        [HttpDelete]
        [Route("DeleteMacroproject/{id}")]
        public void DeleteMacroproject(int id)
        {
            _macroprojectRepository.DeleteMacroproject(id);
        }
    }
}

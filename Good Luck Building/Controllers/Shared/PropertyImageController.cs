﻿using Applications.Interfaces.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("PropertyImage")]
    [ApiController]

    public class PropertyImageController : ControllerBase
    {
        private IPropertyImageRepository _propertyImageRepository;
        private IPropertyImageApplication _propertyImageApplication;
        public PropertyImageController(IPropertyImageRepository propertyImageRepository, IPropertyImageApplication propertyImageApplication)
        {
            _propertyImageRepository = propertyImageRepository;
            _propertyImageApplication = propertyImageApplication;
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public PropertyImage GetPropertyImage(int id)
        {
            return _propertyImageRepository.GetPropertyImage(id);
        }

        [Authorize(Roles = "Administrator,User")]
        [HttpGet]
        [Route("GetPropertyImages")]
        public List<PropertyImage> GetPropertyImages()
        {
            return _propertyImageRepository.GetPropertyImages();
        }

        [HttpGet]
        [Route("GetLastOrderOfImagenForProyect/{id}")]
        [Authorize(Roles = "Administrator,User")]
        public int GetLastOrderOfImagenForProyect(int id)
        {
            return _propertyImageApplication.GetLastOrderOfImagenForProyect(id);
        }

        [HttpGet]
        [Route("GetPropertyImagesForProperty/{id}")]
        [Authorize(Roles = "Administrator,User")]
        public List<PropertyImage> GetPropertyImagesForProperty(int id)
        {
            return _propertyImageApplication.GetPropertyImagesForProperty(id);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("AddPropertyImage")]
        public PropertyImage AddPropertyImage(PropertyImage propertyImage)
        {
            return _propertyImageRepository.AddPropertyImage(propertyImage);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("AddPropertyImages")]
        public List<PropertyImage> AddPropertyImages(List<PropertyImage> propertyImage)
        {
            return _propertyImageRepository.AddPropertyImages(propertyImage);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut]
        [Route("EditPropertyImage")]
        public PropertyImage EditPropertyImage(PropertyImage propertyImage)
        {
            return _propertyImageRepository.EditPropertyImage(propertyImage);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("DeletePropertyImage/{id}")]
        public void DeletePropertyImage(int id)
        {
            _propertyImageRepository.DeletePropertyImage(id);
        }
    }
}

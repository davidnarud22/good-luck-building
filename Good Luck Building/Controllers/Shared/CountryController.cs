﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Shared;
using Shared.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Shared
{
    [Route("Country")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class CountryController : ControllerBase
    {
        private ICountryRepository _countryRepository;
        public CountryController(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }

        [HttpGet]
        public Country GetCountry(int id)
        {
            return _countryRepository.GetCountry(id);
        }

        [HttpGet]
        [Route("GetCountries")]
        public List<Country> GetCountries()
        {
            return _countryRepository.GetCountries();
        }

        [HttpPost]
        [Route("AddCountry")]
        public Country AddCountry(Country country)
        {
            return _countryRepository.AddCountry(country);
        }

        [HttpPut]
        [Route("EditCountry")]
        public Country EditCountry(Country country)
        {
            return _countryRepository.EditCountry(country);
        }

        [HttpDelete]
        [Route("DeleteCountry/{id}")]
        public void DeleteCountry(int id)
        {
            _countryRepository.DeleteCountry(id);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Sales;
using Sales.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Sales
{
    [Route("PropertyTrace")]
    [ApiController]
    public class PropertyTraceController : ControllerBase
    {
        private IPropertyTraceRepository _propertyTraceRepository;
        public PropertyTraceController(IPropertyTraceRepository propertyTraceRepository)
        {
            _propertyTraceRepository = propertyTraceRepository;
        }

        [HttpGet]
        public PropertyTrace GetPropertyTrace(int id)
        {
            return _propertyTraceRepository.GetPropertyTrace(id);
        }

        [HttpGet]
        [Route("GetPropertyTraces")]
        public List<PropertyTrace> GetPropertyTraces()
        {
            return _propertyTraceRepository.GetPropertyTraces();
        }

        [HttpPost]
        [Route("AddPropertyTrace")]
        public PropertyTrace AddPropertyTrace(PropertyTrace propertyTrace)
        {
            return _propertyTraceRepository.AddPropertyTrace(propertyTrace);
        }

        [HttpPut]
        [Route("EditPropertyTrace")]
        public PropertyTrace EditPropertyTrace(PropertyTrace propertyTrace)
        {
            return _propertyTraceRepository.EditPropertyTrace(propertyTrace);
        }

        [HttpDelete]
        [Route("DeletePropertyTrace/{id}")]
        public void DeletePropertyTrace(int id)
        {
            _propertyTraceRepository.DeletePropertyTrace(id);
        }
    }
}

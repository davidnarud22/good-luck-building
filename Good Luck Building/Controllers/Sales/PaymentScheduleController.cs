﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.PaymentSchedules;
using Sales.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Sales
{
    [Route("PaymentSchedule")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class PaymentScheduleController : ControllerBase
    {
        private IPaymentScheduleRepository _paymentScheduleRepository;
        public PaymentScheduleController(IPaymentScheduleRepository paymentScheduleRepository)
        {
            _paymentScheduleRepository = paymentScheduleRepository;
        }

        [HttpGet]
        public PaymentSchedule GetPaymentSchedule(int id)
        {
            return _paymentScheduleRepository.GetPaymentSchedule(id);
        }

        [HttpGet]
        [Route("GetPaymentSchedules")]
        public List<PaymentSchedule> GetPaymentSchedules()
        {
            return _paymentScheduleRepository.GetPaymentSchedules();
        }

        [HttpPost]
        [Route("AddPaymentSchedule")]
        public PaymentSchedule AddPaymentSchedule(PaymentSchedule paymentSchedule)
        {
            return _paymentScheduleRepository.AddPaymentSchedule(paymentSchedule);
        }

        [HttpPut]
        [Route("EditPaymentSchedule")]
        public PaymentSchedule EditPaymentSchedule(PaymentSchedule paymentSchedule)
        {
            return _paymentScheduleRepository.EditPaymentSchedule(paymentSchedule);
        }

        [HttpDelete]
        [Route("DeletePaymentSchedule/{id}")]
        public void DeletePaymentSchedule(int id)
        {
            _paymentScheduleRepository.DeletePaymentSchedule(id);
        }
    }
}

﻿using Applications.Interfaces.Authentication;
using Authentication.Models;
using Authentication.Models.Models;
using Microsoft.AspNetCore.Mvc;

namespace Good_Luck_Building.Controllers.Authentication
{
    [Route("Authentication")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private IUserApplication _userApplication;
        private ILogin _authentication;

        public AuthenticationController(IUserApplication userApplication, ILogin authentication)
        {
            _userApplication = userApplication;
            _authentication = authentication;
        }

        [HttpPost]
        [Route("Login")]
        public TokenAndRole Login(LoginUser User)
        {
            return _authentication.Authentication(User);
        }
    }
}

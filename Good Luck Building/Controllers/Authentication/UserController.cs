﻿using Applications.Interfaces.Authentication;
using Authentication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Authentication;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Authentication
{
    [Route("User")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class UserController : ControllerBase
    {
        private IUserRepository _UserRepository;
        private IUserApplication _userApplication;

        public UserController(IUserRepository UserRepository, IUserApplication userApplication)
        {
            _UserRepository = UserRepository;
            _userApplication = userApplication;
        }

        [HttpGet]
        public User GetUser(int id)
        {
            return _UserRepository.GetUser(id);
        }

        [HttpGet]
        [Route("GetUsers")]
        public List<User> GetUsers()
        {
            return _UserRepository.GetUsers();
        }

        [HttpPost]
        [Route("AddUser")]
        public User AddUser(User User)
        {
            return _UserRepository.AddUser(User);
        }

        [HttpPost]
        [Route("GetUserForEmail/{email}")]
        public User GetUserForEmail(string email)
        {
            return _userApplication.getUserForEmail(email);
        }

        [HttpPut]
        [Route("EditUser")]
        public User EditUser(User User)
        {
            return _UserRepository.EditUser(User);
        }

        [HttpDelete]
        [Route("DeleteUser/{id}")]
        public void DeleteUser(int id)
        {
            _UserRepository.DeleteUser(id);
        }
    }
}

﻿using Authentication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.Authentication;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.Authentication
{
    [Route("UserType")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class UserTypeController : ControllerBase
    {
        private IUserTypeRepository _UserTypeRepository;

        public UserTypeController(IUserTypeRepository UserTypeRepository)
        {
            _UserTypeRepository = UserTypeRepository;
        }

        [HttpGet]
        public UserType GetUserType(int id)
        {
            return _UserTypeRepository.GetUserType(id);
        }

        [HttpGet]
        [Route("GetUsersType")]
        public List<UserType> GetUsersType()
        {
            return _UserTypeRepository.GetUsersType();
        }

        [HttpPost]
        [Route("AddUserType")]
        public UserType AddUserType(UserType UserType)
        {
            return _UserTypeRepository.AddUserType(UserType);
        }

        [HttpPut]
        [Route("EditUserType")]
        public UserType EditUserType(UserType UserType)
        {
            return _UserTypeRepository.EditUserType(UserType);
        }

        [HttpDelete]
        [Route("DeleteUserType/{id}")]
        public void DeleteUserType(int id)
        {
            _UserTypeRepository.DeleteUserType(id);
        }
    }
}

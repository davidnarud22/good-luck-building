﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.SalesRoom;
using SalesRoom.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.SalesRoom
{
    [Route("Quote")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class QuoteController : ControllerBase
    {
        private IQuoteRepository _quoteRepository;
        public QuoteController(IQuoteRepository quoteRepository)
        {
            _quoteRepository = quoteRepository;
        }

        [HttpGet]
        public Quote GetQuote(int id)
        {
            return _quoteRepository.GetQuote(id);
        }

        [HttpGet]
        [Route("GetQuotes")]
        public List<Quote> GetQuotes()
        {
            return _quoteRepository.GetQuotes();
        }

        [HttpPost]
        [Route("AddQuote")]
        public Quote AddQuote(Quote quote)
        {
            return _quoteRepository.AddQuote(quote);
        }

        [HttpPut]
        [Route("EditQuote")]
        public Quote EditQuote(Quote quote)
        {
            return _quoteRepository.EditQuote(quote);
        }

        [HttpDelete]
        [Route("DeleteQuote/{id}")]
        public void DeleteQuote(int id)
        {
            _quoteRepository.DeleteQuote(id);
        }
    }
}

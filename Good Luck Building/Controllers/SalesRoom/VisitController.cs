﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.SalesRoom;
using SalesRoom.Models;
using System.Collections.Generic;

namespace Good_Luck_Building.Controllers.SalesRoom
{
    [Route("Visit")]
    [ApiController]
    [Authorize(Roles = "Administrator")]

    public class VisitController : ControllerBase
    {
        private IVisitRepository _visitRepository;
        public VisitController(IVisitRepository visitRepository)
        {
            _visitRepository = visitRepository;
        }

        [HttpGet]
        public Visit GetVisit(int id)
        {
            return _visitRepository.GetVisit(id);
        }

        [HttpGet]
        [Route("GetVisits")]
        public List<Visit> GetVisits()
        {
            return _visitRepository.GetVisits();
        }

        [HttpPost]
        [Route("AddVisit")]
        public Visit AddVisit(Visit visit)
        {
            return _visitRepository.AddVisit(visit);
        }

        [HttpPut]
        [Route("EditVisit")]
        public Visit EditVisit(Visit visit)
        {
            return _visitRepository.EditVisit(visit);
        }

        [HttpDelete]
        [Route("DeleteVisit/{id}")]
        public void DeleteVisit(int id)
        {
            _visitRepository.DeleteVisit(id);
        }
    }
}

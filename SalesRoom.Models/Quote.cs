﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SalesRoom.Models
{
    public class Quote
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? IdProperty { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public int IdTypeOfPayment { get; set; }

        public int? NumbersOfQuotas { get; set; }

        public int? ValueOfQuotas { get; set; }

        [Required]
        public int Tax { get; set; }

        public int? IdDownPayment { get; set; }

        public int? IdOwner { get; set; }
    }
}

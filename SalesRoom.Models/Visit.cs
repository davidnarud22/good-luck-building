﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SalesRoom.Models
{
    public class Visit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public DateTime DateVisit { get; set; }

        [Required]
        public int IdProperty { get; set; }

        [Required]
        public string NameOfVisitor { get; set; } 
    }
}

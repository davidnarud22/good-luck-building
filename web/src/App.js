import React from 'react';
import { Routes, Route } from 'react-router'

import Login from './components/Screens/Authentication/Login'
import City from './components/Screens/Shared/City';
import Country from './components/Screens/Shared/Country';
import Department from './components/Screens/Shared/Department';
import Macroproject from './components/Screens/Shared/Macroproject';
import Owner from './components/Screens/Shared/Owner';
import Project from './components/Screens/Shared/Project';
import StateOfProperty from './components/Screens/Shared/StateOfProperty';
import TypeOfIdentification from './components/Screens/Shared/TypeOfIdentification';
import TypeOfPayment from './components/Screens/Shared/TypeOfPayment';
import TypeOfProperty from './components/Screens/Shared/TypeOfProperty';
import Property from './components/Screens/Shared/Property';
import PropertyImage from './components/Screens/Shared/PropertyImage';
import ListOfPropertys from './components/Screens/Shared/ListOfPropertys';
import Visit from './components/Screens/SalesRoom/Visit';
import Quote from './components/Screens/SalesRoom/Quote';
import PropertyTrace from './components/Screens/Sales/PropertyTrace';
import PaymentSchedule from './components/Screens/Sales/PaymentSchedule';
import User from './components/Screens/Authentication/User';
import UserType from './components/Screens/Authentication/UserType';

function App() {
  return (
    <>
      <div>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/city" element={<City />} />
          <Route path="/country" element={<Country />} />
          <Route path="/typeOfPayment" element={<TypeOfPayment />} />
          <Route path="/typeOfProperty" element={<TypeOfProperty />} />
          <Route path="/owner" element={<Owner />} />
          <Route path="/typeOfIdentification" element={<TypeOfIdentification />} />
          <Route path="/department" element={<Department />} />
          <Route path="/macroproject" element={<Macroproject />} />
          <Route path="/project" element={<Project />} />
          <Route path="/stateOfProperty" element={<StateOfProperty />} />
          <Route path="/property" element={<Property />} />
          <Route path="/propertyImage" element={<PropertyImage />} />
          <Route path="/visit" element={<Visit />} />
          <Route path="/quote" element={<Quote />} />
          <Route path="/listOfPropertys" element={<ListOfPropertys />} />
          <Route path="/propertyTrace" element={<PropertyTrace />} />
          <Route path="/paymentSchedule" element={<PaymentSchedule />} />
          <Route path="/user" element={<User />} />
          <Route path="/userType" element={<UserType />} />
        </Routes>
      </div>
    </>
  );
}

export default App;

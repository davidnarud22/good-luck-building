import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const ReactLoader = () => {
    return (
        <Loader
            type="Bars"
            color='rgba(245, 158, 11)'
            height={100}
            width={100}
        />
    );
}
export default ReactLoader;
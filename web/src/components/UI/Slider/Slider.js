import React, { useState, useEffect } from 'react'
import './Slider.css'
import BtnSlider from './BtnSlider'
import PropertyImages from './dataSlider'
import { getPropertyImagesForProperty } from '../../../services/Shared/PropertyImageService';


const Slider = ({ idProperty }) => {

    const [PropertyImages, savePropertyImages] = useState([]);

    const GetPropertyImagesForProperty = () => {
        getPropertyImagesForProperty(idProperty)
            .then((response) => {
                console.log(response.data)
                savePropertyImages(response.data.sort((a, b) => (a.order < b.order) ? -1 : 1));
            })
            .catch((err) => {
                alert(err);
            });
    }

    const [slideIndex, setSlideIndex] = useState(1)

    const nextSlide = () => {
        if (slideIndex !== PropertyImages.length) {
            setSlideIndex(slideIndex + 1)
        }
        else if (slideIndex === PropertyImages.length) {
            setSlideIndex(1)
        }
    }

    const prevSlide = () => {
        if (slideIndex !== 1) {
            setSlideIndex(slideIndex - 1)
        }
        else if (slideIndex === 1) {
            setSlideIndex(PropertyImages.length)
        }
    }

    const moveDot = index => {
        setSlideIndex(index)
    }

    useEffect(() => {
        GetPropertyImagesForProperty();
    }, [])

    return (
        <div className="container-slider  rounded-lg shadow-lg">
            {PropertyImages.map((obj, index) => {
                return (
                    <div
                        key={index}
                        className={slideIndex === index + 1 ? "slide active-anim" : "slide"}
                    >
                        <img
                            src={obj.file}
                        />
                    </div>
                )
            })}
            <BtnSlider moveSlide={nextSlide} direction={"next"} />
            <BtnSlider moveSlide={prevSlide} direction={"prev"} />
            <div className="container-dots" style={{ marginBottom: 50 }}>
                {PropertyImages.map((item, index) => (
                    <div
                        onClick={() => moveDot(index + 1)}
                        className={slideIndex === index + 1 ? "dot active" : "dot"}
                    ></div>
                ))}
            </div>
        </div>
    )
}
export default Slider;


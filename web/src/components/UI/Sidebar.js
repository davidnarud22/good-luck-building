import React from 'react';
import { NavLink } from 'react-router-dom';

const Sidebar = () => {

    const removeTokenAndRole = () => {
        localStorage.removeItem("token");
        localStorage.removeItem("role");
    }
    return (
        <div className="md:w-2/5 xl:w-1/5 bg-gray-800">
            <div className="p-6">
                <p className="uppercase text-yellow-500 text-2xl tracking-wide text-center font-bold">Good Luck Building</p>
                <p className="mt-3 text-yellow-300">Administra tus procesos con las siguientes opciones:</p>
                <nav className="mt-5">
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/country">Paises</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/city">Ciudades</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/department">Departamentos</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/typeOfPayment">Medios de pago</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/typeOfProperty">Tipos propiedades</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/stateOfProperty">Estados de propiedad</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/typeOfIdentification">Tipos de identificacion</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/owner">Propietarios</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/macroproject">Macroproyectos</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/project">Proyectos</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/property">Propiedades</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/propertyImage">Imagenes de Propiedades</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/visit">Visitas</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/quote">Cotizaciones</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/listOfPropertys">Listado de propiedades</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/paymentSchedule">Acuerdos de pagos</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/propertyTrace">Facturas</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/usertype">Tipos de usuario</NavLink>
                    <NavLink className="p-1 text-gray-400 block hover:bg-yellow-500 hover:text-gray-900" activeclassname="active" to="/user">Usuarios</NavLink>
                </nav>
            </div>
            <div className="p-6 mt-20">
                <nav className="mt-11" onClick={() => { removeTokenAndRole() }}>
                    <NavLink className="p-1 text-gray-400 block hover:bg-red-500 hover:text-gray-900" activeclassname="active" to="/">Cerrar sesion</NavLink>
                </nav>
            </div>
        </div>
    );
}

export default Sidebar;
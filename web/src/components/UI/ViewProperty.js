import React from 'react';
import Slider from '../UI//Slider/Slider'

const ViewProperty = ({ property }) => {
    console.log(property)

    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
    })

    return (
        <>
            <div className="mb-6 shadow-md bg-white">
                <Slider
                    idProperty={property.id}
                />
                <div className="relative -mt-9 " style={{ paddingLeft: '15%', paddingRight: '15%' }}>
                    <div className="bg-white p-6 rounded-lg shadow-lg ">
                        <div className="flex items-baseline">
                            <span className="bg-teal-300 text-teal-900 text-xs mr-1 inline-block rounded-full  uppercase font-semibold tracking-wide">
                                {property.typeProperty}
                            </span>
                            <div className=" text-gray-600 uppercase text-xs font-semibold tracking-wider">
                                {property.numberOfFloors > 1 ? property.numberOfFloors + " Pisos" : property.numberOfFloors + " Piso"}  &bull; {property.numberOfRooms > 1 ? property.numberOfRooms + " Habitaciones " : property.numberOfRooms + " Habitacion"} &bull; {property.numberOfBathrooms > 1 ? property.numberOfBathrooms + " Baños" : property.numberOfBathrooms + " Baño"}
                            </div>
                        </div>

                        <h4 className="mt-1 text-xl font-semibold  leading-tight truncate">{property.name + " - " + property.area + "m²"} </h4>

                        <div className="mt-1">
                            {formatterPeso.format(property.price)}
                            <span className="text-gray-600 text-sm"></span>
                        </div>
                        <div className="mt-1 uppercase text-xs font-semibold tracking-wider">
                            {property.stateProperty}
                        </div>
                        {property.idProject > 0 ? <div className="mt-1 uppercase text-xs font-semibold tracking-wider">
                            Proyecto : {property.nameProject}
                        </div> : null}
                        <div className="mt-1 text-gray-600 uppercase text-xs font-semibold tracking-wider">
                            {property.country}  &bull; {property.city} &bull; {property.department}
                        </div>
                        <div className="mt-1">
                            <span className="text-teal-600 text-md font-semibold">{property.address}</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ViewProperty;
import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getPropertyTraces, addPropertyTrace, editPropertyTrace, deletePropertyTrace } from '../../../services/Sales/PropertyTraceService';
import { getPropertys } from '../../../services/Shared/PropertyService';
import { getTypeOfPayments } from '../../../services/Shared/TypeOfPaymentService';
import { getPaymentSchedules } from '../../../services/Sales/PaymentScheduleService';
import { getQuotes } from '../../../services/SalesRoom/QuoteService';
import { getOwners } from '../../../services/Shared/OwnerService';
import { useNavigate } from 'react-router-dom'
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar';
import ReactLoader from '../../UI/ReactLoader';

const PropertyTrace = () => {
    const navigate = useNavigate();
    const [propertys, savePropertys] = useState([]);
    const [typeOfPayments, saveTypeOfPayments] = useState([]);
    const [paymentsSchedules, savePaymentsSchedules] = useState([]);
    const [quotes, saveQuotes] = useState([]);
    const [PropertyTraces, savePropertyTraces] = useState([]);
    const [owners, saveOwners] = useState([]);
    const [namePropertyTraces, saveNamePropertyTraces] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idPropertyTrace, saveIdPropertyTrace] = useState('');
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            name: '',
            value: 0,
            tax: 0,
            downPayment: 0,
            idProperty: '',
            idTypeOfPayment: '',
            idNewOwner: '',
            idPaymentSchedule: '',
            idQuote: ''
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El Nombre de la Factura deben tener al menos 5 caracteres')
                .required('El Nombre de la Factura es obligatorio'),
            value: Yup.string()
                .required('El Nombre de la Factura es obligatorio'),
            tax: Yup.string()
                .required('El Nombre de la Factura es obligatorio'),
            idProperty: Yup.string()
                .required('La propiedad es obligatorio'),
            idTypeOfPayment: Yup.string()
                .required('El tipo de pago es obligatorio'),
            idNewOwner: Yup.string()
                .required('El propietario es obligatorio'),
            idPaymentSchedule: Yup.string()
                .required('El acuerdo de pago es obligatorio'),
        }),
        onSubmit: PropertyTrace => {
            try {
                saveshowLoader(true);
                if (idPropertyTrace > 0) {
                    editPropertyTrace(PropertyTrace, idPropertyTrace)
                        .then(() => {
                            GetPropertyTraces();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        })
                    saveIdPropertyTrace('')
                }
                else {
                    addPropertyTrace(PropertyTrace)
                        .then(() => {
                            GetPropertyTraces();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        });

                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetPropertyTraces = () => {
        saveshowLoader(true);
        getPropertyTraces()
            .then((response) => {
                console.log(response.data);
                savePropertyTraces(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });

    }

    const renderObject = (PropertyTraces, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickPropertyTrace(PropertyTraces)}>
                {PropertyTraces.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
        formik.initialValues.value = ''
        formik.initialValues.tax = ''
        formik.initialValues.downPayment = ''
        formik.initialValues.idProperty = ''
        formik.initialValues.idTypeOfPayment = ''
        formik.initialValues.idNewOwner = ''
        formik.initialValues.idPaymentSchedule = ''
        formik.initialValues.idQuote = ''
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idPropertyTrace > 0) {
            saveIdPropertyTrace('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickPropertyTrace = (PropertyTrace) => {
        openModal();
        saveIdPropertyTrace(PropertyTrace.id)
        formik.initialValues.name = PropertyTrace.name
        formik.initialValues.value = PropertyTrace.value
        formik.initialValues.tax = PropertyTrace.tax
        formik.initialValues.downPayment = PropertyTrace.downPayment
        formik.initialValues.idProperty = PropertyTrace.idProperty
        formik.initialValues.idTypeOfPayment = PropertyTrace.idTypeOfPayment
        formik.initialValues.idNewOwner = PropertyTrace.idNewOwner
        formik.initialValues.idPaymentSchedule = PropertyTrace.idPaymentSchedule
        formik.initialValues.idQuote = PropertyTrace.idQuote
    }
    const DeletePropertyTrace = () => {
        saveshowLoader(true);
        deletePropertyTrace(idPropertyTrace)
            .then(() => {
                GetPropertyTraces();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });

    }

    const fillPriceProperty = (e) => {
        if (!!e.target.value) {
            propertys.forEach(element => {
                if (element.id == e.target.value) {
                    formik.setFieldValue(
                        'value',
                        element.price
                    )
                }
            });
        }
    }

    const fillSelects = () => {
        saveshowLoader(true);
        getPropertys()
            .then((response) => {
                savePropertys(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
        getTypeOfPayments()
            .then((response) => {
                saveTypeOfPayments(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
        getPaymentSchedules()
            .then((response) => {
                savePaymentsSchedules(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
        getPaymentSchedules()
            .then((response) => {
                savePaymentsSchedules(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
        getQuotes()
            .then((response) => {
                saveQuotes(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
        getOwners()
            .then((response) => {
                saveOwners(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetPropertyTraces();
        fillSelects();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Factura"
                                        appElement={document.getElementById('root')}
                                    >
                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre Factura</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre Factura"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idProperty">Propiedad</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idProperty"
                                                    name="idProperty"
                                                    value={formik.values.idProperty}
                                                    onChange={(e) => { formik.handleChange(e); fillPriceProperty(e) }}>
                                                    <option value="">-- Seleccione --</option>
                                                    {propertys.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idProperty && formik.errors.idProperty ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idProperty}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="value">Precio propiedad</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="value"
                                                    type="number"
                                                    placeholder="Precio propiedad"
                                                    value={formik.values.value}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.value && formik.errors.value ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.value}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="tax">Impuesto</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="tax"
                                                    type="number"
                                                    placeholder="Precio Impuesto"
                                                    value={formik.values.tax}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.tax && formik.errors.tax ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.tax}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="downPayment">Cuota inicial</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="downPayment"
                                                    type="number"
                                                    placeholder="Cuota inicial"
                                                    value={formik.values.downPayment}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idTypeOfPayment">Tipo de pago</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idTypeOfPayment"
                                                    name="idTypeOfPayment"
                                                    value={formik.values.idTypeOfPayment}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {typeOfPayments.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idTypeOfPayment && formik.errors.idTypeOfPayment ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idTypeOfPayment}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idNewOwner">Nuevo propietario</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idNewOwner"
                                                    name="idNewOwner"
                                                    value={formik.values.idNewOwner}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {owners.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.fullName}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idNewOwner && formik.errors.idNewOwner ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idNewOwner}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idPaymentSchedule">Acuerdo de pago</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idPaymentSchedule"
                                                    name="idPaymentSchedule"
                                                    value={formik.values.idPaymentSchedule}
                                                    onChange={formik.handleChange}>
                                                    <option value=''>-- Seleccione --</option>
                                                    {paymentsSchedules.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {"Numero de cuotas: " + item.numbersOfQuotas}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idPaymentSchedule && formik.errors.idPaymentSchedule ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idPaymentSchedule}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idQuote">Cotizacion</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idQuote"
                                                    name="idQuote"
                                                    value={formik.values.idQuote}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {quotes.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {"Cotizacion Num #" + item.id}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idPropertyTrace > 0 ? "Editar Factura" : "Agregar Factura"}
                                            />
                                            {idPropertyTrace > 0 ? <input
                                                type="button"
                                                onClick={() => DeletePropertyTrace()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar Factura"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Facturas</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={PropertyTraces}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Facturaes!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Factura"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default PropertyTrace;
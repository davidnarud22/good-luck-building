import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getPaymentSchedules, addPaymentSchedule, editPaymentSchedule, deletePaymentSchedule } from '../../../services/Sales/PaymentScheduleService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const PaymentSchedule = () => {
    const navigate = useNavigate();
    const [PaymentSchedules, savePaymentSchedules] = useState([]);
    const [namePaymentSchedules, saveNamePaymentSchedules] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idPaymentSchedule, saveIdPaymentSchedule] = useState('');
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            numbersOfQuotas: '',
            valueOfQuotas: 0,
        },
        validationSchema: Yup.object({
            numbersOfQuotas: Yup.string()
                .required('El Nombre de la Acuerdo de pago es obligatorio'),
            valueOfQuotas: Yup.string()
                .required('El Nombre de la Acuerdo de pago es obligatorio'),
        }),
        onSubmit: PaymentSchedule => {
            try {
                saveshowLoader(true);
                if (idPaymentSchedule > 0) {
                    editPaymentSchedule(PaymentSchedule, idPaymentSchedule)
                        .then(() => {
                            GetPaymentSchedules();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                    saveIdPaymentSchedule('')
                }
                else {
                    addPaymentSchedule(PaymentSchedule)
                        .then(() => {
                            GetPaymentSchedules();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });


    const GetPaymentSchedules = () => {
        saveshowLoader(true);
        getPaymentSchedules()
            .then((response) => {
                savePaymentSchedules(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (PaymentSchedules, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickPaymentSchedule(PaymentSchedules)}>
                {"# Cuotas: " + PaymentSchedules.numbersOfQuotas}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.valueOfQuotas = ''
        formik.initialValues.numbersOfQuotas = ''
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idPaymentSchedule > 0) {
            saveIdPaymentSchedule('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickPaymentSchedule = (PaymentSchedule) => {
        openModal();
        saveIdPaymentSchedule(PaymentSchedule.id)
        formik.initialValues.numbersOfQuotas = PaymentSchedule.numbersOfQuotas
        formik.initialValues.valueOfQuotas = PaymentSchedule.valueOfQuotas;
    }
    const DeletePaymentSchedule = () => {
        saveshowLoader(true);
        deletePaymentSchedule(idPaymentSchedule)
            .then(() => {
                GetPaymentSchedules();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetPaymentSchedules();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Acuerdo de pago"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="numbersOfQuotas">Numero de cuotas</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="numbersOfQuotas"
                                                    type="number"
                                                    placeholder="Numero de cuotas"
                                                    value={formik.values.numbersOfQuotas}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.numbersOfQuotas && formik.errors.numbersOfQuotas ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.numbersOfQuotas}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="valueOfQuotas">Valor de cuota</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="valueOfQuotas"
                                                    type="number"
                                                    placeholder="Valor de cuota"
                                                    value={formik.values.valueOfQuotas}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.valueOfQuotas && formik.errors.valueOfQuotas ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.valueOfQuotas}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idPaymentSchedule > 0 ? "Editar Acuerdo de pago" : "Agregar Acuerdo de pago"}
                                            />
                                            {idPaymentSchedule > 0 ? <input
                                                type="button"
                                                onClick={() => DeletePaymentSchedule()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar Acuerdo de pago"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Acuerdos de Pagos</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={PaymentSchedules}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Acuerdo de pagos!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Acuerdo de pago"
                                        onClick={() => openModal()}
                                    />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default PaymentSchedule;
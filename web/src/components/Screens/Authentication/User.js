import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getUsers, addUser, editUser, deleteUser } from '../../../services/Authentication/UserService';
import { getUsersType } from '../../../services/Authentication/UsertypeService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar';
import { useNavigate } from 'react-router-dom';
import ReactLoader from '../../UI/ReactLoader';

const User = () => {
    const navigate = useNavigate();
    const [Users, saveUsers] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idUser, saveIdUser] = useState('');
    const [userTypes, saveUserTypes] = useState([]);
    const [deleteUsers, setDeleteUsers] = useState(false);
    const [editUsers, setEditUsers] = useState(false);
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            idUserType: ''
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El nombre del usuario deben tener al menos 5 caracteres')
                .required('El nombre del usuario es obligatorio'),
            email: Yup.string()
                .min(8, 'El correo debe tener al menos 8 caracteres')
                .required('El correo es obligatorio'),
            password: Yup.string()
                .min(8, 'La contraseña debe tener al menos 5 caracteres')
                .required('La contraseña es obligatoria'),
            idUserType: Yup.string()
                .required('El tipo de usuario es obligatorio'),
        }),
        onSubmit: User => {
            try {
                saveshowLoader(true);
                if (idUser > 0) {
                    if (idUser == 1 || idUser == 2) {
                        setEditUsers(true);
                    }
                    else {
                        console.log(User)
                        editUser(User, idUser)
                            .then(() => {
                                GetUsers();
                                formik.resetForm();
                                closeModal();
                                saveshowLoader(false);
                            }).catch((err) => {
                                navigate('/')
                            });
                        saveIdUser('')
                    }
                }
                else {
                    console.log(User)
                    addUser(User)
                        .then(() => {
                            GetUsers();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetUsers = () => {
        saveshowLoader(true);
        getUsers()
            .then((response) => {
                saveUsers(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (Users, idx) => {
        console.log(Users)
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickUser(Users)}>
                {"Tipo Usuario : " + Users.idUserType + " - " + Users.name}
            </li>
        );
    }

    const openModal = () => {
        setDeleteUsers(false);
        setEditUsers(false);
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = '';
        formik.initialValues.email = '';
        formik.initialValues.password = '';
        formik.initialValues.idUserType = '';
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idUser > 0) {
            saveIdUser('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickUser = (User) => {
        openModal();
        saveIdUser(User.idUser)
        formik.initialValues.name = User.name;
        formik.initialValues.email = User.email;
        formik.initialValues.password = User.password;
        formik.initialValues.idUserType = User.idUserType;
    }
    const DeleteUser = () => {
        if (idUser == 1 || idUser == 2) {
            setDeleteUsers(true);
        }
        else {
            saveshowLoader(true);
            deleteUser(idUser)
                .then(() => {
                    GetUsers();
                    formik.resetForm();
                    closeModal();
                    saveshowLoader(false);
                }).catch((err) => {
                    navigate('/')
                });
        }
    }

    const GetUsersType = () => {
        saveshowLoader(true);
        getUsersType()
            .then((response) => {
                saveUserTypes(response.data)
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetUsers();
        GetUsersType();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar User"
                                        appElement={document.getElementById('root')}
                                    >
                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre usuario</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Numbre usuario"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">Email usuario</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="email"
                                                    type="text"
                                                    placeholder="Email usuario"
                                                    value={formik.values.email}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.email && formik.errors.email ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.email}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">Contraseña</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="password"
                                                    type="text"
                                                    placeholder="Contraseña"
                                                    value={formik.values.password}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.password && formik.errors.password ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.password}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idUserType">Tipo de usuario</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idUserType"
                                                    name="idUserType"
                                                    value={formik.values.idUserType}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {userTypes.map(item => (
                                                        <option
                                                            key={item.idUserType}
                                                            value={item.idUserType}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idUserType && formik.errors.idUserType ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idUserType}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idUser > 0 ? "Editar Usuario" : "Agregar Usuario"}
                                            />
                                            {editUsers ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-2 mt-5" role="alert">
                                                    <p>El usuario Administrador y User no se pueden editar</p>
                                                </div>
                                            ) : null
                                            }
                                            {idUser > 0 ? <>
                                                <input
                                                    type="button"
                                                    onClick={() => DeleteUser()}
                                                    className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                    value="Eliminar Usuario"
                                                />
                                                {deleteUsers ? (
                                                    <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5 mt-5" role="alert">
                                                        <p>El usuario Administrador y User no se pueden elminar</p>
                                                    </div>
                                                ) : null
                                                }
                                            </>
                                                : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Users</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={Users}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Users!</div>}
                                            sortBy={["name", { key: "idUser", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar User"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default User;
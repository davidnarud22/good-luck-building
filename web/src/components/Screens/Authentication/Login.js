import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Authentication } from '../../../services/Authentication/LoginService';
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const Login = () => {
    const navigate = useNavigate();
    const [showLoader, saveshowLoader] = useState(false);
    const [unauthorized, savesUnauthorized] = useState(false);

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .required('El email es obligatorio'),
            password: Yup.string()
                .required('La contraseña es requerida'),
        }),
        onSubmit: user => {
            try {
                saveshowLoader(true);
                Authentication(user)
                    .then((response) => {
                        console.log(response.data)
                        if (response.data.role == 'Administrator') {
                            localStorage.token = response.data.token;
                            localStorage.role = response.data.role;
                            navigate('/city')
                        }
                        else if (response.data.role == 'User') {
                            localStorage.token = response.data.token;
                            localStorage.role = response.data.role;
                            navigate('/listOfPropertys')
                        }
                        else {
                            savesUnauthorized(true);
                            saveshowLoader(false);
                        }
                    }).catch(() => {
                        saveshowLoader(false);
                        savesUnauthorized(true);
                    })

            } catch (error) {
                console.log(error)
            }
        }
    });

    useEffect(() => {
        localStorage.removeItem("token");
        localStorage.removeItem("role");
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div className="flex justify-center" style={{ marginTop: '10%' }}>
                    <div>
                        <form
                            style={{ width: 400 }}
                            onSubmit={formik.handleSubmit}>
                            <p className="uppercase text-black text-2xl tracking-wide text-center font-bold mb-11">Good Luck Building</p>
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-3" htmlFor="email">Email</label>
                                <input
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="email"
                                    type="text"
                                    placeholder="email"
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur} />
                            </div>
                            {formik.touched.email && formik.errors.email ? (
                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                    <p>{formik.errors.email}</p>
                                </div>
                            ) : null
                            }
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-3" htmlFor="password">Contraseña</label>
                                <input
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="password"
                                    type="password"
                                    placeholder="Contraseña"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur} />
                            </div>
                            {formik.touched.password && formik.errors.password ? (
                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                    <p>{formik.errors.password}</p>
                                </div>
                            ) : null
                            }
                            <div className="flex justify-center mt-10">
                                <input
                                    type='submit'
                                    className="bg-yellow-500 hover:bg-yellow-600 inline-block mb-5 p-2 text-white uppercase font-bold"
                                    value="Ingresar"
                                />
                            </div>
                            {unauthorized ? (
                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                    <p>El usuario no fue encontrado o no esta autorizado</p>
                                </div>
                            ) : null
                            }
                        </form>
                    </div>
                </div>
            }
        </>
    );
}

export default Login;
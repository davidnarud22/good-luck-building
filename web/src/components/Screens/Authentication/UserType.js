import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getUsersType, addUserType, editUserType, deleteUserType } from '../../../services/Authentication/UsertypeService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const UserType = () => {
    const navigate = useNavigate();
    const [UsersType, saveUsersType] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [deleteUserType, setDeleteUserType] = useState(false);
    const [editUserType, setEditUserType] = useState(false);
    const [showLoader, saveshowLoader] = useState(false);
    const [idUserType, saveIdUserType] = useState('');

    const formik = useFormik({
        initialValues: {
            name: '',
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El tipo de usuario deben tener al menos 5 caracteres')
                .required('El Nombre de la pais es obligatorio'),
        }),
        onSubmit: UserType => {
            try {
                if (idUserType > 0) {
                    if (idUserType == 1 || idUserType == 2) {
                        setEditUserType(true);
                    }
                    else {
                        saveshowLoader(true);
                        editUserType(UserType, idUserType)
                            .then(() => {
                                GetUsersType();
                                formik.resetForm();
                                closeModal();
                                saveshowLoader(false);
                            }).catch((err) => {
                                navigate('/')
                            });
                        saveIdUserType('')
                    }
                }
                else {
                    addUserType(UserType)
                        .then(() => {
                            GetUsersType();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetUsersType = () => {
        saveshowLoader(true);
        getUsersType()
            .then((response) => {
                saveshowLoader(false);
                saveUsersType(response.data);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (UsersType, idx) => {
        console.log(UsersType)
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickUserType(UsersType)}>
                {"# " + UsersType.idUserType + " - " + UsersType.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = '';
        setDeleteUserType(false)
        setEditUserType(false)
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idUserType > 0) {
            saveIdUserType('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickUserType = (UserType) => {
        openModal();
        saveIdUserType(UserType.idUserType)
        formik.initialValues.name = UserType.name;
    }
    const DeleteUserType = () => {
        if (idUserType == 1 || idUserType == 2) {
            setDeleteUserType(true);
        }
        else {
            saveshowLoader(true);
            deleteUserType(idUserType)
                .then(() => {
                    GetUsersType();
                    formik.resetForm();
                    closeModal();
                    saveshowLoader(false);
                }).catch((err) => {
                    navigate('/')
                });
        }

    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetUsersType();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar UserTypea"
                                        appElement={document.getElementById('root')}
                                    >
                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Tipo de usuario</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Tipo de usuario"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5 mt-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idUserType > 0 ? "Editar Tipo de usuario" : "Agregar Tipo de usuario"}
                                            />
                                            {editUserType ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-2 mt-5" role="alert">
                                                    <p>El tipo de usuario Administrador y User no se pueden editar</p>
                                                </div>
                                            ) : null
                                            }
                                            {idUserType > 0 ? <>
                                                <input
                                                    type="button"
                                                    onClick={() => DeleteUserType()}
                                                    className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                    value="Eliminar Tipo de usuario"
                                                />
                                                {deleteUserType ? (
                                                    <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5 mt-5" role="alert">
                                                        <p>El tipo de usuario Administrador y User no se pueden elminar</p>
                                                    </div>
                                                ) : null
                                                }
                                            </>
                                                : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Tipos de usuarios</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={UsersType}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Tipos de usuarios!</div>}
                                            sortBy={["name", { key: "IdUserType", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar tipo de usuario"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default UserType;
import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getOwners, addOwner, editOwner, deleteOwner } from '../../../services/Shared/OwnerService';
import { getTypeOfIdentifications } from '../../../services/Shared/TypeOfIdentificationService';
import FlatList from 'flatlist-react';
import moment from 'moment';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom';
import ReactLoader from '../../UI/ReactLoader';

const Owner = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const navigate = useNavigate();
    const [Owners, saveOwners] = useState([]);
    const [typeOfIdentifications, saveTypeOfIdentifications] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idOwner, saveIdOwner] = useState('');
    const [photo, savePhoto] = useState('');

    const formik = useFormik({
        initialValues: {
            fullName: '',
            firstName: '',
            secondName: '',
            firstSurName: '',
            secondSurName: '',
            address: '',
            photo: '',
            dateOfBirth: '',
            identificactionNumber: '',
            idTypeOfIdentification: '',
            email: '',
            celPhone: ''
        },
        validationSchema: Yup.object({
            firstName: Yup.string()
                .min(3, 'El Primer Nombre debe tener al menos 3 caracteres')
                .required('El Primer Nombre es obligatorio'),
            firstSurName: Yup.string()
                .min(3, 'El Primer Apellido debe tener al menos 3 caracteres')
                .required('El Primer Apellido es obligatorio'),
            identificactionNumber: Yup.string()
                .min(8, 'El Numero de identificacion debe tener al menos 8 caracteres')
                .required('El Numero de identificacion es obligatorio'),
            idTypeOfIdentification: Yup.string()
                .required('El tipo de identificacion es obligatorio'),
        }),
        onSubmit: Owner => {
            try {
                saveshowLoader(true);
                Owner.photo = photo;
                Owner.fullName = Owner.firstSurName + " " + Owner.secondSurName + " " + Owner.firstName + " " + Owner.secondName;
                if (idOwner > 0) {
                    editOwner(Owner, idOwner)
                        .then(() => {
                            GetOwners();
                            formik.resetForm();
                            openModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        });
                    saveIdOwner('')
                }
                else {
                    Owner.photo = photo;
                    addOwner(Owner)
                        .then(() => {
                            GetOwners();
                            formik.resetForm();
                            openModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });


    const GetOwners = () => {
        saveshowLoader(true);
        getOwners()
            .then((response) => {
                saveOwners(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetTypesOfIdentifications = () => {
        saveshowLoader(true);
        getTypeOfIdentifications()
            .then((response) => {
                saveTypeOfIdentifications(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (Owners, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickOwner(Owners)}>
                {Owners.firstSurName + " " + Owners.secondSurName + " " + Owners.firstName + " " + Owners.secondName}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(!modalIsOpen);
        formik.resetForm();
        if (idOwner > 0) {
            saveIdOwner(0)
        }
    }

    const clickOwner = (Owner) => {
        openModal();
        saveIdOwner(Owner.id);
        savePhoto(Owner.photo);
        const dateBirth = new Date(Owner.dateOfBirth);
        formik.initialValues.firstName = Owner.firstName;
        formik.initialValues.secondName = Owner.secondName;
        formik.initialValues.firstSurName = Owner.firstSurName;
        formik.initialValues.secondSurName = Owner.secondSurName;
        formik.initialValues.address = Owner.address;
        formik.initialValues.dateOfBirth = moment(dateBirth).format('YYYY-MM-DD');
        formik.initialValues.identificactionNumber = Owner.identificactionNumber;
        formik.initialValues.idTypeOfIdentification = Owner.idTypeOfIdentification;
        formik.initialValues.email = Owner.email;
        formik.initialValues.celPhone = Owner.celPhone;
    }
    const DeleteOwner = () => {
        saveshowLoader(true);
        deleteOwner(idOwner)
            .then(() => {
                GetOwners();
                formik.resetForm();
                openModal();
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const imageUpload = (image) => {
        saveshowLoader(true);
        const file = image.target.files[0];
        convertBase64(file).then(base64 => {
            savePhoto(base64)
            localStorage["fileBase64"] = base64;
            console.debug("file stored", base64);
            saveshowLoader(false);
        });
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            if (!!file) {
                fileReader.readAsDataURL(file)
                fileReader.onload = () => {
                    resolve(fileReader.result);
                }
                fileReader.onerror = (error) => {
                    reject(error);
                }
            }
        })
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetOwners();
        GetTypesOfIdentifications();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div className="w-full max-w-3xl">
                                    {modalIsOpen ?
                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <h1 className='text-3xl font-bold mb-4'>Agregar propietario</h1>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="firstName">Primer Nombre</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="firstName"
                                                    type="text"
                                                    placeholder="Primer Nombre"
                                                    value={formik.values.firstName}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.firstName && formik.errors.firstName ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.firstName}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="secondName">Segundo Nombre</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="secondName"
                                                    type="text"
                                                    placeholder="Segundo Nombre"
                                                    value={formik.values.secondName}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="firstSurName">Primer Apellido</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="firstSurName"
                                                    type="text"
                                                    placeholder="Primer Apellido"
                                                    value={formik.values.firstSurName}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.firstSurName && formik.errors.firstSurName ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.firstSurName}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="secondSurName">Segundo Apellido</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="secondSurName"
                                                    type="text"
                                                    placeholder="Segundo Apellido"
                                                    value={formik.values.secondSurName}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="dateOfBirth">Fecha de nacimiento</label>
                                                <input
                                                    id="dateOfBirth"
                                                    name="dateOfBirth"
                                                    type="date"
                                                    disabled={false}
                                                    value={formik.values.dateOfBirth}
                                                    onChange={formik.handleChange} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="address">Direccion</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="address"
                                                    type="text"
                                                    placeholder="Direccion"
                                                    value={formik.values.address}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idTypeOfIdentification">Tipo de identificacion</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idTypeOfIdentification"
                                                    name="idTypeOfIdentification"
                                                    value={formik.values.idTypeOfIdentification}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {typeOfIdentifications.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idTypeOfIdentification && formik.errors.idTypeOfIdentification ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idTypeOfIdentification}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="identificactionNumber">Numero de identificacion</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="identificactionNumber"
                                                    type="text"
                                                    placeholder="Numero de identificacion"
                                                    value={formik.values.identificactionNumber}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.identificactionNumber && formik.errors.identificactionNumber ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.identificactionNumber}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">Correo</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="email"
                                                    type="text"
                                                    placeholder="Correo"
                                                    value={formik.values.email}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="celPhone">Numero de celular</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="celPhone"
                                                    type="text"
                                                    placeholder="Numero de celular"
                                                    value={formik.values.celPhone}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="photo">Foto Propietario</label>
                                                <input
                                                    type="file"
                                                    id="imageFile"
                                                    name='imageFile'
                                                    onChange={(file) => imageUpload(file)} />
                                            </div>
                                            {!!photo ? <img src={photo} alt="Foto" /> : <></>}
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idOwner > 0 ? "Editar Propietario" : "Agregar Propietario"}
                                            />
                                            {idOwner > 0 ?
                                                <input
                                                    type="button"
                                                    onClick={() => DeleteOwner()}
                                                    className="bg-gray-900 hover:bg-gray-500 w-full mt-2 p-2 text-white uppercase font-bold"
                                                    value="Eliminar Propietario"
                                                /> : <> </>}
                                            <input
                                                type="button"
                                                onClick={() => openModal()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-2 p-2 mb-3 text-white uppercase font-bold"
                                                value="Cancelar"
                                            />
                                        </form> : <> </>}
                                    <div className="w-full max-w-3xl">
                                        <h1 className='text-3xl font-bold mb-4'>Propietarios</h1>
                                        <ul className="divide-y-2 divide-gray-200">
                                            <FlatList
                                                list={Owners}
                                                renderItem={renderObject}
                                                renderWhenEmpty={() => <div>No hay propietarios!</div>}
                                                sortBy={["name", { key: "id", descending: true }]}
                                            />
                                        </ul>
                                        <input
                                            style={{ marginTop: 40 }}
                                            type='button'
                                            className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                            value="Agregar Propietarios"
                                            onClick={() => openModal()}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Owner;
import React, { useState, useEffect } from 'react';
import FlatList from 'flatlist-react';
import ViewProperty from '../../UI/ViewProperty';
import {
    getPropertys,
    GetNumberOfBathrooms,
    GetNumberOfRooms,
    GetNumberOfFloors,
    GetPrices,
    GetAreas,
    GetYears,
    GetPropertyForViewsForNaturalPerson,
    GetPropertyForViewsForConstructor,
    GetPropertyForViewsForSearch
} from '../../../services/Shared/PropertyService';
import { getTypeOfPropertys } from '../../../services/Shared/TypeOfPropertyService';
import { getStateOfPropertys } from '../../../services/Shared/StateOfPropertyService';
import ReactLoader from '../../UI/ReactLoader';

const ListOfPropertys = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const [propertys, savePropertys] = useState([]);
    const [numberOfBathrooms, saveNumberOfBathrooms] = useState([]);
    const [numberOfRooms, saveNumberOfRooms] = useState([]);
    const [numberOfFloors, saveNumberOfFloors] = useState([]);
    const [prices, savePrices] = useState([]);
    const [areas, saveAreas] = useState([]);
    const [years, saveYears] = useState([]);
    const [statesOfPropertys, saveStatesOfPropertys] = useState([]);
    const [typesOfPropertys, saveTypesOfPropertyss] = useState([]);
    const [propertysForTypeOfOwner, savePropertysForTypeOfOwner] = useState(0);
    const [wordToSearch, saveWordToSearch] = useState('');
    const [disableFilter, saveDisableFilter] = useState(false);
    const [filter, SaveFilter] = useState({
        numberOfBathrooms: 0,
        majororminorofbathrooms: 0,
        numberOfFloors: 0,
        majororminoroffloors: 0,
        numberOfRooms: 0,
        majororminorofrooms: 0,
        area: 0,
        majororminoroarea: 0,
        state: 0,
        year: 0,
        majororminoroyear: 0,
        price: 0,
        majororminoroprice: 0,
        idTypeOfProperty: 0

    })
    const Seleccione = 0;

    const FillFilters = () => {
        saveshowLoader(true);
        GetNumberOfBathrooms()
            .then((response) => {
                saveNumberOfBathrooms(response.data.sort((a, b) => (a > b) ? 1 : -1))
                saveshowLoader(false);
            })
        GetNumberOfRooms()
            .then((response) => {
                saveNumberOfRooms(response.data.sort((a, b) => (a > b) ? 1 : -1))
                saveshowLoader(false);
            })
        GetNumberOfFloors()
            .then((response) => {
                saveNumberOfFloors(response.data.sort((a, b) => (a > b) ? 1 : -1))
                saveshowLoader(false);
            })
        GetPrices()
            .then((response) => {
                savePrices(response.data.sort((a, b) => (a > b) ? 1 : -1))
                saveshowLoader(false);
            })
        GetAreas()
            .then((response) => {
                saveAreas(response.data.sort((a, b) => (a > b) ? 1 : -1))
                saveshowLoader(false);
            })
        GetYears()
            .then((response) => {
                saveYears(response.data.sort((a, b) => (a > b) ? 1 : -1))
                saveshowLoader(false);
            })
        getStateOfPropertys()
            .then((response) => {
                saveStatesOfPropertys(response.data)
                saveshowLoader(false);
            })
        getTypeOfPropertys()
            .then((response) => {
                saveTypesOfPropertyss(response.data)
                saveshowLoader(false);
            })
    }

    const renderPropertys = (Propertys, idx) => {
        return (
            <li className="p-3  hover:text-yellow-500" key={idx} >
                <ViewProperty
                    property={Propertys} />
            </li>
        );
    }

    const getPropertysForTypeOfOwner = (e) => {
        saveshowLoader(true);
        savePropertysForTypeOfOwner(e.target.value);
        saveDisableFilter(true);
        saveWordToSearch('');
        deleteFilters();
        const filters = {
            numberOfBathrooms: 0,
            majororminorofbathrooms: 0,
            numberOfFloors: 0,
            majororminoroffloors: 0,
            numberOfRooms: 0,
            majororminorofrooms: 0,
            area: 0,
            majororminoroarea: 0,
            state: 0,
            year: 0,
            majororminoroyear: 0,
            price: 0,
            majororminoroprice: 0,
            idTypeOfProperty: 0
        }
        if (e.target.value == 1) {
            saveDisableFilter(false);
            GetPropertyForViewsForConstructor(filters)
                .then((response) => {
                    savePropertys(response.data)
                    saveshowLoader(false);
                })
        }
        else if (e.target.value == 2) {
            saveDisableFilter(false);
            GetPropertyForViewsForNaturalPerson(filters)
                .then((response) => {
                    savePropertys(response.data)
                    saveshowLoader(false);
                })
        }
        else if (e.target.value == 3) {
            getPropertys()
                .then((response) => {
                    savePropertys(response.data)
                    saveshowLoader(false);
                })
        }
    }

    const handleOnChange = (name, e) => {
        console.log(name, e.target.value)
        SaveFilter({
            ...filter,
            [name]: e.target.value,
        });
        console.log(filter)
    }

    const handleOnChangeRadio = (name, value) => {
        SaveFilter({
            ...filter,
            [name]: value,
        });
    }

    const applyFilters = () => {
        saveshowLoader(true);
        if (propertysForTypeOfOwner == 1) {
            GetPropertyForViewsForConstructor(filter)
                .then((response) => {
                    savePropertys(response.data)
                    saveshowLoader(false);
                })
        }
        else if (propertysForTypeOfOwner == 2) {
            GetPropertyForViewsForNaturalPerson(filter)
                .then((response) => {
                    savePropertys(response.data)
                    saveshowLoader(false);
                })
        }
    }

    const deleteFilters = () => {
        SaveFilter({
            ...filter,
            numberOfBathrooms: 0,
            majororminorofbathrooms: 0,
            numberOfFloors: 0,
            majororminoroffloors: 0,
            numberOfRooms: 0,
            majororminorofrooms: 0,
            area: 0,
            majororminoroarea: 0,
            state: 0,
            year: 0,
            majororminoroyear: 0,
            price: 0,
            majororminoroprice: 0,
            idTypeOfProperty: 0
        })
    }

    const search = () => {
        saveshowLoader(true);
        saveDisableFilter(true);
        savePropertysForTypeOfOwner(0)
        GetPropertyForViewsForSearch(wordToSearch)
            .then((response) => {
                savePropertys(response.data)
                saveshowLoader(false);
            })
    }

    useEffect(() => {
        FillFilters();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :

                <div className="md:flex min-h-screen">
                    <div className="md:w-2/5 xl:w-1/5 bg-gray-800">
                        <div className="p-6">
                            <p className="uppercase text-white text-2xl tracking-wide text-center font-bold">Good Luck Building</p>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-1" >Precio</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    value={filter.price}
                                    onChange={(e) => handleOnChange('price', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {prices.map(item => (
                                        <option
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.price > 0 ? false : true}
                                        checked={filter.majororminoroprice == 0}
                                        onChange={() => handleOnChangeRadio('majororminoroprice', 0)}
                                    />
                                    <label className="block text-white text-sm font-bold" >  Menor de ${filter.price}</label>
                                </div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.price > 0 ? false : true}
                                        checked={filter.majororminoroprice == 1}
                                        onChange={() => handleOnChangeRadio('majororminoroprice', 1)}
                                    />
                                    <label className="block text-white text-sm font-bold " >Mayor de ${filter.price}</label>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Metros cuadrados</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    value={filter.area}
                                    onChange={(e) => handleOnChange('area', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {areas.map(item => (
                                        <option
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.area > 0 ? false : true}
                                        checked={filter.majororminoroarea == 0}
                                        onChange={() => handleOnChangeRadio('majororminoroarea', 0)}
                                    />
                                    <label className="block text-white text-sm font-bold" >  Menos de {filter.area} metros</label>
                                </div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.area > 0 ? false : true}
                                        checked={filter.majororminoroarea == 1}
                                        onChange={() => handleOnChangeRadio('majororminoroarea', 1)}
                                    />
                                    <label className="block text-white text-sm font-bold" >Mas de {filter.area} metros</label>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Numero de habitaciones</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    value={filter.numberOfRooms}
                                    onChange={(e) => handleOnChange('numberOfRooms', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {numberOfRooms.map(item => (
                                        <option
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.numberOfRooms > 0 ? false : true}
                                        checked={filter.majororminorofrooms == 0}
                                        onChange={() => handleOnChangeRadio('majororminorofrooms', 0)}
                                    />
                                    <label className="block text-white text-sm font-bold " >  Menos de {filter.numberOfRooms} habitaciones</label>
                                </div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.numberOfRooms > 0 ? false : true}
                                        checked={filter.majororminorofrooms == 1}
                                        onChange={() => handleOnChangeRadio('majororminorofrooms', 1)}
                                    />
                                    <label className="block text-white text-sm font-bold " >Mas de {filter.numberOfRooms} habitaciones</label>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Numero de baños</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    value={filter.numberOfBathrooms}
                                    onChange={(e) => handleOnChange('numberOfBathrooms', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {numberOfBathrooms.map(item => (
                                        <option
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.numberOfBathrooms > 0 ? false : true}
                                        checked={filter.majororminorofbathrooms == 0}
                                        onChange={() => handleOnChangeRadio('majororminorofbathrooms', 0)}
                                    />
                                    <label className="block text-white text-sm font-bold " >  Menos de {filter.numberOfBathrooms} baños</label>
                                </div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.numberOfBathrooms > 0 ? false : true}
                                        checked={filter.majororminorofbathrooms == 1}
                                        onChange={() => handleOnChangeRadio('majororminorofbathrooms', 1)}
                                    />
                                    <label className="block text-white text-sm font-bold " >Mas de {filter.numberOfBathrooms} baños</label>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Numero de pisos</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"

                                    value={filter.numberOfFloors}
                                    onChange={(e) => handleOnChange('numberOfFloors', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {numberOfFloors.map(item => (
                                        <option
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.numberOfFloors > 0 ? false : true}
                                        checked={filter.majororminoroffloors == 0}
                                        onChange={() => handleOnChangeRadio('majororminoroffloors', 0)}
                                    />
                                    <label className="block text-white text-sm font-bold " >  Menos de {filter.numberOfFloors} pisos</label>
                                </div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"

                                        disabled={filter.numberOfFloors > 0 ? false : true}
                                        checked={filter.majororminoroffloors == 1}
                                        onChange={() => handleOnChangeRadio('majororminoroffloors', 1)}
                                    />
                                    <label className="block text-white text-sm font-bold " >Mas de {filter.numberOfFloors} pisos</label>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Año de construccion</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    value={filter.year}
                                    onChange={(e) => handleOnChange('year', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {years.map(item => (
                                        <option
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"
                                        disabled={filter.year > 0 ? false : true}
                                        checked={filter.majororminoroarea == 0}
                                        onChange={() => handleOnChangeRadio('majororminoroarea', 0)}
                                    />
                                    <label className="block text-white text-sm font-bold " >Menor al año {filter.year} </label>
                                </div>
                                <div style={{ display: '-webkit-box' }}>
                                    <input
                                        style={{ marginRight: 15, paddingBottom: 10 }}
                                        type="radio"

                                        disabled={filter.year > 0 ? false : true}
                                        checked={filter.majororminoroarea == 1}
                                        onChange={() => handleOnChangeRadio('majororminoroarea', 1)}
                                    />
                                    <label className="block text-white text-sm font-bold " >Mayor al año {filter.year} </label>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Estado propiedad</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"

                                    value={filter.state}
                                    onChange={(e) => handleOnChange('state', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {statesOfPropertys.map(item => (
                                        <option
                                            key={item.id}
                                            value={item.id}
                                        >
                                            {item.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-1 mt-5" >Tipo de propiedad</label>
                                <select
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    value={filter.idTypeOfProperty}
                                    onChange={(e) => handleOnChange('idTypeOfProperty', e)}>
                                    <option value={Seleccione}>-- Seleccione --</option>
                                    {typesOfPropertys.map(item => (
                                        <option
                                            key={item.id}
                                            value={item.id}
                                        >
                                            {item.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <input
                                type="button"
                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                value="Aplicar filtros"
                                disabled={disableFilter}
                                onClick={() => applyFilters()}
                            />
                            {disableFilter ? (
                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5 mt-3" role="alert">
                                    <p>Seleccione constructora o persona natural para aplicar los filtros</p>
                                </div>
                            ) : null
                            }
                            <input
                                type="button"
                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                value="Borrar filtros"
                                onClick={() => { deleteFilters() }}
                            />
                        </div>
                    </div>
                    <div className="md:w-2/5 xl:w-4/5 p-6">
                        <div className="flex justify-center">
                            <div className="w-full max-w-3xl">
                                <h1 className='text-3xl font-bold mb-2'>Listado de propiedades</h1>
                                <div className="mb-4">
                                    <label className="block text-black text-sm font-bold mb-1 mt-5" >Tipo de dueño de propiedad</label>
                                    <select
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                        id="idDepartment"
                                        name="idDepartment"
                                        value={propertysForTypeOfOwner}
                                        onChange={(e) => getPropertysForTypeOfOwner(e)}>
                                        <option value={Seleccione}>-- Seleccione --</option>
                                        <option value="1">Constructora</option>
                                        <option value="2">Persona natural</option>
                                        <option value="3">TODOS</option>
                                    </select>
                                </div>
                                <div className="mb-4">
                                    <input
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                        id="name"
                                        type="text"
                                        value={wordToSearch}
                                        placeholder="Busca por nombre de propiedad,ciudad,pais,departamento o barrio"
                                        onChange={(e) => { saveWordToSearch(e.target.value) }}
                                    />
                                    <input
                                        type="button"
                                        className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                        value="Busqueda personalizada"
                                        onChange={(e) => getPropertysForTypeOfOwner(e)}
                                        onClick={() => search()}
                                    />
                                </div>
                                <div className="mb-4">
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={propertys}
                                            renderItem={renderPropertys}
                                            renderWhenEmpty={() => <div>No hay propiedades!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default ListOfPropertys;
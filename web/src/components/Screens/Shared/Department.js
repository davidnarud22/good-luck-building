import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getDepartments, addDepartment, editDepartment, deleteDepartment } from '../../../services/Shared/DepartmentService';
import { getCitys } from '../../../services/Shared/CityService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar';
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const Department = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const [cities, saveCitys] = useState([]);
    const [departments, saveDepartments] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idDepartment, saveIdDepartment] = useState(0);
    const navigate = useNavigate();

    const formik = useFormik({
        initialValues: {
            name: '',
            idCity: '',
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El Nombre de la Departamento deben tener al menos 3 caracteres')
                .required('El Nombre de la Departamento es obligatorio'),
            idCity: Yup.string()
                .required('La ciudad es obligatorio'),
        }),
        onSubmit: Departamento => {
            try {
                saveshowLoader(true);
                if (idDepartment > 0) {
                    editDepartment(Departamento, idDepartment)
                        .then(() => {
                            GetDepartments();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        })
                    saveIdDepartment('')
                }
                else {
                    addDepartment(Departamento)
                        .then(() => {
                            GetDepartments();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        })
                }
            } catch (error) {
                console.log(error)
            }
        }
    });

    const GetDepartments = () => {
        saveshowLoader(true);
        getDepartments()
            .then((response) => {
                saveDepartments(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetCities = () => {
        saveshowLoader(true);
        getCitys()
            .then((response) => {
                saveCitys(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }


    const renderObject = (cities, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickDepartment(cities)}>
                {cities.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
        formik.initialValues.idCity = '';
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idDepartment > 0) {
            saveIdDepartment(0)
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickDepartment = (Departamento) => {
        openModal();
        saveIdDepartment(Departamento.id)
        formik.initialValues.name = Departamento.name
        formik.initialValues.idCity = Departamento.idCity;
    }
    const DeleteDepartment = () => {
        saveshowLoader(true);
        deleteDepartment(idDepartment)
            .then(() => {
                GetDepartments();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetDepartments();
        GetCities();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :

                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Departamento"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre departamento</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre departamento"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idCity">Ciudad</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idCity"
                                                    name="idCity"
                                                    value={formik.values.idCity}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {cities.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idCity && formik.errors.idCity ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idCity}</p>
                                                </div>
                                            ) : null
                                            }

                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idDepartment > 0 ? "Editar Departamento" : "Agregar Departamento"}
                                            />
                                            {idDepartment > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteDepartment()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar Departamento"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Departamentos</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={departments}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay departamentos!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Departamento"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Department;
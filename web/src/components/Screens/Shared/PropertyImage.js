import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getPropertyImages, addPropertyImage, editPropertyImage, deletePropertyImage, getLastOrderOfImagenForProyect } from '../../../services/Shared/PropertyImageService';
import { getPropertys } from '../../../services/Shared/PropertyService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const PropertyImage = () => {
    const navigate = useNavigate();
    const [PropertyImages, savePropertyImages] = useState([]);
    const [namePropertyImages, saveNamePropertyImages] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idPropertyImage, saveIdPropertyImage] = useState('');
    const [photo, savePhoto] = useState('');
    const [propertys, savePropertys] = useState([]);
    const [toggle, setToggle] = useState(false);
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            file: '',
            enable: true,
            idProperty: '',
            order: 0
        },
        validationSchema: Yup.object({
            file: Yup.string()
                .required('La imagen es obligatoria'),
            idProperty: Yup.string()
                .required('La propiedad es  obligatoria'),
            order: Yup.string()
                .required('La posicion es  obligatoria'),

        }),
        onSubmit: PropertyImage => {
            try {
                saveshowLoader(true);
                if (idPropertyImage > 0) {
                    propertys.forEach(element => {
                        if (element.id == PropertyImage.idProperty) {
                            PropertyImage.nameProperty = element.name
                        }
                    });
                    PropertyImage.file = photo;
                    editPropertyImage(PropertyImage, idPropertyImage)
                        .then(() => {
                            closeModal();
                            GetPropertyImages();
                            formik.resetForm();
                            closeModal();
                            saveIdPropertyImage('')
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
                else {
                    propertys.forEach(element => {
                        if (element.id == PropertyImage.idProperty) {
                            PropertyImage.nameProperty = element.name;
                        }
                    });

                    if (PropertyImage.id > 0) {
                        delete PropertyImage.id;
                    }

                    PropertyImage.file = photo;
                    addPropertyImage(PropertyImage)
                        .then(() => {
                            GetPropertyImages();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });

                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetPropertys = () => {
        saveshowLoader(true);
        getPropertys()
            .then((response) => {
                savePropertys(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetPropertyImages = () => {
        saveshowLoader(true);
        getPropertyImages()
            .then((response) => {
                savePropertyImages(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (PropertyImages, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500  hover:text-white" key={idx} onClick={() => clickPropertyImage(PropertyImages)}>
                {PropertyImages.order + ": " + PropertyImages.nameProperty}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
    }

    const closeModal = () => {
        setIsOpen(false);
        savePhoto('')
        if (idPropertyImage > 0) {
            saveIdPropertyImage('')
            saveNamePropertyImages('')
        }
        formik.initialValues.idProperty = '';
        formik.initialValues.order = 0
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickPropertyImage = (PropertyImage) => {
        openModal();
        saveIdPropertyImage(PropertyImage.id)
        savePhoto(PropertyImage.file)
        propertys.forEach(element => {
            if (element.id == PropertyImage.idProperty) {
                saveNamePropertyImages(element.name)
            }
        });
        formik.initialValues.file = PropertyImage.file
        formik.initialValues.enable = PropertyImage.enable;
        formik.initialValues.idProperty = PropertyImage.idProperty;
        formik.initialValues.order = PropertyImage.order;
    }

    const DeletePropertyImage = () => {
        saveshowLoader(true);
        deletePropertyImage(idPropertyImage)
            .then(() => {
                GetPropertyImages();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const imageUpload = (image) => {
        const file = image.target.files[0];
        convertBase64(file).then(base64 => {
            savePhoto(base64)
            formik.initialValues.file = base64
            localStorage["fileBase64"] = base64;
            console.debug("file stored", base64);
        });
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            if (!!file) {
                fileReader.readAsDataURL(file)
                fileReader.onload = () => {
                    resolve(fileReader.result);
                }
                fileReader.onerror = (error) => {
                    reject(error);
                }
            }
        })
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetPropertyImages();
        GetPropertys();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Imagen de propiedad"
                                        appElement={document.getElementById('root')}
                                    >
                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="photo">Foto Propietario</label>
                                                <input
                                                    type="file"
                                                    id="imageFile"
                                                    name='imageFile'
                                                    onChange={(file) => imageUpload(file)} />
                                            </div>
                                            {!!photo ? <img src={photo} alt="Foto" /> : <></>}
                                            <div className="mb-4" style={{ marginTop: 20 }}>
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idProperty">Propiedad</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idProperty"
                                                    name="idProperty"
                                                    value={formik.values.idProperty}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {propertys.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idProperty && formik.errors.idProperty ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idProperty}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4" style={{ marginTop: 10 }}>
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="enablePhoto">Activar Imagen</label>
                                                <input
                                                    type="checkbox"
                                                    id="enablePhoto"
                                                    name='enablePhoto'
                                                    defaultChecked={toggle}
                                                    onChange={() => setToggle(!toggle)} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="order">Posicion de la imagen</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="order"
                                                    type="number"
                                                    placeholder="Posicion de la imagen"
                                                    value={formik.values.order}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.order && formik.errors.order ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.order}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idPropertyImage > 0 ? "Editar Imagen de propiedad" : "Agregar Imagen de propiedad"}
                                            />
                                            {idPropertyImage > 0 ? <input
                                                type="button"
                                                onClick={() => DeletePropertyImage()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar Imagen de propiedad"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Imagenes de Propiedades</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={PropertyImages}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Imagen de propiedades!</div>}
                                            sortBy={["nameProperty", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Imagen de propiedad"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default PropertyImage;
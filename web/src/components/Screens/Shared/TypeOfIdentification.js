import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getTypeOfIdentifications, addTypeOfIdentification, editTypeOfIdentification, deleteTypeOfIdentification } from '../../../services/Shared/TypeOfIdentificationService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const TypeOfIdentification = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const navigate = useNavigate();
    const [TypeOfIdentifications, saveTypeOfIdentifications] = useState([]);
    const [nameTypeOfIdentifications, saveNameTypeOfIdentifications] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idTypeOfIdentification, saveIdTypeOfIdentification] = useState('');

    const formik = useFormik({
        initialValues: {
            name: '',
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El tipo de identificacion debe tener al menos 3 caracteres')
                .required('El tipo de identificacion es obligatorio'),

        }),
        onSubmit: TypeOfIdentification => {
            try {
                saveshowLoader(true);
                if (idTypeOfIdentification > 0) {
                    editTypeOfIdentification(TypeOfIdentification, idTypeOfIdentification)
                        .then(() => {
                            GetTypeOfIdentifications();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(true);
                        }).catch((err) => {
                            navigate('/')
                        })
                    saveIdTypeOfIdentification('')
                }
                else {
                    addTypeOfIdentification(TypeOfIdentification)
                        .then(() => {
                            GetTypeOfIdentifications();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(true);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });


    const GetTypeOfIdentifications = () => {
        saveshowLoader(true);
        getTypeOfIdentifications()
            .then((response) => {
                saveTypeOfIdentifications(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }



    const renderObject = (TypeOfIdentifications, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickTypeOfIdentification(TypeOfIdentifications)}>
                {TypeOfIdentifications.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idTypeOfIdentification > 0) {
            saveIdTypeOfIdentification('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickTypeOfIdentification = (TypeOfIdentification) => {
        openModal();
        saveIdTypeOfIdentification(TypeOfIdentification.id)
        formik.initialValues.name = TypeOfIdentification.name
        formik.initialValues.idTypeOfIdentification = TypeOfIdentification.idTypeOfIdentification;
    }
    const DeleteTypeOfIdentification = () => {
        saveshowLoader(true);
        deleteTypeOfIdentification(idTypeOfIdentification)
            .then(() => {
                GetTypeOfIdentifications();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetTypeOfIdentifications();
        GetTypeOfIdentifications();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Tipo de Identificacion"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Tipo de identificacion</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre ciudad"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idTypeOfIdentification > 0 ? "Editar tipo" : "Agregar Tipo"}
                                            />
                                            {idTypeOfIdentification > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteTypeOfIdentification()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar ciudad"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Tipos de identificacion</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={TypeOfIdentifications}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Tipos de identificacion!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Tipo de Identificacion"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default TypeOfIdentification;
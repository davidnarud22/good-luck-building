import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getProjects, addProject, editProject, deleteProject } from '../../../services/Shared/ProjectService';
import { getMacroprojects } from '../../../services/Shared/MacroprojectService';
import { getCitiesForCountry } from '../../../services/Shared/CityService';
import { getDepartmentsForCity } from '../../../services/Shared/DepartmentService';
import { getOwners } from '../../../services/Shared/OwnerService';
import { getCountries } from '../../../services/Shared/CountryService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const Project = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const navigate = useNavigate();
    const [proyects, saveProyects] = useState([]);
    const [cities, saveCities] = useState([]);
    const [countries, saveCountries] = useState([]);
    const [departments, saveDepartments] = useState([]);
    const [macroprojects, saveMacroprojects] = useState([]);
    const [owners, saveOwners] = useState([]);
    const [isCityDisabled, saveIsCityDisabled] = useState(true);
    const [isDepartmentDisabled, saveIsDepartmentDisabled] = useState(true);

    const [modalIsOpen, setIsOpen] = useState(false);
    const [idProject, saveIdProject] = useState(0);

    const formik = useFormik({
        initialValues: {
            name: '',
            idCountry: '',
            idCity: '',
            idDepartment: '',
            idCountry: '',
            idMacroProject: '',
            idOwner: ''
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El Nombre del proyecto deben tener al menos 3 caracteres')
                .required('El Nombre del proyecto es obligatorio'),
            idCountry: Yup.string()
                .required('El Pais es obligatorio'),
            idCity: Yup.string()
                .required('La ciudad es obligatoria'),
            idDepartment: Yup.string()
                .required('El Departamento es obligatorio'),
            idMacroProject: Yup.string()
                .required('El Macroproyecto es obligatorio'),
            idOwner: Yup.string()
                .required('El Propietario es obligatorio'),
        }),
        onSubmit: proyecto => {
            try {
                saveshowLoader(true);
                if (idProject > 0) {
                    editProject(proyecto, idProject)
                        .then(() => {
                            GetProjects();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        })
                    saveIdProject('')
                }
                else {
                    addProject(proyecto)
                        .then(() => {
                            GetProjects();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        });


                }
            } catch (error) {
                console.log(error)
            }

        }
    });


    const GetProjects = () => {
        saveshowLoader(true);
        getProjects()
            .then((response) => {
                saveProyects(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetCountries = () => {
        saveshowLoader(true);
        getCountries()
            .then((response) => {
                saveCountries(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetCityForCountry = (e) => {
        saveshowLoader(true);
        saveIsCityDisabled(true)
        if (!!e.target.value) {
            getCitiesForCountry(e.target.value)
                .then((response) => {
                    saveIsCityDisabled(false);
                    saveCities(response.data)
                    saveshowLoader(false);
                })
                .catch((err) => {
                    navigate('/')
                });
        }
        else {
            saveIsCityDisabled(true);
            saveIsDepartmentDisabled(true);
            formik.initialValues.idCity = '';
            formik.initialValues.idDepartment = '';
        }
    }

    const GetDepartmentsForCity = (e) => {
        saveshowLoader(true);
        saveIsDepartmentDisabled(true)
        if (!!e.target.value) {
            getDepartmentsForCity(e.target.value)
                .then((response) => {
                    saveIsDepartmentDisabled(false)
                    saveDepartments(response.data);
                    saveshowLoader(false);
                }).catch((err) => {
                    navigate('/')
                });
        }
        else {
            saveIsDepartmentDisabled(true)
            formik.initialValues.idDepartment = '';
        }
    }

    const getCitiesAndDepartmens = (idCountry, idCity) => {
        saveshowLoader(true);
        getCitiesForCountry(idCountry)
            .then((response) => {
                saveIsCityDisabled(false);
                saveCities(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
        getDepartmentsForCity(idCity)
            .then((response) => {
                saveIsDepartmentDisabled(false)
                saveDepartments(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetMacroprojects = () => {
        saveshowLoader(true);
        getMacroprojects()
            .then((response) => {
                saveMacroprojects(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetOwners = () => {
        saveshowLoader(true);
        getOwners()
            .then((response) => {
                saveOwners(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (cities, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickProject(cities)}>
                {cities.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
        formik.initialValues.idCountry = '';
        formik.initialValues.idCity = '';
        formik.initialValues.idDepartment = '';
        formik.initialValues.idCountry = '';
        formik.initialValues.idMacroProject = '';
        formik.initialValues.idOwner = '';
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idProject > 0) {
            saveIdProject(0)
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickProject = (proyecto) => {
        openModal();
        saveIdProject(proyecto.id)
        saveIsCityDisabled(false);
        saveIsDepartmentDisabled(false)
        formik.initialValues.name = proyecto.name
        formik.initialValues.idCountry = proyecto.idCountry;
        formik.initialValues.idCity = proyecto.idCity;
        formik.initialValues.idDepartment = proyecto.idDepartment;
        formik.initialValues.idCountry = proyecto.idCountry;
        formik.initialValues.idMacroProject = proyecto.idMacroProject;
        formik.initialValues.idOwner = proyecto.idOwner;
        getCitiesAndDepartmens(proyecto.idCountry, proyecto.idCity);
    }
    const DeleteProject = () => {
        saveshowLoader(true);
        deleteProject(idProject)
            .then(() => {
                GetProjects();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetProjects();
        GetCountries();
        GetOwners();
        GetMacroprojects();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar proyecto"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idMacroProject">Macroproyecto</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idMacroProject"
                                                    name="idMacroProject"
                                                    value={formik.values.idMacroProject}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {macroprojects.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idMacroProject && formik.errors.idMacroProject ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idMacroProject}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre proyecto</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre proyecto"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }

                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idCountry">Pais</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idCountry"
                                                    name="idCountry"
                                                    value={formik.values.idCountry}
                                                    onChange={(e) => { formik.handleChange(e); GetCityForCountry(e) }}>
                                                    <option value="">-- Seleccione --</option>
                                                    {countries.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idCountry && formik.errors.idCountry ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idCountry}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idCity">Ciudad</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idCity"
                                                    name="idCity"
                                                    value={formik.values.idCity}
                                                    disabled={isCityDisabled}
                                                    onChange={(e) => { formik.handleChange(e); GetDepartmentsForCity(e) }}>
                                                    <option value="">-- Seleccione --</option>
                                                    {cities.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idCity && formik.errors.idCity ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idCity}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idDepartment">Departamento</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idDepartment"
                                                    name="idDepartment"
                                                    value={formik.values.idDepartment}
                                                    disabled={isDepartmentDisabled}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {departments.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idDepartment && formik.errors.idDepartment ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idDepartment}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idOwner">Propietario</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idOwner"
                                                    name="idOwner"
                                                    value={formik.values.idOwner}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {owners.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.fullName}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idOwner && formik.errors.idOwner ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idOwner}</p>
                                                </div>
                                            ) : null
                                            }

                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idProject > 0 ? "Editar proyecto" : "Agregar proyecto"}
                                            />
                                            {idProject > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteProject()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar proyecto"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Proyectos</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={proyects}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay proyectos!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar proyecto"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Project;
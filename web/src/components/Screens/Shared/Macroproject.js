import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getMacroprojects, addMacroproject, editMacroproject, deleteMacroproject } from '../../../services/Shared/MacroprojectService';
import { getOwners } from '../../../services/Shared/OwnerService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar';
import { useNavigate } from 'react-router-dom';
import ReactLoader from '../../UI/ReactLoader';

const Macroproject = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const navigate = useNavigate();
    const [owners, saveOwners] = useState([]);
    const [macroprojects, saveMacroprojects] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idMacroproject, saveIdMacroproject] = useState(0);

    const formik = useFormik({
        initialValues: {
            name: '',
            idOwner: '',
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El Nombre del Macroproyecto deben tener al menos 3 caracteres')
                .required('El Nombre del Macroproyecto es obligatorio'),
            idOwner: Yup.string()
                .required('El propietario es obligatorio'),
        }),
        onSubmit: Macroproyecto => {
            try {
                saveshowLoader(true);
                if (idMacroproject > 0) {
                    editMacroproject(Macroproyecto, idMacroproject)
                        .then(() => {
                            GetMacroprojects();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        })
                    saveIdMacroproject('')
                }
                else {
                    addMacroproject(Macroproyecto)
                        .then(() => {
                            GetMacroprojects();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        });

                }
            } catch (error) {
                console.log(error)
            }

        }
    });


    const GetMacroprojects = () => {
        saveshowLoader(true);
        getMacroprojects()
            .then((response) => {
                saveMacroprojects(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetOwners = () => {
        saveshowLoader(true);
        getOwners()
            .then((response) => {
                saveOwners(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }


    const renderObject = (cities, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickMacroproject(cities)}>
                {cities.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
        formik.initialValues.idOwner = '';
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idMacroproject > 0) {
            saveIdMacroproject(0)
        }

    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickMacroproject = (Macroproyecto) => {
        openModal();
        saveIdMacroproject(Macroproyecto.id)
        formik.initialValues.name = Macroproyecto.name
        formik.initialValues.idOwner = Macroproyecto.idOwner;
    }
    const DeleteMacroproject = () => {
        saveshowLoader(true);
        deleteMacroproject(idMacroproject)
            .then(() => {
                GetMacroprojects();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetMacroprojects();
        GetOwners();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Macroproyecto"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre Macroproyecto</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre Macroproyecto"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idOwner">Propietarios</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idOwner"
                                                    name="idOwner"
                                                    value={formik.values.idOwner}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {owners.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.fullName}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idOwner && formik.errors.idOwner ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idOwner}</p>
                                                </div>
                                            ) : null
                                            }

                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idMacroproject > 0 ? "Editar Macroproyecto" : "Agregar Macroproyecto"}
                                            />
                                            {idMacroproject > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteMacroproject()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar Macroproyecto"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Macroproyectos</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={macroprojects}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Macroproyectos!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Macroproyecto"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Macroproject;
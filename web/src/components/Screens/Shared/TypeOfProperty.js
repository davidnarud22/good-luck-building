import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getTypeOfPropertys, addTypeOfProperty, editTypeOfProperty, deleteTypeOfProperty } from '../../../services/Shared/TypeOfPropertyService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const TypeOfProperty = () => {
    const navigate = useNavigate();
    const [TypeOfProperty, saveTypeOfProperty] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idTypeOfProperty, saveIdTypeOfProperty] = useState('');
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            name: '',
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El Nombre del tipo de propiedad deben tener al menos 3 caracteres')
                .required('El Nombre del tipo de propiedad es obligatorio'),

        }),
        onSubmit: TypeOfProperty => {
            try {
                saveshowLoader(true);

                if (idTypeOfProperty > 0) {
                    editTypeOfProperty(TypeOfProperty, idTypeOfProperty)
                        .then(() => {
                            GetTypeOfProperty();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        })
                    saveIdTypeOfProperty('')
                }
                else {
                    addTypeOfProperty(TypeOfProperty)
                        .then(() => {
                            GetTypeOfProperty();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetTypeOfProperty = () => {
        saveshowLoader(true);
        getTypeOfPropertys()
            .then((response) => {
                saveTypeOfProperty(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (TypeOfProperty, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickTypeOfProperty(TypeOfProperty)}>
                {TypeOfProperty.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idTypeOfProperty > 0) {
            saveIdTypeOfProperty('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickTypeOfProperty = (TypeOfProperty) => {
        openModal();
        saveIdTypeOfProperty(TypeOfProperty.id)
        formik.initialValues.name = TypeOfProperty.name
        formik.initialValues.idTypeOfProperty = TypeOfProperty.idTypeOfProperty;
    }

    const DeleteTypeOfProperty = () => {
        saveshowLoader(true);
        deleteTypeOfProperty(idTypeOfProperty)
            .then(() => {
                GetTypeOfProperty();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetTypeOfProperty();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar ciudad"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre tipo de pago</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre ciudad"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idTypeOfProperty > 0 ? "Editar Ciudad" : "Agregar Ciudad"}
                                            />
                                            {idTypeOfProperty > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteTypeOfProperty()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar ciudad"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Tipos de propiedades</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={TypeOfProperty}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay ciudades!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500, inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar tipo de propiedad"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default TypeOfProperty;
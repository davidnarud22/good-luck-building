import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getPropertys, addProperty, editProperty, deleteProperty } from '../../../services/Shared/PropertyService';
import { getStateOfPropertys } from '../../../services/Shared/StateOfPropertyService';
import { getProjectsForMacroProjects, getProject } from '../../../services/Shared/ProjectService';
import { getMacroprojects } from '../../../services/Shared/MacroprojectService';
import { getTypeOfPropertys } from '../../../services/Shared/TypeOfPropertyService';
import { getOwners } from '../../../services/Shared/OwnerService';
import { addPropertyImages } from '../../../services/Shared/PropertyImageService';
import { getDepartments } from '../../../services/Shared/DepartmentService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar';
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const Property = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const navigate = useNavigate();
    const [Propertys, savePropertys] = useState([]);
    const [formIsOpen, setFormIsOpen] = useState(false);
    const [idProperty, saveIdProperty] = useState('');
    const [idMacroproyecto, saveIdMacroproyecto] = useState('');
    const [photo, savePhoto] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [statesOfProperty, saveStatesOfProperty] = useState([]);
    const [macroProjects, saveMacroProjects] = useState([]);
    const [projects, saveProjects] = useState([]);
    const [typesOfPropertys, saveTypesOfPropertys] = useState([]);
    const [owners, saveOwners] = useState([]);
    const [departments, saveDepartments] = useState([]);
    const [isProjectDisabled, saveIsProjectDisabled] = useState(true);
    const [imagensOfProperty, saveImagensOfProperty] = useState([]);
    const [toggle, setToggle] = useState(true);

    const formik = useFormik({
        initialValues: {
            name: '',
            address: '',
            idProject: 0,
            idDepartment: '',
            idTypeOfProperty: '',
            price: '',
            area: '',
            numberOfFloors: '',
            numberOfRooms: '',
            numberOfBathrooms: '',
            codeInternal: '',
            neighborhood: '',
            year: '',
            state: '',
            idOwner: ''
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El nombre de la propiedad debe tener al menos 3 caracteres')
                .required('El nombre de la propiedad es obligatorio'),
            address: Yup.string()
                .min(3, 'La direccion debe tener al menos 3 caracteres')
                .required('La direccion es obligatoria'),
            idTypeOfProperty: Yup.string()
                .required('El tipo de propiedad es obligatoria'),
            price: Yup.string()
                .min(3, 'El precio debe tener al menos 3 caracteres')
                .required('El precio es obligatorio'),
            area: Yup.string()
                .required('El area es obligatorio'),
            numberOfFloors: Yup.string()
                .required('El numero de pisos es obligatorio'),
            numberOfRooms: Yup.string()
                .required('El numero de habitaciones es obligatorio'),
            numberOfBathrooms: Yup.string()
                .required('El numero de baños es obligatorio'),
            codeInternal: Yup.string()
                .min(3, 'El codigo interno debe tener al menos 3 caracteres')
                .required('El codigo interno es obligatorio'),
            year: Yup.string()
                .min(4, 'El año de construccion debe tener al menos 3 caracteres')
                .required('El año de construccion es obligatorio'),
            state: Yup.string()
                .required('El estado es obligatorio'),
            idOwner: Yup.string()
                .required('El propietario es obligatorio'),
            idDepartment: Yup.string()
                .required('El departamento es obligatorio'),
            neighborhood: Yup.string()
                .required('El barrio es obligatorio'),
        }),
        onSubmit: Property => {
            try {
                saveshowLoader(true);
                if (idProperty > 0) {
                    console.log(Property)
                    editProperty(Property, idProperty)
                        .then(() => {
                            GetPropertys();
                            formik.resetForm();
                            openForm();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        })
                    saveIdProperty('')
                }
                else {
                    addProperty(Property)
                        .then((response) => {
                            let Count = 0
                            let newImagensProperty = []
                            imagensOfProperty.forEach(element => {
                                const counter = Count == 0 ? 1 : Count;
                                const newImagenProperty = {
                                    file: element.file == '' ? 'no tiene imagen' : element.file,
                                    enable: element.enable,
                                    idProperty: response.data.id,
                                    nameProperty: response.data.name,
                                    order: counter
                                }
                                Count = Count + 1;
                                newImagensProperty.push(newImagenProperty);
                            });
                            addPropertyImages(newImagensProperty)
                                .then((response) => {
                                    GetPropertys();
                                    formik.resetForm();
                                    openForm();
                                    saveshowLoader(false);
                                }).catch((err) => {
                                    navigate('/')
                                });
                        });
                }
            } catch (error) {
                console.log(error)
            }
        }
    });

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const addPhotoProperty = () => {
        const newImagenProperty = {
            file: photo,
            enable: toggle,
            idProperty: 0,
            nameProperty: '',
            order: 0
        }
        saveImagensOfProperty([
            ...imagensOfProperty,
            newImagenProperty
        ]);
        closeModalImage();
    }

    const openModalImage = () => {
        savePhoto('');
        setToggle(false);
        setModalIsOpen(true);
    }

    const closeModalImage = () => {
        setModalIsOpen(false);
    }

    const GetPropertys = () => {
        saveshowLoader(true);
        getPropertys()
            .then((response) => {
                savePropertys(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (Propertys, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500  hover:text-gray-800" key={idx} onClick={() => clickProperty(Propertys)}>
                {"Identificador :" + Propertys.id + " - " + Propertys.name + "- Codigo interno: " + Propertys.codeInternal}
            </li>
        );
    }

    const openForm = () => {
        setFormIsOpen(!formIsOpen);
        formik.resetForm();
        if (idProperty > 0) {
            saveIdProperty(0)
            saveIdMacroproyecto(0)
            formik.initialValues.name = '';
            formik.initialValues.address = '';
            formik.initialValues.idProject = '';
            formik.initialValues.idTypeOfProperty = '';
            formik.initialValues.price = '';
            formik.initialValues.area = '';
            formik.initialValues.numberOfFloors = '';
            formik.initialValues.numberOfRooms = '';
            formik.initialValues.numberOfBathrooms = '';
            formik.initialValues.codeInternal = '';
            formik.initialValues.year = '';
            formik.initialValues.state = '';
            formik.initialValues.idOwner = '';
        }
    }

    const clickProperty = (Property) => {
        saveshowLoader(true);
        openForm();
        saveIdProperty(Property.id);
        getProject(Property.idProject)
            .then((response) => {
                saveIdMacroproyecto(response.data.idMacroProject)
                saveIsProjectDisabled(false);
                const e = {
                    target: {
                        value: response.data.idMacroProject
                    }
                }
                GetProjectsForMacroProjects(e);
                saveshowLoader(false);
            })
        formik.initialValues.name = Property.name;
        formik.initialValues.address = Property.address;
        formik.initialValues.idProject = Property.idProject;
        formik.initialValues.idTypeOfProperty = Property.idTypeOfProperty;
        formik.initialValues.price = Property.price;
        formik.initialValues.area = Property.area;
        formik.initialValues.numberOfFloors = Property.numberOfFloors;
        formik.initialValues.numberOfRooms = Property.numberOfRooms;
        formik.initialValues.numberOfBathrooms = Property.numberOfBathrooms;
        formik.initialValues.codeInternal = Property.codeInternal;
        formik.initialValues.year = Property.year;
        formik.initialValues.state = Property.state;
        formik.initialValues.idOwner = Property.idOwner;
        formik.initialValues.neighborhood = Property.neighborhood;
        formik.initialValues.idDepartment = Property.idDepartment;

    }
    const DeleteProperty = () => {
        saveshowLoader(true);
        deleteProperty(idProperty)
            .then(() => {
                GetPropertys();
                formik.resetForm();
                openForm();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetProjectsForMacroProjects = (e) => {
        if (!!e.target.value) {
            saveshowLoader(true);
            saveIdMacroproyecto(e.target.value)
            getProjectsForMacroProjects(e.target.value)
                .then((response) => {
                    saveIsProjectDisabled(false);
                    saveProjects(response.data);
                    saveshowLoader(false);
                }).catch((err) => {
                    navigate('/')
                });
        }
        else {
            saveIsProjectDisabled(true);
        }
    }

    const GetMacroprojects = () => {
        saveshowLoader(true);
        getMacroprojects()
            .then((response) => {
                saveMacroProjects(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetTypesOfPropertys = () => {
        saveshowLoader(true);
        getTypeOfPropertys()
            .then((response) => {
                saveTypesOfPropertys(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetStatesOfProperty = () => {
        saveshowLoader(true);
        getStateOfPropertys()
            .then((response) => {
                saveStatesOfProperty(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                alert(err);
            });
    }

    const GetOwners = () => {
        saveshowLoader(true);
        getOwners()
            .then((response) => {
                saveOwners(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetDepartments = () => {
        saveshowLoader(true);
        getDepartments()
            .then((response) => {
                saveDepartments(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetPropertys();
        GetTypesOfPropertys();
        GetStatesOfProperty();
        GetMacroprojects();
        GetOwners();
        GetDepartments();
    }, [])

    const imageUpload = (image) => {
        const file = image.target.files[0];
        convertBase64(file).then(base64 => {
            savePhoto(base64)
            localStorage["fileBase64"] = base64;
            console.debug("file stored", base64);
        });
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            if (!!file) {
                fileReader.readAsDataURL(file)
                fileReader.onload = () => {
                    resolve(fileReader.result);
                }
                fileReader.onerror = (error) => {
                    reject(error);
                }
            }
        })
    }

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :

                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div className="w-full max-w-3xl">
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModalImage}
                                        style={customStyles}
                                        contentLabel="Agregar pais"
                                        appElement={document.getElementById('root')}
                                    >
                                        <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                            <button onClick={closeModalImage}>X</button>
                                        </div>
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="photo">Foto Propietario</label>
                                            <input
                                                type="file"
                                                id="imageFile"
                                                name='imageFile'
                                                onChange={(file) => imageUpload(file)} />
                                        </div>
                                        {!!photo ? <img src={photo} alt="Foto" /> : <></>}
                                        <div className="mb-4" style={{ marginTop: 40 }}>
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="enablePhoto">Activar Imagen</label>
                                            <input
                                                type="checkbox"
                                                id="enablePhoto"
                                                name='enablePhoto'
                                                onChange={() => setToggle(!toggle)} />
                                        </div>
                                        <div className="mb-4">
                                            <input
                                                type="button"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Guardar imagen"
                                                onClick={() => addPhotoProperty()}
                                            />
                                        </div>
                                    </Modal>

                                    {formIsOpen ? <form
                                        onSubmit={formik.handleSubmit}
                                    >
                                        <h1 className='text-3xl font-bold mb-4'>Agregar propiedad</h1>
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre Propiedad</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="name"
                                                type="text"
                                                placeholder="Nombre propiedad"
                                                value={formik.values.name}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.name && formik.errors.name ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.name}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idDepartment">Departamento</label>
                                            <select
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="idDepartment"
                                                name="idDepartment"
                                                value={formik.values.idDepartment}
                                                onChange={formik.handleChange}>
                                                <option value="">-- Seleccione --</option>
                                                {departments.map(item => (
                                                    <option
                                                        key={item.id}
                                                        value={item.id}
                                                    >
                                                        {item.name}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {formik.touched.idDepartment && formik.errors.idDepartment ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.idDepartment}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="address">Direccion</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="address"
                                                type="text"
                                                placeholder="Nombre propiedad"
                                                value={formik.values.address}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.address && formik.errors.address ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.address}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="neighborhood">Barrio</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="neighborhood"
                                                type="text"
                                                placeholder="Barrio"
                                                value={formik.values.neighborhood}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.neighborhood && formik.errors.neighborhood ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.neighborhood}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="Macroproyecto">Macroproyecto</label>
                                            <select
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="Macroproyecto"
                                                name="Macroproyecto"
                                                value={idMacroproyecto}
                                                onChange={(e) => GetProjectsForMacroProjects(e)}>
                                                <option value="">-- Seleccione --</option>
                                                {macroProjects.map(item => (
                                                    <option
                                                        key={item.id}
                                                        value={item.id}
                                                    >
                                                        {item.name}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idProject">Proyecto</label>
                                            <select
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="idProject"
                                                name="idProject"
                                                value={formik.values.idProject}
                                                disabled={isProjectDisabled}
                                                onChange={formik.handleChange}>
                                                <option value='0'>-- Seleccione --</option>
                                                {projects.map(item => (
                                                    <option
                                                        key={item.id}
                                                        value={item.id}
                                                    >
                                                        {item.name}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idTypeOfProperty">Tipo de propiedad</label>
                                            <select
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="idTypeOfProperty"
                                                name="idTypeOfProperty"
                                                value={formik.values.idTypeOfProperty}
                                                onChange={formik.handleChange}>
                                                <option value="">-- Seleccione --</option>
                                                {typesOfPropertys.map(item => (
                                                    <option
                                                        key={item.id}
                                                        value={item.id}
                                                    >
                                                        {item.name}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {formik.touched.idTypeOfProperty && formik.errors.idTypeOfProperty ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.idTypeOfProperty}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="price">Precio</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="price"
                                                type="number"
                                                placeholder="Precio"
                                                value={formik.values.price}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.price && formik.errors.price ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.price}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="area">Area(m²)</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="area"
                                                name="area"
                                                type="number"
                                                disabled={false}
                                                value={formik.values.area}
                                                onChange={formik.handleChange} />
                                        </div>
                                        {formik.touched.area && formik.errors.area ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.area}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="numberOfFloors">Numero de pisos</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="numberOfFloors"
                                                type="number"
                                                placeholder="Numero de pisos"
                                                value={formik.values.numberOfFloors}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.numberOfFloors && formik.errors.numberOfFloors ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.numberOfFloors}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="numberOfRooms">Numero de cuartos</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="numberOfRooms"
                                                type="number"
                                                placeholder="Numero de cuartos"
                                                value={formik.values.numberOfRooms}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.numberOfRooms && formik.errors.numberOfRooms ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.numberOfRooms}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="numberOfBathrooms">Numero de baños</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="numberOfBathrooms"
                                                type="number"
                                                placeholder="Numero de baños"
                                                value={formik.values.numberOfBathrooms}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.numberOfBathrooms && formik.errors.numberOfBathrooms ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.numberOfBathrooms}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="codeInternal">Codigo interno</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="codeInternal"
                                                type="number"
                                                placeholder="Codigo interno"
                                                value={formik.values.codeInternal}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.codeInternal && formik.errors.codeInternal ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.codeInternal}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="year">Año de construccion</label>
                                            <input
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="year"
                                                type="number"
                                                placeholder="Año de construccion"
                                                value={formik.values.year}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </div>
                                        {formik.touched.year && formik.errors.year ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.year}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="state">Estado de propiedad</label>
                                            <select
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="state"
                                                name="state"
                                                value={formik.values.state}
                                                onChange={formik.handleChange}>
                                                <option value="">-- Seleccione --</option>
                                                {statesOfProperty.map(item => (
                                                    <option
                                                        key={item.id}
                                                        value={item.id}
                                                    >
                                                        {item.name}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {formik.touched.state && formik.errors.state ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.state}</p>
                                            </div>
                                        ) : null
                                        }
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idOwner">Propietario</label>
                                            <select
                                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="idOwner"
                                                name="idOwner"
                                                value={formik.values.idOwner}
                                                onChange={formik.handleChange}>
                                                <option value="">-- Seleccione --</option>
                                                {owners.map(item => (
                                                    <option
                                                        key={item.id}
                                                        value={item.id}
                                                    >
                                                        {item.fullName}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {formik.touched.idOwner && formik.errors.idOwner ? (
                                            <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                <p>{formik.errors.idOwner}</p>
                                            </div>
                                        ) : null
                                        }
                                        {idProperty == 0 ? <>
                                            {imagensOfProperty.length > 0 ?
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="enablePhoto">Imagenes cargadas {imagensOfProperty.length}</label>
                                                : null}
                                            <input
                                                type="button"
                                                onClick={() => openModalImage()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Agregar Imagenes"
                                            /> </> : <> </>}
                                        <input
                                            type="submit"
                                            className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 mb-3 text-white uppercase font-bold"
                                            value={idProperty > 0 ? "Editar Propiedad" : "Agregar Propiedad"}
                                        />

                                        {idProperty > 0 ?
                                            <input
                                                type="button"
                                                onClick={() => DeleteProperty()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-2 p-2 mb-3 text-white uppercase font-bold"
                                                value="Eliminar Propiedad"
                                            />
                                            : <> </>}
                                        <input
                                            type="button"
                                            onClick={() => openForm()}
                                            className="bg-gray-900 hover:bg-gray-500 w-full mt-2 p-2 mb-3 text-white uppercase font-bold"
                                            value="Cancelar"
                                        />
                                    </form> : <> </>}
                                    <div className="w-full max-w-3xl">
                                        <h1 className='text-3xl font-bold mb-4'>Propiedades</h1>
                                        <ul className="divide-y-2 divide-gray-200">
                                            <FlatList
                                                list={Propertys}
                                                renderItem={renderObject}
                                                renderWhenEmpty={() => <div>No hay propietarios!</div>}
                                                sortBy={["name", { key: "id", descending: true }]}
                                            />
                                        </ul>
                                        <input
                                            style={{ marginTop: 40 }}
                                            type='button'
                                            className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                            value="Agregar Propietarios"
                                            onClick={() => openForm()}
                                        />
                                    </div>
                                </div>
                            </div >
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Property;
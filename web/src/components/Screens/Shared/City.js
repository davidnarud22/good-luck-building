import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getCitys, addCity, editCity, deleteCity } from '../../../services/Shared/CityService';
import { getCountries } from '../../../services/Shared/CountryService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const City = () => {
    const [showLoader, saveshowLoader] = useState(false);
    const [cities, saveCities] = useState([]);
    const [nameCities, saveNameCities] = useState([]);
    const [countries, saveCountries] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idCity, saveIdCity] = useState(0);
    const navigate = useNavigate();


    const formik = useFormik({
        initialValues: {
            name: '',
            idCountry: '',
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'El Nombre de la ciudad deben tener al menos 3 caracteres')
                .required('El Nombre de la ciudad es obligatorio'),
            idCountry: Yup.string()
                .required('El pais es obligatorio'),
        }),
        onSubmit: ciudad => {
            try {
                saveshowLoader(true);
                if (idCity > 0) {
                    editCity(ciudad, idCity)
                        .then(() => {
                            GetCities();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        })
                    saveIdCity('')
                }
                else {
                    addCity(ciudad)
                        .then(() => {
                            GetCities();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        })
                        .catch((err) => {
                            navigate('/')
                        })
                }
            } catch (error) {
                console.log(error)
            }

        }
    });


    const GetCities = () => {
        saveshowLoader(true);
        getCitys()
            .then((response) => {
                saveCities(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const GetCountries = () => {
        saveshowLoader(true);
        getCountries()
            .then((response) => {
                saveCountries(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }


    const renderObject = (cities, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickCity(cities)}>
                {cities.name}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.name = ''
        formik.initialValues.idCountry = '';
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idCity > 0) {
            saveIdCity(0)
        }

    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickCity = (ciudad) => {
        openModal();
        saveIdCity(ciudad.id)
        formik.initialValues.name = ciudad.name
        formik.initialValues.idCountry = ciudad.idCountry;
    }
    const DeleteCity = () => {
        deleteCity(idCity)
            .then(() => {
                GetCities();
                formik.resetForm();
                closeModal();
            })
            .catch((err) => {
                navigate('/')
            })
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetCities();
        GetCountries();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar ciudad"
                                        appElement={document.getElementById('root')}
                                    >
                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">Nombre ciudad</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="name"
                                                    type="text"
                                                    placeholder="Nombre ciudad"
                                                    value={formik.values.name}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.name && formik.errors.name ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.name}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idCountry">Pais</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idCountry"
                                                    name="idCountry"
                                                    value={formik.values.idCountry}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {countries.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.idCountry && formik.errors.idCountry ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idCountry}</p>
                                                </div>
                                            ) : null
                                            }

                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idCity > 0 ? "Editar Ciudad" : "Agregar Ciudad"}
                                            />
                                            {idCity > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteCity()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar ciudad"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>
                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Ciudades</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={cities}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay ciudades!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Ciudad"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
        </>
    );
}

export default City;
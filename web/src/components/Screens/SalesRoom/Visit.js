import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getVisits, addVisit, editVisit, deleteVisit } from '../../../services/SalesRoom/VisitService';
import { getPropertys } from '../../../services/Shared/PropertyService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import moment from 'moment'
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const Visit = () => {
    const navigate = useNavigate();
    const [Visits, saveVisits] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [idVisit, saveIdVisit] = useState('');
    const [propertys, savePropertys] = useState([]);
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            DateVisit: '',
            IdProperty: '',
            NameOfVisitor: ''
        },
        validationSchema: Yup.object({
            DateVisit: Yup.string()
                .required('La fecha de la visita es obligatoria'),
            IdProperty: Yup.string()
                .required('La propiedad es obligatoria'),
            NameOfVisitor: Yup.string()
                .min(3, 'El nombre del visitante deben tener al menos 3 caracteres')
                .required('El nombre del visitante es obligatorio'),

        }),
        onSubmit: Visit => {
            try {
                saveshowLoader(true);
                if (idVisit > 0) {
                    editVisit(Visit, idVisit)
                        .then(() => {
                            GetVisits();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                    saveIdVisit('')
                }
                else {
                    addVisit(Visit)
                        .then(() => {
                            GetVisits();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetVisits = () => {
        saveshowLoader(true);
        getVisits()
            .then((response) => {
                console.log(response.data);
                saveVisits(response.data);
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (Visits, idx) => {
        console.log(Visits)
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickVisit(Visits)}>
                {Visits.idProperty + " - " + Visits.nameOfVisitor}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.DateVisit = '';
        formik.initialValues.IdProperty = '';
        formik.initialValues.NameOfVisitor = '';
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idVisit > 0) {
            saveIdVisit('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickVisit = (Visit) => {
        openModal();
        saveIdVisit(Visit.id)
        const date = new Date(Visit.dateVisit);
        formik.initialValues.DateVisit = moment(date).format('YYYY-MM-DD');
        formik.initialValues.IdProperty = Visit.idProperty;
        formik.initialValues.NameOfVisitor = Visit.nameOfVisitor;
    }
    const DeleteVisit = () => {
        saveshowLoader(true);
        deleteVisit(idVisit)
            .then(() => {
                GetVisits();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetPropertys = () => {
        saveshowLoader(true);
        getPropertys()
            .then((response) => {
                savePropertys(response.data)
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetVisits();
        GetPropertys();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar visita"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="DateVisit">Fecha de visita</label>
                                                <input
                                                    id="DateVisit"
                                                    name="DateVisit"
                                                    type="date"
                                                    disabled={false}
                                                    value={formik.values.DateVisit}
                                                    onChange={formik.handleChange} />
                                            </div>
                                            {formik.touched.DateVisit && formik.errors.DateVisit ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.DateVisit}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="IdProperty">Propiedad</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="IdProperty"
                                                    name="IdProperty"
                                                    value={formik.values.IdProperty}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {propertys.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>

                                            {formik.touched.IdProperty && formik.errors.IdProperty ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.IdProperty}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="NameOfVisitor">Nombre visitante</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="NameOfVisitor"
                                                    type="text"
                                                    placeholder="Primer Nombre"
                                                    value={formik.values.NameOfVisitor}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.NameOfVisitor && formik.errors.NameOfVisitor ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.NameOfVisitor}</p>
                                                </div>
                                            ) : null
                                            }
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idVisit > 0 ? "Editar visita" : "Agregar visita"}
                                            />
                                            {idVisit > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteVisit()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar visita"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Visitas</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={Visits}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay visitas!</div>}
                                            sortBy={["nameOfVisitor", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar visita"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Visit;
import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getQuotes, addQuote, editQuote, deleteQuote } from '../../../services/SalesRoom/QuoteService';
import { getPropertys } from '../../../services/Shared/PropertyService';
import { getOwners } from '../../../services/Shared/OwnerService';
import { getTypeOfPayments } from '../../../services/Shared/TypeOfPaymentService';
import FlatList from 'flatlist-react';
import Modal from 'react-modal';
import Sidebar from '../../UI/Sidebar'
import { useNavigate } from 'react-router-dom'
import ReactLoader from '../../UI/ReactLoader';

const Quote = () => {
    const navigate = useNavigate();
    const [Quotes, saveQuotes] = useState([]);
    const [typeOfPayments, saveTypeOfPayments] = useState([]);
    const [owners, saveOwners] = useState([]);
    const [propertys, savePropertys] = useState([]);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [quotasIsDisable, setQuotasIsDisable] = useState(true);
    const [idQuote, saveIdQuote] = useState('');
    const [showLoader, saveshowLoader] = useState(false);

    const formik = useFormik({
        initialValues: {
            idProperty: 0,
            tax: 0,
            price: 0,
            idTypeOfPayment: '',
            numbersOfQuotas: '',
            valueOfQuotas: '',
            idDownPayment: '',
            idOwner: ''
        },
        validationSchema: Yup.object({
            idProperty: Yup.string()
                .required('La propiedad es obligatoria'),
            tax: Yup.string()
                .required('El valor del impuesto es obligatorio'),
            price: Yup.string()
                .required('El precio es obligatorio'),
            idTypeOfPayment: Yup.string()
                .required('El tipo de pago es obligatorio'),
        }),
        onSubmit: Quote => {
            try {
                saveshowLoader(true);
                if (idQuote > 0) {
                    editQuote(Quote, idQuote)
                        .then(() => {
                            GetQuotes();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                    saveIdQuote('')
                }
                else {
                    addQuote(Quote)
                        .then(() => {
                            GetQuotes();
                            formik.resetForm();
                            closeModal();
                            saveshowLoader(false);
                        }).catch((err) => {
                            navigate('/')
                        });
                }
            } catch (error) {
                console.log(error)
            }

        }
    });

    const GetQuotes = () => {
        saveshowLoader(true);
        getQuotes()
            .then((response) => {
                saveQuotes(response.data);
                saveshowLoader(false);
            })
            .catch((err) => {
                navigate('/')
            });
    }

    const renderObject = (Quotes, idx) => {
        return (
            <li className="p-3 hover:bg-yellow-500 hover:text-white" key={idx} onClick={() => clickQuote(Quotes)}>
                {"Cotizacion #" + Quotes.id + " - Ide. propiedad : " + Quotes.idProperty + " - $" + Quotes.price}
            </li>
        );
    }

    const openModal = () => {
        setIsOpen(true);
        formik.resetForm();
        formik.initialValues.idProperty = ''
        formik.initialValues.tax = ''
        formik.initialValues.price = ''
        formik.initialValues.idTypeOfPayment = ''
        formik.initialValues.numbersOfQuotas = ''
        formik.initialValues.valueOfQuotas = ''
        formik.initialValues.idDownPayment = ''
        formik.initialValues.idOwner = ''
    }

    const closeModal = () => {
        setIsOpen(false);
        if (idQuote > 0) {
            saveIdQuote('')
        }
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: 400
        },
    };

    const clickQuote = (Quote) => {
        openModal();
        saveIdQuote(Quote.id)
        formik.initialValues.idProperty = Quote.idProperty
        formik.initialValues.tax = Quote.tax
        formik.initialValues.price = Quote.price
        formik.initialValues.idTypeOfPayment = Quote.idTypeOfPayment
        formik.initialValues.numbersOfQuotas = Quote.numbersOfQuotas
        formik.initialValues.valueOfQuotas = Quote.valueOfQuotas
        formik.initialValues.idDownPayment = Quote.idDownPayment
        formik.initialValues.idOwner = Quote.idOwner
    }
    const DeleteQuote = () => {
        saveshowLoader(true);
        deleteQuote(idQuote)
            .then(() => {
                GetQuotes();
                formik.resetForm();
                closeModal();
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const fillPriceProperty = (e) => {
        if (!!e.target.value) {
            formik.values.price = ''
            propertys.forEach(element => {
                if (element.id == e.target.value) {
                    formik.setFieldValue(
                        'price',
                        element.price
                    )
                }
            });
        }
    }

    const GetTypeOfPayments = () => {
        saveshowLoader(true);
        getTypeOfPayments()
            .then((response) => {
                saveTypeOfPayments(response.data)
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetOwners = () => {
        saveshowLoader(true);
        getOwners()
            .then((response) => {
                saveOwners(response.data)
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const GetPropertys = () => {
        saveshowLoader(true);
        getPropertys()
            .then((response) => {
                savePropertys(response.data)
                saveshowLoader(false);
            }).catch((err) => {
                navigate('/')
            });
    }

    const enableQuotas = (e) => {
        if (e.target.value > 0) {
            setQuotasIsDisable(false);
        }
    }

    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
    })

    const calcValueOfQuotas = (e) => {
        if (e.target.value > 0) {
            let ValueOfQuotas = 0;
            let tax = 0;
            let DownPayment = 0;
            let price = 0;
            let totalQuotas = 0;
            if (formik.values.tax > 0) {
                tax = formik.values.tax;
            }
            if (formik.values.idDownPayment > 0) {
                DownPayment = formik.values.idDownPayment;
                price = formik.values.price - DownPayment;
            }
            ValueOfQuotas = (price + tax) / e.target.value;
            console.log(price, DownPayment, e.target.value);
            formik.setFieldValue(
                'valueOfQuotas',
                ValueOfQuotas
            )
        }
    }

    useEffect(() => {
        if (localStorage.role == "User") {
            navigate('/')
        }
        GetQuotes();
        GetTypeOfPayments();
        GetOwners();
        GetPropertys();
    }, [])

    return (
        <>
            {showLoader ? <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)',
            }} ><ReactLoader /></div> :
                <div>
                    <div className="md:flex min-h-screen">
                        <Sidebar />
                        <div className="md:w-2/5 xl:w-4/5 p-6">
                            <div className="flex justify-center mt-10">
                                <div>
                                    <Modal
                                        isOpen={modalIsOpen}
                                        onRequestClose={closeModal}
                                        style={customStyles}
                                        contentLabel="Agregar Cotizacion"
                                        appElement={document.getElementById('root')}
                                    >

                                        <form
                                            onSubmit={formik.handleSubmit}
                                        >
                                            <div style={{ marginLeft: '95%', marginRight: '0%' }}>
                                                <button onClick={closeModal}>X</button>
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idProperty">Propiedad</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idProperty"
                                                    name="idProperty"
                                                    value={formik.values.idProperty}
                                                    onChange={(e) => { formik.handleChange(e); fillPriceProperty(e) }}>
                                                    <option value="">-- Seleccione --</option>
                                                    {propertys.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idProperty && formik.errors.idProperty ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idProperty}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="price">Precio propiedad</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="price"
                                                    type="number"
                                                    placeholder="Precio propiedad"
                                                    value={formik.values.price}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.price && formik.errors.price ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.price}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="tax">Precio Impuesto</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="tax"
                                                    type="number"
                                                    placeholder="Precio Impuesto"
                                                    value={formik.values.tax}
                                                    onChange={(e) => { formik.handleChange(e); enableQuotas(e) }}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            {formik.touched.tax && formik.errors.tax ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.tax}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idDownPayment">Cuota inicial</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idDownPayment"
                                                    type="number"
                                                    placeholder="Cuota inicial"
                                                    value={formik.values.idDownPayment}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idTypeOfPayment">Tipos de pago</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idTypeOfPayment"
                                                    name="idTypeOfPayment"
                                                    value={formik.values.idTypeOfPayment}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {typeOfPayments.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            {formik.touched.idTypeOfPayment && formik.errors.idTypeOfPayment ? (
                                                <div className="bg-red-100  border-l-4 border-red-500 text-red-700 p-4 mb-5" role="alert">
                                                    <p>{formik.errors.idTypeOfPayment}</p>
                                                </div>
                                            ) : null
                                            }
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="numbersOfQuotas">Numero de cuotas</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="numbersOfQuotas"
                                                    type="number"
                                                    placeholder="Numero de cuotas"
                                                    disabled={quotasIsDisable}
                                                    value={formik.values.numbersOfQuotas}
                                                    onChange={(e) => { formik.handleChange(e); calcValueOfQuotas(e) }}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="valueOfQuotas">Valor de cuotas</label>
                                                <input
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="valueOfQuotas"
                                                    type="number"
                                                    disabled={quotasIsDisable}
                                                    placeholder="Valor de cuotas"
                                                    value={formik.values.valueOfQuotas}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur} />
                                            </div>
                                            <div className="mb-4">
                                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="idOwner">Propietario</label>
                                                <select
                                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="idOwner"
                                                    name="idOwner"
                                                    value={formik.values.idOwner}
                                                    onChange={formik.handleChange}>
                                                    <option value="">-- Seleccione --</option>
                                                    {owners.map(item => (
                                                        <option
                                                            key={item.id}
                                                            value={item.id}
                                                        >
                                                            {item.fullName}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                            <input
                                                type="submit"
                                                className="bg-yellow-500 hover:bg-yellow-600 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value={idQuote > 0 ? "Editar Cotizacion" : "Agregar Cotizacion"}
                                            />
                                            {idQuote > 0 ? <input
                                                type="button"
                                                onClick={() => DeleteQuote()}
                                                className="bg-gray-900 hover:bg-gray-500 w-full mt-5 p-2 text-white uppercase font-bold"
                                                value="Eliminar Cotizacion"
                                            /> : <> </>}
                                        </form>
                                    </Modal>
                                </div>

                                <div className="w-full max-w-3xl">
                                    <h1 className='text-3xl font-bold mb-4'>Cotizaciones</h1>
                                    <ul className="divide-y-2 divide-gray-200">
                                        <FlatList
                                            list={Quotes}
                                            renderItem={renderObject}
                                            renderWhenEmpty={() => <div>No hay Cotizaciones!</div>}
                                            sortBy={["name", { key: "id", descending: true }]}
                                        />
                                    </ul>
                                    <input
                                        style={{ marginTop: 40 }}
                                        type='button'
                                        className="bg-gray-800 hover:bg-gray-500 inline-block mb-5 p-2 text-white uppercase font-bold"
                                        value="Agregar Cotizacion"
                                        onClick={() => openModal()}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default Quote;
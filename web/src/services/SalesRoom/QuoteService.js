import axiosInstance from '../../components/UI/AxiosOnstance';

export const getQuotes = async () => {
    try {
        const url = 'https://localhost:44328/Quote/'
        return axiosInstance().get(`${url}GetQuotes`)
    } catch (error) {
        console.log('error service');
    }
};

export const addQuote = async (Quote) => {
    try {
        const url = 'https://localhost:44328/Quote/'
        return axiosInstance().post(`${url}AddQuote`, Quote)
    } catch (error) {
        return error;
    }
};

export const editQuote = async (Quote, idQuote) => {
    try {
        Quote.id = idQuote
        const url = 'https://localhost:44328/Quote/'
        return axiosInstance().put(`${url}EditQuote`, Quote)
    } catch (error) {
        return error;
    }
};

export const deleteQuote = async (id) => {
    try {
        const url = 'https://localhost:44328/Quote/'
        return axiosInstance().delete(`${url}DeleteQuote/${id}`)
    } catch (error) {
        return error;
    }
};

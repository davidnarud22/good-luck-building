import axiosInstance from '../../components/UI/AxiosOnstance';

export const getVisits = async () => {
    try {
        const url = 'https://localhost:44328/Visit/'

      return axiosInstance().get(`${url}GetVisits`)

    } catch (error) {
      return error;
    }
  };

export const addVisit = async (Visit) => {
    try {
        const url = 'https://localhost:44328/Visit/'
        return axiosInstance().post(`${url}AddVisit`, Visit)

    } catch (error) {
        return error;
    }
};

export const editVisit = async (Visit, idVisit) => {
    try {
        Visit.id = idVisit
        const url = 'https://localhost:44328/Visit/'
        return axiosInstance().put(`${url}EditVisit`, Visit)
    } catch (error) {
        return error;
    }
};

export const deleteVisit = async (id) => {
    try {
        const url = 'https://localhost:44328/Visit/'
        return axiosInstance().delete(`${url}DeleteVisit/${id}`)
    } catch (error) {
        return error;
    }
};

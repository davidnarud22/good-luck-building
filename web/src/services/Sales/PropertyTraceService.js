import axiosInstance from '../../components/UI/AxiosOnstance';

export const getPropertyTraces = async () => {
    try {
        const url = 'https://localhost:44328/PropertyTrace/'

      return axiosInstance().get(`${url}GetPropertyTraces`)

    } catch (error) {
      return error;
    }
  };

export const addPropertyTrace = async (PropertyTrace) => {
    try {
        const url = 'https://localhost:44328/PropertyTrace/'
        return axiosInstance().post(`${url}AddPropertyTrace`, PropertyTrace)

    } catch (error) {
        return error;
    }
};

export const editPropertyTrace = async (PropertyTrace, idPropertyTrace) => {
    try {
        PropertyTrace.id = idPropertyTrace
        const url = 'https://localhost:44328/PropertyTrace/'
        return axiosInstance().put(`${url}EditPropertyTrace`, PropertyTrace)
    } catch (error) {
        return error;
    }
};

export const deletePropertyTrace = async (id) => {
    try {
        const url = 'https://localhost:44328/PropertyTrace/'
        return axiosInstance().delete(`${url}DeletePropertyTrace/${id}`)
    } catch (error) {
        return error;
    }
};

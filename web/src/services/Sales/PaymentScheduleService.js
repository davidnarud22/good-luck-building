import axiosInstance from '../../components/UI/AxiosOnstance';


export const getPaymentSchedules = async () => {
    try {
        const url = 'https://localhost:44328/PaymentSchedule/'

      return axiosInstance().get(`${url}GetPaymentSchedules`)

    } catch (error) {
      return error;
    }
  };

export const addPaymentSchedule = async (PaymentSchedule) => {
    try {
        const url = 'https://localhost:44328/PaymentSchedule/'
        return axiosInstance().post(`${url}AddPaymentSchedule`, PaymentSchedule)

    } catch (error) {
        return error;
    }
};

export const editPaymentSchedule = async (PaymentSchedule, idPaymentSchedule) => {
    try {
        PaymentSchedule.id = idPaymentSchedule
        const url = 'https://localhost:44328/PaymentSchedule/'
        return axiosInstance().put(`${url}EditPaymentSchedule`, PaymentSchedule)
    } catch (error) {
        return error;
    }
};

export const deletePaymentSchedule = async (id) => {
    try {
        const url = 'https://localhost:44328/PaymentSchedule/'
        return axiosInstance().delete(`${url}DeletePaymentSchedule/${id}`)
    } catch (error) {
        return error;
    }
};

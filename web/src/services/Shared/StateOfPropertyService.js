import axiosInstance from '../../components/UI/AxiosOnstance';

export const getStateOfPropertys = async () => {
    try {
        const url = 'https://localhost:44328/StateOfProperty/'

      return axiosInstance().get(`${url}GetStateOfPropertys`)

    } catch (error) {
      return error;
    }
  };

export const addStateOfProperty = async (StateOfProperty) => {
    try {
        const url = 'https://localhost:44328/StateOfProperty/'
        return axiosInstance().post(`${url}AddStateOfProperty`, StateOfProperty)

    } catch (error) {
        return error;
    }
};

export const editStateOfProperty = async (StateOfProperty, idStateOfProperty) => {
    try {
        StateOfProperty.id = idStateOfProperty
        const url = 'https://localhost:44328/StateOfProperty/'
        return axiosInstance().put(`${url}EditStateOfProperty`, StateOfProperty)
    } catch (error) {
        return error;
    }
};

export const deleteStateOfProperty = async (id) => {
    try {
        const url = 'https://localhost:44328/StateOfProperty/'
        return axiosInstance().delete(`${url}DeleteStateOfProperty/${id}`)
    } catch (error) {
        return error;
    }
};

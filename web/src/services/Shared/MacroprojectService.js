import axiosInstance from '../../components/UI/AxiosOnstance';


export const getMacroprojects = async () => {
    try {
        const url = 'https://localhost:44328/Macroproject/'

      return axiosInstance().get(`${url}GetMacroprojects`)

    } catch (error) {
      return error;
    }
  };

export const addMacroproject = async (Macroproject) => {
    try {
        const url = 'https://localhost:44328/Macroproject/'
        return axiosInstance().post(`${url}AddMacroproject`, Macroproject)

    } catch (error) {
        return error;
    }
};

export const editMacroproject = async (Macroproject, idMacroproject) => {
    try {
        Macroproject.id = idMacroproject
        const url = 'https://localhost:44328/Macroproject/'
        return axiosInstance().put(`${url}EditMacroproject`, Macroproject)
    } catch (error) {
        return error;
    }
};

export const deleteMacroproject = async (id) => {
    try {
        const url = 'https://localhost:44328/Macroproject/'
        return axiosInstance().delete(`${url}DeleteMacroproject/${id}`)
    } catch (error) {
        return error;
    }
};

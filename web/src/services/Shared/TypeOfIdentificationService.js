import axiosInstance from '../../components/UI/AxiosOnstance';

export const getTypeOfIdentifications = async () => {
    try {
        const url = 'https://localhost:44328/TypeOfIdentification/'

      return axiosInstance().get(`${url}GetTypeOfIdentifications`)

    } catch (error) {
      return error;
    }
  };

export const addTypeOfIdentification = async (TypeOfIdentification) => {
    try {
        const url = 'https://localhost:44328/TypeOfIdentification/'
        return axiosInstance().post(`${url}AddTypeOfIdentification`, TypeOfIdentification)

    } catch (error) {
        return error;
    }
};

export const editTypeOfIdentification = async (TypeOfIdentification, idTypeOfIdentification) => {
    try {
        TypeOfIdentification.id = idTypeOfIdentification
        const url = 'https://localhost:44328/TypeOfIdentification/'
        return axiosInstance().put(`${url}EditTypeOfIdentification`, TypeOfIdentification)
    } catch (error) {
        return error;
    }
};

export const deleteTypeOfIdentification = async (id) => {
    try {
        const url = 'https://localhost:44328/TypeOfIdentification/'
        return axiosInstance().delete(`${url}DeleteTypeOfIdentification/${id}`)
    } catch (error) {
        return error;
    }
};

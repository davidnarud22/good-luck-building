import axiosInstance from '../../components/UI/AxiosOnstance';


export const getPropertys = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}GetPropertys`)

    } catch (error) {
        return error;
    }
};

export const GetPropertyForViewsForSearch = async (search) => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().post(`${url}GetPropertyForViewsForSearch/${search}`)

    } catch (error) {
        return error;
    }
};

export const GetPropertyForViewsForNaturalPerson = async (filters) => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().post(`${url}GetPropertyForViewsForNaturalPerson`, filters)

    } catch (error) {
        return error;
    }
};

export const GetPropertyForViewsForConstructor = async (filters) => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().post(`${url}GetPropertyForViewsForConstructor`, filters)

    } catch (error) {
        return error;
    }
};

export const GetNumberOfBathrooms = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}getNumberOfBathrooms`)

    } catch (error) {
        return error;
    }
};

export const GetNumberOfRooms = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}getNumberOfRooms`)

    } catch (error) {
        return error;
    }
};

export const GetNumberOfFloors = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}getNumberOfFloors`)

    } catch (error) {
        return error;
    }
};

export const GetPrices = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}getPrices`)

    } catch (error) {
        return error;
    }
};

export const GetAreas = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}getAreas`)

    } catch (error) {
        return error;
    }
};

export const GetYears = async () => {
    try {
        const url = 'https://localhost:44328/Property/'

        return axiosInstance().get(`${url}getYears`)

    } catch (error) {
        return error;
    }
};

export const addProperty = async (Property) => {
    try {
        const url = 'https://localhost:44328/Property/'
        return axiosInstance().post(`${url}AddProperty`, Property)

    } catch (error) {
        return error;
    }
};

export const editProperty = async (Property, idProperty) => {
    try {
        Property.id = idProperty
        const url = 'https://localhost:44328/Property/'
        return axiosInstance().put(`${url}EditProperty`, Property)
    } catch (error) {
        return error;
    }
};

export const deleteProperty = async (id) => {
    try {
        const url = 'https://localhost:44328/Property/'
        return axiosInstance().delete(`${url}DeleteProperty/${id}`)
    } catch (error) {
        return error;
    }
};

import axiosInstance from '../../components/UI/AxiosOnstance';


export const getDepartments = async () => {
    try {
        const url = 'https://localhost:44328/Department/'
      return axiosInstance().get(`${url}GetDepartments`)

    } catch (error) {
      return error
    }
  };

  export const getDepartmentsForCity = async (id) => {
    try {
        const url = 'https://localhost:44328/Department/'
      return axiosInstance().get(`${url}GetDepartmentsForCity/${id}`)

    } catch (error) {
      return error
    }
  };

export const addDepartment = async (Department) => {
    try {
        const url = 'https://localhost:44328/Department/'
        return axiosInstance().post(`${url}AddDepartment`, Department)

    } catch (error) {
        return error
    }
};

export const editDepartment = async (Department, idDepartment) => {
    try {
        Department.id = idDepartment
        const url = 'https://localhost:44328/Department/'
        return axiosInstance().put(`${url}EditDepartment`, Department)
    } catch (error) {
        return error
    }
};

export const deleteDepartment = async (id) => {
    try {
        const url = 'https://localhost:44328/Department/'
        return axiosInstance().delete(`${url}DeleteDepartment/${id}`)
    } catch (error) {
        return error
    }
};

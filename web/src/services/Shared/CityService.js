import axiosInstance from '../../components/UI/AxiosOnstance';

export const getCitys = async () => {
    try {
        const url = 'https://localhost:44328/City/'
        return axiosInstance().get(`${url}GetCities`)
    } catch (error) {
        return error
    }
};

export const getCitiesForCountry = async (id) => {
    try {
        const url = 'https://localhost:44328/City/'
        return axiosInstance().get(`${url}GetCitiesForCountry/${id}`)

    } catch (error) {
        return error
    }
};

export const addCity = async (city) => {
    try {
        const url = 'https://localhost:44328/City/'
        return axiosInstance().post(`${url}AddCity`, city)

    } catch (error) {
        return error
    }
};

export const editCity = async (city, idCity) => {
    try {
        city.id = idCity
        const url = 'https://localhost:44328/City/'
        return axiosInstance().put(`${url}EditCity`, city)
    } catch (error) {
        return error
    }
};

export const deleteCity = async (id) => {
    try {
        const url = 'https://localhost:44328/City/'
        return axiosInstance().delete(`${url}DeleteCity/${id}`)
    } catch (error) {
        return error
    }
};
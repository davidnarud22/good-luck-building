import axiosInstance from '../../components/UI/AxiosOnstance';


export const getOwners = async () => {
    try {
        const url = 'https://localhost:44328/Owner/'
        return axiosInstance().get(`${url}GetOwners`)

    } catch (error) {
        return error;
    }
};

export const addOwner = async (Owner) => {
    try {
        const url = 'https://localhost:44328/Owner/'
        return axiosInstance().post(`${url}AddOwner`, Owner)

    } catch (error) {
        return error;
    }
};

export const editOwner = async (Owner, idOwner) => {
    try {
        Owner.id = idOwner
        const url = 'https://localhost:44328/Owner/'
        return axiosInstance().put(`${url}EditOwner`, Owner)
    } catch (error) {
        return error;
    }
};

export const deleteOwner = async (id) => {
    try {
        const url = 'https://localhost:44328/Owner/'
        return axiosInstance().delete(`${url}DeleteOwner/${id}`)
    } catch (error) {
        return error;
    }
};

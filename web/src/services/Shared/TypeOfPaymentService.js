import axiosInstance from '../../components/UI/AxiosOnstance';

export const getTypeOfPayments = async () => {
    try {
        const url = 'https://localhost:44328/TypeOfPayment/'

      return axiosInstance().get(`${url}GetTypeOfPayments`)

    } catch (error) {
      return error;
    }
  };

export const addTypeOfPayment = async (TypeOfPayment) => {
    try {
        const url = 'https://localhost:44328/TypeOfPayment/'
        return axiosInstance().post(`${url}AddTypeOfPayment`, TypeOfPayment)

    } catch (error) {
        return error;
    }
};

export const editTypeOfPayment = async (TypeOfPayment, idTypeOfPayment) => {
    try {
        TypeOfPayment.id = idTypeOfPayment
        const url = 'https://localhost:44328/TypeOfPayment/'
        return axiosInstance().put(`${url}EditTypeOfPayment`, TypeOfPayment)
    } catch (error) {
        return error;
    }
};

export const deleteTypeOfPayment = async (id) => {
    try {
        const url = 'https://localhost:44328/TypeOfPayment/'
        return axiosInstance().delete(`${url}DeleteTypeOfPayment/${id}`)
    } catch (error) {
        return error;
    }
};

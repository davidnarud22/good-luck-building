import axiosInstance from '../../components/UI/AxiosOnstance';


export const getPropertyImages = async () => {
    try {
        const url = 'https://localhost:44328/PropertyImage/'

      return axiosInstance().get(`${url}GetPropertyImages`)

    } catch (error) {
      return error;
    }
  };

  export const getLastOrderOfImagenForProyect = async (id) => {
    try {
        const url = 'https://localhost:44328/PropertyImage/'
        return axiosInstance().get(`${url}GetLastOrderOfImagenForProyect/${id}`)
    } catch (error) {
        return error;
    }
};

export const getPropertyImagesForProperty = async (id) => {
    try {
        const url = 'https://localhost:44328/PropertyImage/'
        return axiosInstance().get(`${url}GetPropertyImagesForProperty/${id}`)
    } catch (error) {
        return error;
    }
};

export const addPropertyImage = async (PropertyImage) => {
    try {
        const url = 'https://localhost:44328/PropertyImage/'
        return axiosInstance().post(`${url}AddPropertyImage`, PropertyImage)

    } catch (error) {
        return error;
    }
};

export const addPropertyImages = async (PropertyImages) => {
    try {
        const url = 'https://localhost:44328/PropertyImage/'
        return axiosInstance().post(`${url}AddPropertyImages`, PropertyImages)
    } catch (error) {
        return error;
    }
};

export const editPropertyImage = async (PropertyImage, idPropertyImage) => {
    try {
        PropertyImage.id = idPropertyImage
        const url = 'https://localhost:44328/PropertyImage/'
        return axiosInstance().put(`${url}EditPropertyImage`, PropertyImage)
    } catch (error) {
        return error;
    }
};

export const deletePropertyImage = async (id) => {
    try {
        const url = 'https://localhost:44328/PropertyImage/'
        return axiosInstance().delete(`${url}DeletePropertyImage/${id}`)
    } catch (error) {
        return error;
    }
};

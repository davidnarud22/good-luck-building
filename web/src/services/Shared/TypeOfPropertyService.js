import axiosInstance from '../../components/UI/AxiosOnstance';

export const getTypeOfPropertys = async () => {
    try {
        const url = 'https://localhost:44328/TypeOfProperty/'

      return axiosInstance().get(`${url}GetTypeOfPropertys`)

    } catch (error) {
      return error;
    }
  };

export const addTypeOfProperty = async (TypeOfProperty) => {
    try {
        const url = 'https://localhost:44328/TypeOfProperty/'
        return axiosInstance().post(`${url}AddTypeOfProperty`, TypeOfProperty)

    } catch (error) {
        return error;
    }
};

export const editTypeOfProperty = async (TypeOfProperty, idTypeOfProperty) => {
    try {
        TypeOfProperty.id = idTypeOfProperty
        const url = 'https://localhost:44328/TypeOfProperty/'
        return axiosInstance().put(`${url}EditTypeOfProperty`, TypeOfProperty)
    } catch (error) {
        return error;
    }
};

export const deleteTypeOfProperty = async (id) => {
    try {
        const url = 'https://localhost:44328/TypeOfProperty/'
        return axiosInstance().delete(`${url}DeleteTypeOfProperty/${id}`)
    } catch (error) {
        return error;
    }
};

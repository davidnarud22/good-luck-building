import axiosInstance from '../../components/UI/AxiosOnstance';

export const getCountries = async () => {
    try {
        const url = 'https://localhost:44328/Country/'

      return axiosInstance().get(`${url}GetCountries`)

    } catch (error) {
      return error
    }
  };

export const addCountry = async (Country) => {
    try {
        const url = 'https://localhost:44328/Country/'
        return axiosInstance().post(`${url}AddCountry`, Country)

    } catch (error) {
        return error
    }
};

export const editCountry = async (Country, idCountry) => {
    try {
        Country.id = idCountry
        const url = 'https://localhost:44328/Country/'
        return axiosInstance().put(`${url}EditCountry`, Country)
    } catch (error) {
        return error
    }
};

export const deleteCountry = async (id) => {
    try {
        const url = 'https://localhost:44328/Country/'
        return axiosInstance().delete(`${url}DeleteCountry/${id}`)
    } catch (error) {
        return error
    }
};

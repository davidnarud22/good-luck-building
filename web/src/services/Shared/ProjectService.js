import axiosInstance from '../../components/UI/AxiosOnstance';


export const getProjects = async () => {
    try {
        const url = 'https://localhost:44328/Project/'
        return axiosInstance().get(`${url}GetProjects`)
    } catch (error) {
        return error;
    }
};

export const getProject = async (id) => {
    try {
        const url = 'https://localhost:44328/Project/'
        return axiosInstance().get(`${url}GetProject/${id}`)
    } catch (error) {
        return error;
    }
};

export const getProjectsForMacroProjects = async (id) => {
    try {
        const url = 'https://localhost:44328/Project/'
        return axiosInstance().get(`${url}GetProjectsForMacroProjects/${id}`)
    } catch (error) {
        return error;
    }
};

export const addProject = async (Project) => {
    try {
        const url = 'https://localhost:44328/Project/'
        return axiosInstance().post(`${url}AddProject`, Project)
    } catch (error) {
        return error;
    }
};

export const editProject = async (Project, idProject) => {
    try {
        Project.id = idProject
        const url = 'https://localhost:44328/Project/'
        return axiosInstance().put(`${url}EditProject`, Project)
    } catch (error) {
        return error;
    }
};

export const deleteProject = async (id) => {
    try {
        const url = 'https://localhost:44328/Project/'
        return axiosInstance().delete(`${url}DeleteProject/${id}`)
    } catch (error) {
        return error;
    }
};

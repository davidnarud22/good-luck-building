import axiosInstance from '../../components/UI/AxiosOnstance';

export const getUsers = async () => {
    try {
        const url = 'https://localhost:44328/User/'
        return axiosInstance().get(`${url}GetUsers`)
    } catch (error) {
        return error
    }
};

export const addUser = async (User) => {
    try {
        const url = 'https://localhost:44328/User/'
        return axiosInstance().post(`${url}AddUser`, User)

    } catch (error) {
        return error
    }
};

export const editUser = async (User, idUser) => {
    try {
        User.idUser = idUser
        const url = 'https://localhost:44328/User/'
        return axiosInstance().put(`${url}EditUser`, User)
    } catch (error) {
        return error
    }
};

export const deleteUser = async (id) => {
    try {
        const url = 'https://localhost:44328/User/'
        return axiosInstance().delete(`${url}DeleteUser/${id}`)
    } catch (error) {
        return error
    }
};
import axios from 'axios';

export const Authentication = async (LoginUser) => {
    try {
        const url = 'https://localhost:44328/Authentication/'
        return axios.post(`${url}Login`, LoginUser)

    } catch (error) {
        return error;
    }
};

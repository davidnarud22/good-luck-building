import axiosInstance from '../../components/UI/AxiosOnstance';

export const getUsersType = async () => {
    try {
        const url = 'https://localhost:44328/UserType/'
        return axiosInstance().get(`${url}GetUsersType`)
    } catch (error) {
        return error
    }
};

export const addUserType = async (UserType) => {
    try {
        const url = 'https://localhost:44328/UserType/'
        return axiosInstance().post(`${url}AddUserType`, UserType)

    } catch (error) {
        return error
    }
};

export const editUserType = async (UserType, idUserType) => {
    try {
        UserType.id = idUserType
        const url = 'https://localhost:44328/UserType/'
        return axiosInstance().put(`${url}EditUserType`, UserType)
    } catch (error) {
        return error
    }
};

export const deleteUserType = async (id) => {
    try {
        const url = 'https://localhost:44328/UserType/'
        return axiosInstance().delete(`${url}DeleteUserType/${id}`)
    } catch (error) {
        return error
    }
};
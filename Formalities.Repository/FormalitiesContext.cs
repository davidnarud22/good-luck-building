﻿using Formalities.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Formalities.Repository
{
    public class FormalitiesContext : DbContext
    {
        public FormalitiesContext(DbContextOptions<FormalitiesContext> options) : base(options)
        {

        }

        public DbSet<Procedure> procedures { get; set; }
    }
}

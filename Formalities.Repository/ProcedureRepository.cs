﻿using Formalities.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Formalities;
using System.Collections.Generic;
using System.Linq;

namespace Formalities.Repository
{
    public class ProcedureRepository : IProcedureRepository
    {
        private FormalitiesContext _formalitiesContext;

        public ProcedureRepository(FormalitiesContext formalitiesContext)
        {
            _formalitiesContext = formalitiesContext;
        }

        public Procedure GetProcedure(int id)
        {
            return _formalitiesContext.procedures.Find(id);
        }

        public List<Procedure> GetProcedures()
        {
            return _formalitiesContext.procedures.ToList();
        }

        public Procedure AddProcedure(Procedure procedure)
        {
            _formalitiesContext.procedures.Add(procedure);
            _formalitiesContext.SaveChanges();
            return procedure;
        }

        public Procedure EditProcedure(Procedure procedure)
        {
            _formalitiesContext.Entry(procedure).State = EntityState.Modified;
            _formalitiesContext.SaveChanges();
            return procedure;
        }

        public void DeleteProcedure(Procedure procedure)
        {
            _formalitiesContext.procedures.Remove(procedure);
            _formalitiesContext.SaveChanges();
        }
    }
}

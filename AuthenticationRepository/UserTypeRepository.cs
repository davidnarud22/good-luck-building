﻿using Authentication.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuthenticationRepository
{
    public class UserTypeRepository : IUserTypeRepository
    {
        private AuthenticationContext _authenticationContext;

        public UserTypeRepository(AuthenticationContext authenticationContext)
        {
            _authenticationContext = authenticationContext;
        }

        public UserType GetUserType(int id)
        {
            return _authenticationContext.userTypes.Find(id);
        }

        public List<UserType> GetUsersType()
        {
            return _authenticationContext.userTypes.ToList();
        }

        public UserType AddUserType(UserType userType)
        {
            _authenticationContext.userTypes.Add(userType);
            _authenticationContext.SaveChanges();
            return userType;
        }

        public UserType EditUserType(UserType userType)
        {
            _authenticationContext.Entry(userType).State = EntityState.Modified;
            _authenticationContext.SaveChanges();
            return userType;
        }

        public void DeleteUserType(int id)
        {
            var CityToDelete = _authenticationContext.userTypes.Find(id);
            _authenticationContext.userTypes.Remove(CityToDelete);
            _authenticationContext.SaveChanges();
        }
    }
}

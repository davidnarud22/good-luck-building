﻿using Authentication.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthenticationRepository
{
    public class AuthenticationContext : DbContext
    {
        public AuthenticationContext(DbContextOptions<AuthenticationContext> options) : base(options)
        {

        }

        public DbSet<User> users { get; set; }
        public DbSet<UserType> userTypes { get; set; }
    }
}

﻿using APIPeopleRisk.Utils;
using Authentication.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticationRepository
{
    public class UserRepository : IUserRepository
    {
        private AuthenticationContext _authenticationContext;

        public UserRepository(AuthenticationContext authenticationContext)
        {
            _authenticationContext = authenticationContext;
        }

        public User GetUser(int id)
        {
            return _authenticationContext.users.Find(id);
        }

        public List<User> GetUsers()
        {
            return _authenticationContext.users.ToList();
        }

        public User AddUser(User user)
        {
            user.Password = BCryptHelper.HashText(user.Password);
            _authenticationContext.users.Add(user);
            _authenticationContext.SaveChanges();
            return user;
        }

        public User EditUser(User user)
        {
            user.Password = BCryptHelper.HashText(user.Password);
            _authenticationContext.Entry(user).State = EntityState.Modified;
            _authenticationContext.SaveChanges();
            return user;
        }

        public void DeleteUser(int id)
        {
            var CityToDelete = _authenticationContext.users.Find(id);
            _authenticationContext.users.Remove(CityToDelete);
            _authenticationContext.SaveChanges();
        }

    }
}

﻿using APIPeopleRisk.Utils;
using Applications.Interfaces.Authentication;
using Authentication.Models;
using Authentication.Models.Models;
using Repository.Interfaces.Authentication;

namespace Applications.Authentication
{
    public class Login : ILogin
    {
        private IUserApplication _userApplication;
        private IUserTypeRepository _userTypeRepository;
        private IJsonWebTokenService _jsonWebTokenService;

        public Login(IUserApplication userApplication, IUserTypeRepository userTypeRepository, IJsonWebTokenService jsonWebTokenService)
        {
            _userApplication = userApplication;
            _userTypeRepository = userTypeRepository;
            _jsonWebTokenService = jsonWebTokenService;
        }

        public TokenAndRole Authentication(LoginUser loginUser)
        {
            User ExistingUser = _userApplication.getUserForEmail(loginUser.email);

            if (ExistingUser != null)
            {
                if (BCryptHelper.VerifyHash(loginUser.password, ExistingUser.Password))
                {
                    UserType userType = _userTypeRepository.GetUserType(ExistingUser.idUserType);

                    TokenAndRole tokenAndRole = new TokenAndRole();

                    if (userType != null)
                    {
                        tokenAndRole.Role = userType.Name;
                    }

                    tokenAndRole.Token = _jsonWebTokenService.CreateToken(ExistingUser, userType.Name);

                    return tokenAndRole;
                }
            }

            return null;
        }
    }
}

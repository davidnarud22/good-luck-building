﻿using Applications.Interfaces.Authentication;
using Authentication.Models;
using Repository.Interfaces.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Authentication
{
    public class UserApplication : IUserApplication
    {
        private IUserRepository _userRepository;

        public UserApplication(IUserRepository userRepository)
        {
            _userRepository = userRepository;

        }
            
        public User getUserForEmail(string email)
        {
            User userForEmail = _userRepository.GetUsers().Where(user => user.Email == email).FirstOrDefault();

            return userForEmail;
        }
    }
}

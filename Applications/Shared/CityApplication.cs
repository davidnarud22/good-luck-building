﻿using Applications.Interfaces.Shared;
using Shared.Models;
using Shared.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Applications.Shared
{
    public class CityApplication : ICityApplication
    {
        private SharedContext _sharedContext;

        public CityApplication(SharedContext sharedContext)
        {
            _sharedContext = sharedContext;
        }
        public List<City> getCitiesForCountry(int id)
        {
           return  _sharedContext.Cities.Where(city => city.IdCountry == id).ToList();
        }
    }
}

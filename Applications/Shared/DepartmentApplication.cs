﻿using Applications.Interfaces.Shared;
using Shared.Models;
using Shared.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Shared
{
    public class DepartmentApplication : IDepartmentApplication
    {
        private SharedContext _sharedContext;

        public DepartmentApplication(SharedContext sharedContext)
        {
            _sharedContext = sharedContext;
        }

        public List<Department> GetDepartmentsForCity(int id)
        {
            return _sharedContext.departments.Where(department => department.IdCity == id).ToList();
        }
    }
}

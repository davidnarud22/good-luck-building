﻿using Applications.Interfaces.Shared;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Shared;
using Shared.Models;
using Shared.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Applications.Shared
{
    public class PropertyApplication : IPropertyApplication
    {
        private SharedContext _sharedContext;
        private IPropertyRepository _propertyRepository;
        private IDepartmentRepository _departmentRepository;
        private ICityRepository _cityRepository;
        private ICountryRepository _countryRepository;
        private ITypeOfPropertyRepository _typeOfPropertyRepository;
        private IStateOfPropertyRepository _stateOfPropertyRepository;
        private IOwnerRepository _ownerRepository;

        public PropertyApplication(SharedContext sharedContext,
         IPropertyRepository propertyRepository,
         IDepartmentRepository departmentRepository,
         ICityRepository cityRepository,
         ICountryRepository countryRepository,
         ITypeOfPropertyRepository typeOfPropertyRepository,
         IStateOfPropertyRepository stateOfPropertyRepository,
         IOwnerRepository ownerRepository)
        {
            _sharedContext = sharedContext;
            _propertyRepository = propertyRepository;
            _departmentRepository = departmentRepository;
            _cityRepository = cityRepository;
            _countryRepository = countryRepository;
            _typeOfPropertyRepository = typeOfPropertyRepository;
            _stateOfPropertyRepository = stateOfPropertyRepository;
            _ownerRepository = ownerRepository;
        }

        public List<int> getNumberOfBathrooms()
        {
            List<Property> properties = _propertyRepository.GetPropertys();

            List<int> orderedNames = properties
                .GroupBy(property => property.NumberOfBathrooms)
                .Select(property => property.Key)
                .ToList();

            return orderedNames;
        }

        public List<int> getNumberOfRooms()
        {
            List<Property> properties = _propertyRepository.GetPropertys();

            List<int> orderedNames = properties
                .GroupBy(property => property.NumberOfRooms)
                .Select(property => property.Key)
                .ToList();

            return orderedNames;
        }

        public List<int> getNumberOfFloors()
        {
            List<Property> properties = _propertyRepository.GetPropertys();

            List<int> orderedNames = properties
                .GroupBy(property => property.NumberOfFloors)
                .Select(property => property.Key)
                .ToList();

            return orderedNames;
        }

        public List<int> getPrices()
        {
            List<Property> properties = _propertyRepository.GetPropertys();

            List<int> orderedNames = properties
                .GroupBy(property => property.Price)
                .Select(property => property.Key)
                .ToList();

            return orderedNames;
        }

        public List<int> getAreas()
        {
            List<Property> properties = _propertyRepository.GetPropertys();

            List<int> orderedNames = properties
                .GroupBy(property => property.Area)
                .Select(property => property.Key)
                .ToList();

            return orderedNames;
        }

        public List<int> getYears()
        {
            List<Property> properties = _propertyRepository.GetPropertys();

            List<int> orderedNames = properties
                .GroupBy(property => property.Year)
                .Select(property => property.Key)
                .ToList();

            return orderedNames;
        }

        public List<PropertyForView> GetPropertyForViewsForConstructor(FiltersProperty filter)
        {
            var param = new SqlParameter[] {
                        new SqlParameter() {
                            ParameterName = "@NumberOfBathrooms",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.numberOfBathrooms.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@MAJORORMINOROFBATHROOMS",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.MAJORORMINOROFBATHROOMS.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@NumberOfFloors",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.numberOfFloors.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@MAJORORMINOROFFLOORS",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.MAJORORMINOROFFLOORS.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@NumberOfRooms",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.numberOfRooms.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@MAJORORMINOROFROOMS",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.MAJORORMINOROFROOMS.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@Area",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.area.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@MAJORORMINOROAREA",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.MAJORORMINOROAREA.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@State",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.state.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@Year",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.year.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@MAJORORMINOROYEAR",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.MAJORORMINOROYEAR.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@Price",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.price.ToString()
                        },
                         new SqlParameter() {
                            ParameterName = "@MAJORORMINOROPRICE",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.MAJORORMINOROPRICE.ToString()
                        },
                        new SqlParameter() {
                            ParameterName = "@IdTypeOfProperty",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = filter.idTypeOfProperty.ToString()
                        }};

            List<PropertyForView> newObject = new List<PropertyForView>();

            using (var cnn = _sharedContext.Database.GetDbConnection())
            {
                var cmm = cnn.CreateCommand();
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.CommandText = "[dbo].[SP_FILTERS_PROPERTIES]";
                cmm.Parameters.AddRange(param);
                cmm.Connection = cnn;
                cnn.Open();
                var reader = cmm.ExecuteReader();

                PropertyForView PropertyForView = null;

                while (reader.Read())
                {
                    PropertyForView = new PropertyForView();
                    PropertyForView.id = int.Parse(reader["Id"].ToString());
                    PropertyForView.name = reader["Name"].ToString();
                    PropertyForView.address = reader["Address"].ToString();
                    PropertyForView.idProject = int.Parse(reader["IdProject"].ToString());
                    PropertyForView.nameProject = reader["NAMEPROJECT"].ToString();
                    PropertyForView.department = reader["DEPARTMENT"].ToString();
                    PropertyForView.city = reader["City"].ToString();
                    PropertyForView.country = reader["COUNTRY"].ToString();
                    PropertyForView.idTypeOfProperty = int.Parse(reader["IdTypeOfProperty"].ToString());
                    PropertyForView.typeProperty = reader["TYPEPROPERTY"].ToString();
                    PropertyForView.price = int.Parse(reader["Price"].ToString());
                    PropertyForView.area = int.Parse(reader["Area"].ToString());
                    PropertyForView.numberOfFloors = int.Parse(reader["NumberOfFloors"].ToString());
                    PropertyForView.numberOfRooms = int.Parse(reader["NumberOfRooms"].ToString());
                    PropertyForView.numberOfBathrooms = int.Parse(reader["NumberOfBathrooms"].ToString());
                    PropertyForView.codeInternal = int.Parse(reader["CodeInternal"].ToString());
                    PropertyForView.year = int.Parse(reader["Year"].ToString());
                    PropertyForView.state = int.Parse(reader["State"].ToString());
                    PropertyForView.stateProperty = reader["STATEPROPERTY"].ToString();
                    PropertyForView.idOwner = int.Parse(reader["IdOwner"].ToString());
                    PropertyForView.owner = reader["OWNER"].ToString();
                    PropertyForView.neighborhood = reader["Neighborhood"].ToString();
                    newObject.Add(PropertyForView);
                }
                cnn.Close();
            }

            return newObject;
        }

        public List<PropertyForView> GetPropertyForViewsForNaturalPerson(FiltersProperty filter)
        {
            List<Property> listProperty = _propertyRepository.GetPropertys().Where(property => property.IdProject == 0).ToList();
            List<City> cities = _cityRepository.GetCities();
            List<Department> departments = _departmentRepository.GetDepartments();
            List<Country> countries = _countryRepository.GetCountries();
            List<TypeOfProperty> typesOfPropertys = _typeOfPropertyRepository.GetTypeOfPropertys();
            List<StateOfProperty> stateOfProperties = _stateOfPropertyRepository.GetStateOfPropertys();
            List<Owner> owners = _ownerRepository.GetOwners();

            var propertysWhitDepartments = listProperty.Join(departments,
                   property => property.IdDepartment,
                   department => department.Id,
                   (infoPropertyWhitDepartment, infoDepartment) => new
                   {
                       infoProperty = infoPropertyWhitDepartment,
                       idDepartment = infoDepartment.Id,
                       department = infoDepartment.Name,
                       idCity = infoDepartment.IdCity
                   }).ToList();

            var propertysWhitDepartmentsAndCities = propertysWhitDepartments.Join(cities,
                  property => property.idCity,
                  city => city.Id,
                  (infoPropertyandCities, infoCity) => new
                  {
                      infoProperty = infoPropertyandCities.infoProperty,
                      idDepartment = infoPropertyandCities.infoProperty.IdDepartment,
                      department = infoPropertyandCities.department,
                      idCity = infoPropertyandCities.idCity,
                      city = infoCity.Name,
                      idCountry = infoCity.IdCountry
                  }).ToList();

            var propertysWhitDepartmentsAndCitiesAndCountry = propertysWhitDepartmentsAndCities.Join(countries,
                  property => property.idCountry,
                  country => country.Id,
                  (infoPropertyandCountry, infoCountry) => new
                  {
                      infoProperty = infoPropertyandCountry.infoProperty,
                      idDepartment = infoPropertyandCountry.idDepartment,
                      department = infoPropertyandCountry.department,
                      idCity = infoPropertyandCountry.idCity,
                      city = infoPropertyandCountry.city,
                      idCountry = infoPropertyandCountry.idCountry,
                      country = infoCountry.Name,
                  }).ToList();

            var propertysWhitDepartmentsAndCitiesAndCountryandTypeOfPropertys = propertysWhitDepartmentsAndCitiesAndCountry.Join(typesOfPropertys,
                   property => property.infoProperty.IdTypeOfProperty,
                   typeOfProperty => typeOfProperty.Id,
                   (infoPropertyandTypeOfProperty, infoTypeOfProperty) => new
                   {
                       infoProperty = infoPropertyandTypeOfProperty.infoProperty,
                       idDepartment = infoPropertyandTypeOfProperty.idDepartment,
                       department = infoPropertyandTypeOfProperty.department,
                       idCity = infoPropertyandTypeOfProperty.idCity,
                       city = infoPropertyandTypeOfProperty.city,
                       idCountry = infoPropertyandTypeOfProperty.idCountry,
                       country = infoPropertyandTypeOfProperty.country,
                       typeProperty = infoTypeOfProperty.Name
                   }).ToList();

            var propertysWhitDepartmentsAndCitiesAndCountryandTypeOfPropertysandStateOfProperty = propertysWhitDepartmentsAndCitiesAndCountryandTypeOfPropertys.Join(stateOfProperties,
                   property => property.infoProperty.State,
                   stateOfProperty => stateOfProperty.Id,
                   (infoPropertyandStateOfProperty, infoStateOfProperty) => new
                   {
                       infoProperty = infoPropertyandStateOfProperty.infoProperty,
                       idDepartment = infoPropertyandStateOfProperty.idDepartment,
                       department = infoPropertyandStateOfProperty.department,
                       idCity = infoPropertyandStateOfProperty.idCity,
                       city = infoPropertyandStateOfProperty.city,
                       idCountry = infoPropertyandStateOfProperty.idCountry,
                       country = infoPropertyandStateOfProperty.country,
                       typeProperty = infoPropertyandStateOfProperty.typeProperty,
                       stateproperty = infoStateOfProperty.Name
                   }).ToList();

            List<PropertyForView> propertyForViews = propertysWhitDepartmentsAndCitiesAndCountryandTypeOfPropertysandStateOfProperty.Join(owners,
                   property => property.infoProperty.IdOwner,
                   owner => owner.Id,
                     (infoPropertyandOwner, infoOwner) => new PropertyForView
                     {
                         id = infoPropertyandOwner.infoProperty.Id,
                         name = infoPropertyandOwner.infoProperty.Name,
                         address = infoPropertyandOwner.infoProperty.Address,
                         department = infoPropertyandOwner.department,
                         city = infoPropertyandOwner.city,
                         country = infoPropertyandOwner.country,
                         idTypeOfProperty = infoPropertyandOwner.infoProperty.IdTypeOfProperty,
                         typeProperty = infoPropertyandOwner.typeProperty,
                         price = infoPropertyandOwner.infoProperty.Price,
                         area = infoPropertyandOwner.infoProperty.Area,
                         numberOfFloors = infoPropertyandOwner.infoProperty.NumberOfFloors,
                         numberOfRooms = infoPropertyandOwner.infoProperty.NumberOfRooms,
                         numberOfBathrooms = infoPropertyandOwner.infoProperty.NumberOfBathrooms,
                         codeInternal = infoPropertyandOwner.infoProperty.CodeInternal,
                         year = infoPropertyandOwner.infoProperty.Year,
                         state = infoPropertyandOwner.infoProperty.State,
                         stateProperty = infoPropertyandOwner.stateproperty,
                         idOwner = infoPropertyandOwner.infoProperty.IdOwner,
                         owner = infoOwner.FullName,
                         neighborhood = infoPropertyandOwner.infoProperty.Neighborhood
                     }).ToList();


            List<PropertyForView> propertyForViewsFilter = new List<PropertyForView>();


            if (filter.numberOfBathrooms > 0)
            {
                if (filter.MAJORORMINOROFBATHROOMS == 0)
                {
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.numberOfBathrooms <= filter.numberOfBathrooms).ToList();
                }
                else if (filter.MAJORORMINOROFBATHROOMS == 1)
                {
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.numberOfBathrooms >= filter.numberOfBathrooms).ToList();
                }
            }

            if (filter.numberOfFloors > 0)
            {
                if (filter.MAJORORMINOROFFLOORS == 0)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }

                    propertyForViewsFilter = propertyForViews.Where(Property => Property.numberOfFloors <= filter.numberOfFloors).ToList();
                }
                else if (filter.MAJORORMINOROFFLOORS == 1)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.numberOfFloors >= filter.numberOfFloors).ToList();
                }
            }

            if (filter.numberOfRooms > 0)
            {
                if (filter.MAJORORMINOROFROOMS == 0)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.numberOfRooms <= filter.numberOfRooms).ToList();
                }
                else if (filter.MAJORORMINOROFROOMS == 1)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.numberOfRooms >= filter.numberOfRooms).ToList();
                }
            }

            if (filter.area > 0)
            {
                if (filter.MAJORORMINOROAREA == 0)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.area <= filter.area).ToList();
                }
                else if (filter.MAJORORMINOROAREA == 1)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.area >= filter.area).ToList();
                }
            }

            if (filter.state > 0)
            {
                if (propertyForViewsFilter.Count > 0)
                {
                    propertyForViews = propertyForViewsFilter;
                }
                propertyForViewsFilter = propertyForViews.Where(Property => Property.state == filter.state).ToList();
            }

            if (filter.year > 0)
            {
                if (filter.MAJORORMINOROYEAR == 0)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.year <= filter.year).ToList();
                }
                else if (filter.MAJORORMINOROYEAR == 1)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.year >= filter.year).ToList();
                }
            }

            if (filter.price > 0)
            {
                if (filter.MAJORORMINOROPRICE == 0)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.price <= filter.price).ToList();
                }
                else if (filter.MAJORORMINOROPRICE == 1)
                {
                    if (propertyForViewsFilter.Count > 0)
                    {
                        propertyForViews = propertyForViewsFilter;
                    }
                    propertyForViewsFilter = propertyForViews.Where(Property => Property.price >= filter.price).ToList();
                }
            }


            if (filter.idTypeOfProperty > 0)
            {
                if (propertyForViewsFilter.Count > 0)
                {
                    propertyForViews = propertyForViewsFilter;
                }
                propertyForViewsFilter = propertyForViews.Where(Property => Property.idTypeOfProperty == filter.idTypeOfProperty).ToList();
            }

            if (filter.idTypeOfProperty == 0 && filter.price == 0 && filter.year == 0 && filter.numberOfBathrooms == 0 && filter.numberOfFloors == 0 && filter.numberOfRooms == 0 && filter.area == 0 && filter.state == 0)
            {
                return propertyForViews;
            }

            return propertyForViewsFilter;
        }

        public List<PropertyForView> GetPropertyForViewsForSearch(string search)
        {
            var param = new SqlParameter[] {
                        new SqlParameter() {
                            ParameterName = "@SEARCH",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = 10,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = search
                        }};

            List<PropertyForView> newObject = new List<PropertyForView>();

            using (var cnn = _sharedContext.Database.GetDbConnection())
            {
                var cmm = cnn.CreateCommand();
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.CommandText = "[dbo].[SP_SEARCH]";
                cmm.Parameters.AddRange(param);
                cmm.Connection = cnn;
                cnn.Open();
                var reader = cmm.ExecuteReader();

                PropertyForView PropertyForView = null;

                while (reader.Read())
                {
                    PropertyForView = new PropertyForView();
                    PropertyForView.id = int.Parse(reader["Id"].ToString());
                    PropertyForView.name = reader["Name"].ToString();
                    PropertyForView.address = reader["Address"].ToString();
                    PropertyForView.idProject = int.Parse(reader["IdProject"].ToString());
                    PropertyForView.nameProject = reader["NAMEPROJECT"].ToString();
                    PropertyForView.department = reader["DEPARTMENT"].ToString();
                    PropertyForView.city = reader["City"].ToString();
                    PropertyForView.country = reader["COUNTRY"].ToString();
                    PropertyForView.idTypeOfProperty = int.Parse(reader["IdTypeOfProperty"].ToString());
                    PropertyForView.typeProperty = reader["TYPEPROPERTY"].ToString();
                    PropertyForView.price = int.Parse(reader["Price"].ToString());
                    PropertyForView.area = int.Parse(reader["Area"].ToString());
                    PropertyForView.numberOfFloors = int.Parse(reader["NumberOfFloors"].ToString());
                    PropertyForView.numberOfRooms = int.Parse(reader["NumberOfRooms"].ToString());
                    PropertyForView.numberOfBathrooms = int.Parse(reader["NumberOfBathrooms"].ToString());
                    PropertyForView.codeInternal = int.Parse(reader["CodeInternal"].ToString());
                    PropertyForView.year = int.Parse(reader["Year"].ToString());
                    PropertyForView.state = int.Parse(reader["State"].ToString());
                    PropertyForView.stateProperty = reader["STATEPROPERTY"].ToString();
                    PropertyForView.idOwner = int.Parse(reader["IdOwner"].ToString());
                    PropertyForView.owner = reader["OWNER"].ToString();
                    PropertyForView.neighborhood = reader["Neighborhood"].ToString();
                    newObject.Add(PropertyForView);
                }
                cnn.Close();
            }

            return newObject;
        }
    }
}

﻿using Applications.Interfaces.Shared;
using Repository.Interfaces.Shared;
using Shared.Models;
using Shared.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Shared
{
    public class PropertyImageApplication : IPropertyImageApplication
    {
        private IPropertyImageRepository _propertyImageRepository;

        public PropertyImageApplication(IPropertyImageRepository propertyImageRepository)
        {
            _propertyImageRepository = propertyImageRepository;
        }

        public int GetLastOrderOfImagenForProyect(int id)
        {
            PropertyImage listOfProperties = _propertyImageRepository.GetPropertyImages().Where(propertyImage => propertyImage.IdProperty == id).LastOrDefault();

            return listOfProperties.order;
        }

        public List<PropertyImage> GetPropertyImagesForProperty(int id)
        {
            List<PropertyImage> listOfProperties = _propertyImageRepository.GetPropertyImages().Where(propertyImage => propertyImage.IdProperty == id).ToList();

            return listOfProperties;
        }


    }
}

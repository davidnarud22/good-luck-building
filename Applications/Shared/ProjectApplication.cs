﻿using Applications.Interfaces.Shared;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Shared.Models;
using Shared.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Applications.Shared
{
    public class ProjectApplication : IProjectApplication
    {
        private SharedContext _sharedContext;

        public ProjectApplication(SharedContext sharedContext)
        {
            _sharedContext = sharedContext;
        }
        public List<Project> getProjectsForMacroProjects(int id)
        {
            return _sharedContext.projects.Where(project => project.IdMacroProject == id).ToList();
        }          
    }
}

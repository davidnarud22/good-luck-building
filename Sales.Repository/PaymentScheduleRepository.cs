﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.PaymentSchedules;
using Sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Repository
{
    public class PaymentScheduleRepository : IPaymentScheduleRepository
    {
        private SalesContext _salesContext;

        public PaymentScheduleRepository(SalesContext salesContext)
        {
            _salesContext = salesContext;
        }

        public PaymentSchedule GetPaymentSchedule(int id)
        {
            return _salesContext.paymentSchedules.Find(id);
        }

        public List<PaymentSchedule> GetPaymentSchedules()
        {
            return _salesContext.paymentSchedules.ToList();
        }

        public PaymentSchedule AddPaymentSchedule(PaymentSchedule paymentSchedule)
        {
            _salesContext.paymentSchedules.Add(paymentSchedule);
            _salesContext.SaveChanges();
            return paymentSchedule;
        }

        public PaymentSchedule EditPaymentSchedule(PaymentSchedule paymentSchedule)
        {
            _salesContext.Entry(paymentSchedule).State = EntityState.Modified;
            _salesContext.SaveChanges();
            return paymentSchedule;
        }

        public void DeletePaymentSchedule(int id)
        {
            var paymentScheduleToDelete = _salesContext.paymentSchedules.Find(id);
            _salesContext.paymentSchedules.Remove(paymentScheduleToDelete);
            _salesContext.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.Sales;
using Sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Repository
{
    public class PropertyTraceRepository : IPropertyTraceRepository
    {
        private SalesContext _salesContext;

        public PropertyTraceRepository(SalesContext salesContext)
        {
            _salesContext = salesContext;
        }

        public PropertyTrace GetPropertyTrace(int id)
        {
            return _salesContext.propertyTraces.Find(id);
        }

        public List<PropertyTrace> GetPropertyTraces()
        {
            return _salesContext.propertyTraces.ToList();
        }

        public PropertyTrace AddPropertyTrace(PropertyTrace propertyTrace)
        {
            _salesContext.propertyTraces.Add(propertyTrace);
            _salesContext.SaveChanges();
            return propertyTrace;
        }

        public PropertyTrace EditPropertyTrace(PropertyTrace propertyTrace)
        {
            _salesContext.Entry(propertyTrace).State = EntityState.Modified;
            _salesContext.SaveChanges();
            return propertyTrace;
        }

        public void DeletePropertyTrace(int id)
        {
            var propertyTraceToDelete = _salesContext.propertyTraces.Find(id);
            _salesContext.propertyTraces.Remove(propertyTraceToDelete);
            _salesContext.SaveChanges();
        }
    }
}

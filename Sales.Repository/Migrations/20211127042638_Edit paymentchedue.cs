﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Repository.Migrations
{
    public partial class Editpaymentchedue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdPropertyTrace",
                table: "paymentSchedules");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdPropertyTrace",
                table: "paymentSchedules",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

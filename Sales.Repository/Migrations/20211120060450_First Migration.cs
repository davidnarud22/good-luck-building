﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Repository.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "paymentSchedules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NumbersOfQuotas = table.Column<int>(type: "int", nullable: false),
                    ValueOfQuotas = table.Column<int>(type: "int", nullable: false),
                    IdPropertyTrace = table.Column<int>(type: "int", nullable: false),
                    IdPurchaser = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_paymentSchedules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "propertyTraces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateSale = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    Value = table.Column<int>(type: "int", nullable: false),
                    IdTax = table.Column<int>(type: "int", nullable: false),
                    IdDownPayment = table.Column<int>(type: "int", nullable: true),
                    IdProperty = table.Column<int>(type: "int", nullable: false),
                    IdTypeOfPayment = table.Column<int>(type: "int", nullable: false),
                    IdPaymentSchedule = table.Column<int>(type: "int", nullable: true),
                    IdQuote = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_propertyTraces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_propertyTraces_paymentSchedules_IdPaymentSchedule",
                        column: x => x.IdPaymentSchedule,
                        principalTable: "paymentSchedules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_propertyTraces_properties_IdProperty",
                        column: x => x.IdProperty,
                        principalTable: "properties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });


            migrationBuilder.CreateIndex(
                name: "IX_propertyTraces_IdPaymentSchedule",
                table: "propertyTraces",
                column: "IdPaymentSchedule");

            migrationBuilder.CreateIndex(
                name: "IX_propertyTraces_IdProperty",
                table: "propertyTraces",
                column: "IdProperty");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "propertyTraces");

            migrationBuilder.DropTable(
                name: "paymentSchedules");
        }
    }
}

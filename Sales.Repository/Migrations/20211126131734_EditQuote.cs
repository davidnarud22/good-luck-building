﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Repository.Migrations
{
    public partial class EditQuote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<int>(
                name: "IdNewOwner",
                table: "propertyTraces",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdNewOwner",
                table: "propertyTraces");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Repository.Migrations
{
    public partial class editpropertytrc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IdTax",
                table: "propertyTraces",
                newName: "Tax");

            migrationBuilder.RenameColumn(
                name: "IdDownPayment",
                table: "propertyTraces",
                newName: "DownPayment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Tax",
                table: "propertyTraces",
                newName: "IdTax");

            migrationBuilder.RenameColumn(
                name: "DownPayment",
                table: "propertyTraces",
                newName: "IdDownPayment");
        }
    }
}

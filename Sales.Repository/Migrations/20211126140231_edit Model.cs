﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Repository.Migrations
{
    public partial class editModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdPurchaser",
                table: "paymentSchedules");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdPurchaser",
                table: "paymentSchedules",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Sales.Models;

namespace Sales.Repository
{
    public class SalesContext : DbContext
    {
        public SalesContext(DbContextOptions<SalesContext> options) : base(options)
        {

        }

        public DbSet<PaymentSchedule> paymentSchedules { get; set; }
        public DbSet<PropertyTrace> propertyTraces { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.SalesRoom;
using SalesRoom.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SalesRoom.Repository
{
    public class VisitRepository : IVisitRepository
    {
        private SalesRoomContext _salesRoomContext;

        public VisitRepository(SalesRoomContext salesRoomContext)
        {
            _salesRoomContext = salesRoomContext;
        }

        public Visit GetVisit(int id)
        {
            return _salesRoomContext.visits.Find(id);
        }

        public List<Visit> GetVisits()
        {
            return _salesRoomContext.visits.ToList();
        }

        public Visit AddVisit(Visit visit)
        {
            _salesRoomContext.visits.Add(visit);
            _salesRoomContext.SaveChanges();
            return visit;
        }

        public Visit EditVisit(Visit visit)
        {
            _salesRoomContext.Entry(visit).State = EntityState.Modified;
            _salesRoomContext.SaveChanges();
            return visit;
        }

        public void DeleteVisit(int id)
        {
            var visitToDelete = _salesRoomContext.visits.Find(id);
            _salesRoomContext.visits.Remove(visitToDelete);
            _salesRoomContext.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SalesRoom.Models;

namespace SalesRoom.Repository
{
    public class SalesRoomContext : DbContext
    {
        public SalesRoomContext(DbContextOptions<SalesRoomContext> options) : base(options)
        {

        }

        public DbSet<Quote> quotes { get; set; }
        public DbSet<Visit> visits { get; set; }
    }
}

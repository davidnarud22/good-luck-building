﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces.SalesRoom;
using SalesRoom.Models;
using System.Collections.Generic;
using System.Linq;

namespace SalesRoom.Repository
{
    public class QuoteRepository : IQuoteRepository
    {
        private SalesRoomContext _salesRoomContext;

        public QuoteRepository(SalesRoomContext salesRoomContext)
        {
            _salesRoomContext = salesRoomContext;
        }

        public Quote GetQuote(int id)
        {
            return _salesRoomContext.quotes.Find(id);
        }
        public List<Quote> GetQuotes()
        {
            return _salesRoomContext.quotes.ToList();
        }

        public Quote AddQuote(Quote quote)
        {
            _salesRoomContext.quotes.Add(quote);
            _salesRoomContext.SaveChanges();
            return quote;
        }

        public Quote EditQuote(Quote quote)
        {
            _salesRoomContext.Entry(quote).State = EntityState.Modified;
            _salesRoomContext.SaveChanges();
            return quote;
        }

        public void DeleteQuote(int id)
        {
            var quoteToDelete = _salesRoomContext.quotes.Find(id);
            _salesRoomContext.quotes.Remove(quoteToDelete);
            _salesRoomContext.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalesRoom.Repository.Migrations
{
    public partial class edit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IdTax",
                table: "quotes",
                newName: "Tax");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Tax",
                table: "quotes",
                newName: "IdTax");
        }
    }
}

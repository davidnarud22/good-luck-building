﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalesRoom.Repository.Migrations
{
    public partial class EditPropertyTrace : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdMacroProject",
                table: "quotes");

            migrationBuilder.DropColumn(
                name: "IdProject",
                table: "quotes");

            migrationBuilder.AlterColumn<int>(
                name: "IdProperty",
                table: "quotes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdProperty",
                table: "quotes",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdMacroProject",
                table: "quotes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdProject",
                table: "quotes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
